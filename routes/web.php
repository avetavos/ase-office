<?php

use App\Http\Controllers\Auth\RegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Auth::routes([
    'register' => false,
    'verify' => false,
    'reset' => false    
]);

Route::group(['middleware' => ['web', 'auth']], function() {
    // Login & Register
    Route::get('/register', 'RegisterController@show');
    Route::post('/register', 'RegisterController@register');

    // Dashboard
    Route::get('/', 'DashboardController@index');

    // TQF
    Route::resource('/tqf', 'TqfController');
    Route::get('/subjects/{subject}',"TqfController@indexOfBranch");
    Route::resource('/branch', 'BranchController');
    Route::put('/tqf3/{id}', 'TqfController@updateTqf3');
    Route::put('/tqf5/{id}', 'TqfController@updateTqf5');
    Route::put('/exam/{id}', 'TqfController@updateExam');

    // Education Year
    Route::resource('/education_year', 'EducationYearController');

    // Project
    Route::resource('/project', 'ProjectController');
    Route::get('/project_year/create', 'ProjectController@createYear');
    Route::post('/project_year/create', 'ProjectController@storeYear');
    Route::get('/project_year/edit', 'ProjectController@editYear');
    Route::put('/project_year/{id}/edit', 'ProjectController@updateYear');
    Route::put('/project/{id}/processing', 'ProjectController@uploadProcessingFile');
    Route::put('/project/{id}/processed', 'ProjectController@uploadProcessedFile');
    Route::get('/project_user/index','ProjectController@userProject');
    Route::get('/project_index/summary','ProjectController@summary');

    // Setting
    Route::get('/setting', 'SettingController@show');
    Route::put('/setting', 'SettingController@update');

    //Survey
    Route::resource('/admin_survey','SurveyController');
    Route::get('/admin_survey_list', 'SurveyController@typeIndex');
    Route::get('/admin_survey_list/{type}', 'SurveyController@branchIndex');

    ///User TQF
    Route::get('/subject/{degree}', 'TqfController@degreeIndex');
    Route::get('/subject/{degree}/{branch}', 'TqfController@subjectIndex');

    //Report Survey
    Route::get('/report_survey/{level}','ReportSurveyController@reportType');
    Route::get('/report_survey/graph/{survey_id}','ReportSurveyController@reportBranch');
    Route::get('/report_type', 'ReportSurveyController@typeIndex');
    Route::get('/report_survey/export/{survey_id}', 'ReportSurveyController@export');

    //Report Project
    Route::get('/report_project', 'ReportProjectController@index');

    //Example File
    Route::get('/tqf_example', 'ExampleFileController@tqf');
    Route::get('/project_example', 'ExampleFileController@project');
    Route::get('/exam_example', 'ExampleFileController@exam');
});

//User Survey
Route::get('/survey_type','SurveyController@UserTypeIndex');
Route::get('/survey/{level}','SurveyController@levelIndex');
Route::get('/survey/{level}/{branch_id}','SurveyController@qrcodeBranch');
Route::get('/survey_form/{survey_id}','SurveyController@show');
Route::post('/survey_form/{id}/save', 'SurveyController@save');
Route::get('/survey_form/{id}/saved', 'SurveyController@saveSuccess');
