<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_years', function (Blueprint $table) {
            $table->Increments('id');
            $table->text('education_indicator');
            $table->text('education_strategy');
            $table->text('research_indicator');
            $table->text('research_strategy');
            $table->text('humanresource_indicator');
            $table->text('humanresource_strategy');
            $table->text('academic_indicator');
            $table->text('academic_strategy');
            $table->text('organization_indicator');
            $table->text('organization_strategy');
            $table->text('workplace_indicator');
            $table->text('workplace_strategy');
            $table->text('greencampus_indicator');
            $table->text('greencampus_strategy');
            $table->text('digital_indicator');
            $table->text('digital_strategy');
            $table->text('international_indicator');
            $table->text('international_strategy');
            $table->text('governance_indicator');
            $table->text('governance_strategy');
            $table->text('cooperate_indicator');
            $table->text('cooperate_strategy');
            $table->integer('education_year_id')->unsigned()->index();
            $table->foreign('education_year_id')->references('id')->on('education_years');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_years');
    }
}
