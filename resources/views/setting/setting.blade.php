@extends('layouts.main')

@section('content')
<div class="row p-3">
  <div class="col-12">
    <h3>ตั้งค่าบัญชีผู้ใช้</h3>
    <hr>
    @include('inc.alert')
  </div>
  <div class="col-lg-10 col-xl-8 mb-3 mx-auto">
    <div class="card p-md-5 p-3">
      {!! Form::open(['action' => 'SettingController@update', 'method' => 'put']) !!}
      <div class="form-row">
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('name', 'ชื่อ นามสกุล')}}
          {{ Form::input('text', 'name', Auth::user()->name, ['class' => 'form-control'])}}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('password', 'รหัสผ่านใหม่')}}
          {{ Form::input('password', 'password', null, ['class' => 'form-control', 'autocomplete' => 'new-password'])}}
          <small id="passwordHelp" class="form-text text-muted">รหัสผ่านต้องมีความยาวอย่างน้อย 8 ตัวอักษร</small>
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('password_confirmation', 'ยืนยันรหัสผ่านใหม่')}}
          {{ Form::input('password', 'password_confirmation', null, ['class' => 'form-control', 'autocomplete' => 'new-password'])}}
        </div>
        <div class="col-lg-8 mx-auto text-center mt-3">
          {{ Form::input('submit', null, 'บันทึกการเปลี่ยนแปลง', ['class' => 'btn btn-primary px-5'])}}
        </div>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection