<li class="sidebar-none-dropdown">
  <a href="{{url('/')}}">
    <i class="fa fa-tachometer-alt"></i>
    <span>หน้าหลัก</span>
  </a>
</li>
<li class="sidebar-dropdown">
  <a href="#">
    <i class="fas fa-book"></i>
    <span>มคอ.</span>
  </a>
  <div class="sidebar-submenu">
    <ul>
      <li>
        <a href="{{url('/subject/bachelor')}}">ระดับปริญญาตรี</a>
      </li>
      <li>
        <a href="{{url('/subject/master')}}">ระดับบัณฑิตศึกษา</a>
      </li>
      <li>
        <a href="{{url('/tqf_example')}}">ตัวอย่างเอกสาร มคอ.</a>
      </li>
    </ul>
  </div>
</li>
<li class="sidebar-dropdown">
  <a href="#">
    <i class="fas fa-folder"></i>
    <span>โครงการ</span>
  </a>
  <div class="sidebar-submenu">
    <ul>
      <li>
        <a href="{{url('/project_user/index')}}">โครงการทั้งหมด</a>
      </li>
      <li>
        <a href="{{url('/project_example')}}">ตัวอย่างเอกสารโครงการ</a>
      </li>
    </ul>
  </div>
</li>
<li class="sidebar-menu">
  <a href="{{url('/survey_type')}}">
    <i class="fa fa-chart-line"></i>
    <span>แบบประเมินคุณภาพ</span>
  </a>
</li>
<li class="sidebar-dropdown">
  <a href="#">
    <i class="fas fa-file"></i>
    <span>ตัวอย่างเอกสาร</span>
  </a>
  <div class="sidebar-submenu">
    <ul>
      <li>
        <a href="{{url('/tqf_example')}}">ตัวอย่างเอกสาร มคอ.</a>
      </li>
      <li>
        <a href="{{url('/project_example')}}">ตัวอย่างเอกสารโครงการ</a>
      </li>
      <li>
        <a href="{{url('/exam_example')}}">ตัวอย่างเอกสารทวนสอบ</a>
      </li>
    </ul>
  </div>
</li>