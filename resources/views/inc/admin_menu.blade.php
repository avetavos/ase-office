<li>
  <a href="{{ '/' }}">
    <i class="fa fa-tachometer-alt"></i>
    <span>หน้าหลัก</span>
  </a>
</li>
<li class="sidebar-dropdown">
  <a href="#">
    <i class="fas fa-book-medical"></i>
    <span>แผนการสอน</span>
  </a>
  <div class="sidebar-submenu">
    <ul>
      <li>
        <a href="{{ url('/tqf/create') }}">สร้างวิชาใหม่</a>
      </li>
      <li>
        <a href="{{ url('/tqf') }}">เรียกดูและแก้ไขวิชา</a>
      </li>
    </ul>
  </div>
</li>
<li class="sidebar-dropdown">
  <a href="#">
    <i class="fas fa-folder-plus"></i>
    <span>โครงการ</span>
  </a>
  <div class="sidebar-submenu">
    <ul>
      <li>
        <a href="{{url('/project_year/create')}}">สร้างปีโครงการใหม่</a>
      </li>
      <li>
        <a href="{{url('/project_year/edit')}}">แก้ไขปีโครงการ</a>
      </li>
      <li>
        <a href="{{url('/project/create')}}">สร้างโครงการใหม่</a>
      </li>
      <li>
        <a href="{{url('/project')}}">โครงการทั้งหมด</a>
      </li>
      <li>
        <a href="{{url('/report_project')}}">ตารางสรุปโครงการ</a>
      </li>
    </ul>
  </div>
</li>
<li class="sidebar-dropdown">
  <a href="#">
    <i class="far fa-check-square"></i>
    <span>แบบประเมิน</span>
  </a>
  <div class="sidebar-submenu">
    <ul>
      <li>
        <a href="{{ url('/admin_survey/create') }}">สร้างแบบประเมินใหม่</a>
      </li>
      <li>
        <a href="{{ url('/admin_survey_list') }}">แก้ไขรายละเอียดแบบประเมิน</a>
      </li>
    </ul>
  </div>
</li>
<li class="sidebar-menu">
  <a href="{{url('/report_type')}}">
    <i class="fa fa-chart-line"></i>
    <span>ผลตอบกลับแบบประเมิน</span>
  </a>
</li>
<li class="sidebar-dropdown">
  <a href="#">
    <i class="fas fa-graduation-cap"></i>
    <span>ปีการศึกษา</span>
  </a>
  <div class="sidebar-submenu">
    <ul>
      <li>
        <a href="{{ url('/education_year/create') }}">สร้างปีการศึกษาใหม่</a>
      </li>
      <li>
        <a href="{{ url('/education_year/last/edit') }}">แก้ไขข้อมูลปีการศึกษา</a>
      </li>
    </ul>
  </div>
</li>
<li class="sidebar-dropdown">
  <a href="#">
    <i class="fas fa-chalkboard-teacher"></i>
    <span>สาขาวิชา</span>
  </a>
  <div class="sidebar-submenu">
    <ul>
      <li>
        <a href="{{ url('branch/create') }}">สร้างสาขาวิชาใหม่</a>
      </li>
      <li>
        <a href="{{ url('/branch') }}">แก้ไขข้อมูลสาขาวิชา</a>
      </li>
    </ul>
  </div>
</li>
<li>
  <a href="{{ url('register') }}">
    <i class="fas fa-user-plus"></i>
    <span>สร้างบัญชีผู้ใช้</span>
  </a>
</li>