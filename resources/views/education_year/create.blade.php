@extends('layouts.main')

@section('content')
<div class="row p-3">
  <div class="col-12">
    <h3>ปีการศึกษาใหม่</h3>
    <hr>
    @include('inc.alert')
  </div>
  <div class="col-lg-10 col-xl-8 mb-3 mx-auto">
    <div class="card p-md-5 p-3">
      {!! Form::open(['action' => 'EducationYearController@store']) !!}
      <div class="form-row">
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('year', 'ปีการศึกษา')}}
          {{ Form::select('year', $years, null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('start_first_term', 'วัน/เดือน/ปี ที่เปิดเทอมหนึ่ง')}}
          {{ Form::input('date', 'start_first_term', null, ['class' => 'form-control'])}}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('end_first_term', 'วัน/เดือน/ปี ที่ปิดเทอมหนึ่ง')}}
          {{ Form::input('date', 'end_first_term', null, ['class' => 'form-control'])}}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('start_second_term', 'วัน/เดือน/ปี ที่เปิดเทอมสอง')}}
          {{ Form::input('date', 'start_second_term', null, ['class' => 'form-control'])}}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('end_second_term', 'วัน/เดือน/ปี ที่ปิดเทอมสอง')}}
          {{ Form::input('date', 'end_second_term', null, ['class' => 'form-control'])}}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          <p class="text-danger mb-0"><b>* หนึ่งปีการศึกษาสามารถสร้างได้แค่ครั้งเดียวและไม่สามารถสร้างซ้ำหรือลบได้
              แต่สามารถแก้ไขวันที่เปิดและปิดเทอมได้จากเมนู "ปีการศึกษา > แก้ไขข้อมูลปีการศึกษา"</b></p>
        </div>
        <div class="col-lg-8 mx-auto text-center mt-3">
          {{ Form::input('submit', null, 'สร้าง', ['class' => 'btn btn-primary px-5'])}}
        </div>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection