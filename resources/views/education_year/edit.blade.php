@extends('layouts.main')

@section('content')
<div class="row p-3">
  <div class="col-12">
    <h3>แก้ไขข้อมูลปีการศึกษา {{$current_year->year}}</h3>
    <hr>
    @include('inc.alert')
  </div>
  <div class="col-lg-10 col-xl-8 mb-3 mx-auto">
    <div class="card p-md-5 p-3">
      {!! Form::open(['action' => ['EducationYearController@update', $current_year->id], 'method' => 'put']) !!}
      <div class="form-row">
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('year', 'ปีการศึกษา')}}
          {{ Form::select('year', $years, $current_year->year, ['class' => 'form-control', 'disabled' => 'true']) }}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('start_first_term', 'วัน/เดือน/ปี ที่เปิดเทอมหนึ่ง')}}
          {{ Form::input('date', 'start_first_term', $current_year->start_first_term, ['class' => 'form-control'])}}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('end_first_term', 'วัน/เดือน/ปี ที่ปิดเทอมหนึ่ง')}}
          {{ Form::input('date', 'end_first_term', $current_year->end_first_term, ['class' => 'form-control'])}}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('start_second_term', 'วัน/เดือน/ปี ที่เปิดเทอมสอง')}}
          {{ Form::input('date', 'start_second_term', $current_year->start_second_term, ['class' => 'form-control'])}}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('end_second_term', 'วัน/เดือน/ปี ที่ปิดเทอมสอง')}}
          {{ Form::input('date', 'end_second_term', $current_year->end_second_term, ['class' => 'form-control'])}}
        </div>
        <div class="col-lg-8 mx-auto text-center mt-3">
          {{ Form::input('submit', null, 'บันทึกการแก้ไข', ['class' => 'btn btn-primary px-5'])}}
        </div>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection