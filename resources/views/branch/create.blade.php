@extends('layouts.main')

@section('content')
<div class="row p-3">
  <div class="col-12">
    <h3>สร้างหลักสูตรใหม่</h3>
    <hr>
    @include('inc.alert')
  </div>
  <div class="col-lg-10 col-xl-8 mb-3 mx-auto">
    <div class="card p-md-5 p-3">
      {!! Form::open(['action' => 'BranchController@store']) !!}
      <div class="form-row">
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('name', 'ชื่อสาขาภาษาไทย')}}
          {{ Form::input('text', 'name', null, ['class' => 'form-control'])}}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('degree', 'ระดับ')}}
          {{ Form::select('degree', ['bachelor' => 'ปริญญาตรี', 'master' => 'ปริญญาโท', 'doctor' => 'ปริญญาเอก'], null, ['class' => 'form-control']) }}
        </div>
        <div class="col-lg-8 mx-auto text-center mt-3">
          {{ Form::input('submit', null, 'สร้าง', ['class' => 'btn btn-primary px-5'])}}
        </div>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection