@extends('layouts.main')

@section('content')
<div class="row p-3">
  <div class="col-12">
    <h3>แก้ไขข้อมูลสาขาวิชา</h3>
    <hr>
    @include('inc.alert')
  </div>
  <div class="col-lg-10 col-xl-8 mb-3 mx-auto">
    <div class="card p-md-5 p-3">
      {!! Form::open(['action' => ['BranchController@update', $branch->id], 'method' => 'put']) !!}
      <div class="form-row">
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('name', 'ชื่อสาขาภาษาไทย')}}
          {{ Form::input('text', 'name', $branch->name, ['class' => 'form-control'])}}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('degree', 'ระดับ')}}
          {{ Form::select('degree', ['bachelor' => 'ปริญญาตรี', 'master' => 'ปริญญาโท', 'doctor' => 'ปริญญาเอก'], $branch->degree, ['class' => 'form-control']) }}
        </div>
        <div class="col-lg-8 mx-auto text-center mt-3">
          {{ Form::input('submit', null, 'บันทึก', ['class' => 'btn btn-primary px-5'])}}
        </div>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
  <div class="col-lg-10 col-xl-8 mb-3 mx-auto">
    <div class="card p-3">
      {!! Form::open(['action' => ['BranchController@destroy', $branch->id], 'method' => 'delete']) !!}
      <div class="col-lg-8 mx-auto text-center">
        <h4 class="text-danger">ลบสาขาวิชา <b>{{$branch->name}}</b></h4>
        {{ Form::input('hidden', 'branch_name', $branch->name)}}
        {{ Form::input('submit', null, 'ลบ', ['class' => 'btn btn-danger px-5'])}}
      </div>
    </div>
    {!! Form::close() !!}
  </div>
</div>
</div>
@endsection