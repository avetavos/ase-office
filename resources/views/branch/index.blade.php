@extends('layouts.main') 
@section('content')
<div class="row p-3">
  <div class="col-12">
    <h3>แก้ไขข้อมูลสาขาวิชา</h3>
    <hr>
  @include('inc.alert')
  </div>
  <div class="col-lg-6 mx-auto">
    <h4><u>สาขาวิชา</u></h4>
    @if (count($bachelor)
    <=0 && count($master) <=0 && count($doctor) <=0 ) <p>ยังไม่มีสาขาวิชาในตอนนี้</p>
      @else @if (count($bachelor) > 0)
      <div>
        <h5>ระดับปริญญาตรี</h5>
        <ul class="list-group">
          @foreach ($bachelor as $branch)
          <li class="list-group-item"><a href="{{url('branch/'.$branch->id.'/edit')}}">{{$branch->name}}</a></li>
          @endforeach
        </ul>
      </div>
      @endif @if (count($master) > 0)
      <div class="mt-3">
        <h5>ระดับปริญญาโท</h5>
        <ul class="list-group">
          @foreach ($master as $branch)
          <li class="list-group-item"><a href="{{url('branch/'.$branch->id.'/edit')}}">{{$branch->name}}</a></li>
          @endforeach
        </ul>
      </div>
      @endif @if (count($doctor) > 0)
      <div class="mt-3">
        <h5>ระดับปริญญาเอก</h5>
        <ul class="list-group">
          @foreach ($doctor as $branch)
          <li class="list-group-item"><a href="{{url('branch/'.$branch->id.'/edit')}}">{{$branch->name}}</a></li>
          @endforeach
        </ul>
      </div>
      @endif @endif
  </div>
</div>
@endsection