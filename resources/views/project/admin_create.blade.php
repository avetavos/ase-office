@extends('layouts.main')
@section('content')
<div class="row p-3">
  <div class="col-12">
    <h3>สร้างโครงการใหม่</h3>
    <hr>
    @include('inc.alert')
  </div>
  <div class="col-lg-10 col-xl-8 mb-3 mx-auto">
    <div class="card p-md-5 p-3">
      {!! Form::open (['action' => 'ProjectController@store', 'method' => 'post']) !!}
      <div class="form-row">
        <div class="form-group col-lg-8 mx-auto">
          <p class="text-danger mb-0 text-center"><b>**ปีโครงการที่สร้างจะขึ้นอยู่กับปีการศึกษาล่าสุด
              ขณะนี้ปี {{$current_year->year}}**</b></p>
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('name' ,'ชื่อโครงการ') }}
          {{ Form::input('text','name', null,['class' => 'form-control']) }}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('type' ,'ประเภทโครงการ') }}
          {{ Form::select('type',
                        [
                            '1' => 'ประเด็นยุทธ์ศาสตร์ที่ 1 ปรับเปลี่ยนการจัดการศึกษา',
                            '2' => 'ประเด็นยุทธ์ศาสตร์ที่ 2 ปรับเปลี่ยนการทำงานวิจัย',
                            '3' => 'ประเด็นยุทธ์ศาสตร์ที่ 3 ปรับเปลี่ยนการบริหารทรัพยากรบุคคล',
                            '4' => 'ประเด็นยุทธ์ศาสตร์ที่ 4 ปรับเปลี่ยนการบริการวิชาการ',
                            '5' => 'ประเด็นยุทธ์ศาสตร์ที่ 5 ปรับเปลี่ยนการบริหารจัดการองค์กร',
                            '6' => 'ประเด็นยุทธ์ศาสตร์ที่ 6 สร้างมหาวิทยาลัยขอนแก่นให้เป็นที่น่าทำงาน',
                            '7' => 'ประเด็นยุทธ์ศาสตร์ที่ 7 สร้างมหาวิทยาลัยให้เป็นที่น่าอยู่',
                            '8' => 'ประเด็นยุทธ์ศาสตร์ที่ 8 ปรับเปลี่ยนองค์กรให้ก้าวสู่ยุคดิจิทัล',
                            '9' => 'ประเด็นยุทธ์ศาสตร์ที่ 9 การนำมหาลัยสู่ความเป็นนานาชาติ',
                            '10' => 'ประเด็นยุทธ์ศาสตร์ที่ 10 การบริหารโดยใช้หลักธรรมาภิบาล',
                            '11' => 'ประเด็นยุทธ์ศาสตร์ที่ 11 เสริมสร้างความร่วมมือเพื่อการพัฒนา'
                        ], 
                    null,['class' => 'form-control']) }}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('strategy' ,'กลยุทธ์') }}
          {{ Form::select('strategy', [], null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('indicator' ,'ตัวชี้วัด') }}
          {{ Form::select('indicator', [], null, ['class' => 'form-control', 'name' => 'indicator[]', 'multiple' => 'multiple', 'required']) }}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('user_id' ,'ผู้รับผิดชอบโครงการ') }}
          {{ Form::select('user_id',$users, null,['class' => 'form-control']) }}
        </div>
        <div class="col-lg-8 mx-auto text-center mt-3">
          {{ Form::input('submit', null, 'สร้าง', ['class' => 'btn btn-primary px-5'])}}
        </div>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection

@section('custom_script')
<script>
  var projectYearDetail = {!! json_encode($project_year)!!}
  $(document).ready(async () => {
    await projectYearDetail['education_indicator'].forEach(function (item) {
      $('#indicator').append(`<option value="${item}">${item}</option>`)
    });
    await projectYearDetail['education_strategy'].forEach(function (item) {
      $('#strategy').append(`<option value="${item}">${item}</option>`)
    });
    $('#indicator').select2({ theme: "bootstrap4" });
  })
  $('#type').change(function () {
    $('#indicator').empty();
    $('#strategy').empty();
    if (this.value == 1) {
      projectYearDetail['education_indicator'].forEach(function (item) {
        $('#indicator').append(`<option value="${item}">${item}</option>`)
      });
      projectYearDetail['education_strategy'].forEach(function (item) {
        $('#strategy').append(`<option value="${item}">${item}</option>`)
      });
    } else if (this.value == 2) {
      projectYearDetail['research_indicator'].forEach(function (item) {
        $('#indicator').append(`<option value="${item}">${item}</option>`)
      });
      projectYearDetail['research_strategy'].forEach(function (item) {
        $('#strategy').append(`<option value="${item}">${item}</option>`)
      });
    } else if (this.value == 3) {
      projectYearDetail['humanresource_indicator'].forEach(function (item) {
        $('#indicator').append(`<option value="${item}">${item}</option>`)
      });
      projectYearDetail['humanresource_strategy'].forEach(function (item) {
        $('#strategy').append(`<option value="${item}">${item}</option>`)
      });
    } else if (this.value == 4) {
      projectYearDetail['academic_indicator'].forEach(function (item) {
        $('#indicator').append(`<option value="${item}">${item}</option>`)
      });
      projectYearDetail['academic_strategy'].forEach(function (item) {
        $('#strategy').append(`<option value="${item}">${item}</option>`)
      });
    } else if (this.value == 5) {
      projectYearDetail['organization_indicator'].forEach(function (item) {
        $('#indicator').append(`<option value="${item}">${item}</option>`)
      });
      projectYearDetail['organization_strategy'].forEach(function (item) {
        $('#strategy').append(`<option value="${item}">${item}</option>`)
      });
    } else if (this.value == 6) {
      projectYearDetail['workplace_indicator'].forEach(function (item) {
        $('#indicator').append(`<option value="${item}">${item}</option>`)
      });
      projectYearDetail['workplace_strategy'].forEach(function (item) {
        $('#strategy').append(`<option value="${item}">${item}</option>`)
      });
    } else if (this.value == 7) {
      projectYearDetail['greencampus_indicator'].forEach(function (item) {
        $('#indicator').append(`<option value="${item}">${item}</option>`)
      });
      projectYearDetail['greencampus_strategy'].forEach(function (item) {
        $('#strategy').append(`<option value="${item}">${item}</option>`)
      });
    } else if (this.value == 8) {
      projectYearDetail['digital_indicator'].forEach(function (item) {
        $('#indicator').append(`<option value="${item}">${item}</option>`)
      });
      projectYearDetail['digital_strategy'].forEach(function (item) {
        $('#strategy').append(`<option value="${item}">${item}</option>`)
      });
    } else if (this.value == 9) {
      projectYearDetail['international_indicator'].forEach(function (item) {
        $('#indicator').append(`<option value="${item}">${item}</option>`)
      });
      projectYearDetail['international_strategy'].forEach(function (item) {
        $('#strategy').append(`<option value="${item}">${item}</option>`)
      });
    } else if (this.value == 10) {
      projectYearDetail['governance_indicator'].forEach(function (item) {
        $('#indicator').append(`<option value="${item}">${item}</option>`)
      });
      projectYearDetail['governance_strategy'].forEach(function (item) {
        $('#strategy').append(`<option value="${item}">${item}</option>`)
      });
    } else if (this.value == 11) {
      projectYearDetail['cooperate_indicator'].forEach(function (item) {
        $('#indicator').append(`<option value="${item}">${item}</option>`)
      });
      projectYearDetail['cooperate_strategy'].forEach(function (item) {
        $('#strategy').append(`<option value="${item}">${item}</option>`)
      });
    }
  })
</script>
@endsection