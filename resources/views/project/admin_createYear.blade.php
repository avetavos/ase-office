@extends('layouts.main')
@section('custom_style')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
<div class="row p-3">
  <div class="col-12">
    <h3>สร้างปีโครงการใหม่</h3>
    <hr />
    @include('inc.alert')
  </div>
  <div class="col-xl-8 col-lg-10 mb-3 mx-auto">
    <div class="card p-md-5 p-3">
      <div class="row mx-0 mb-2">
        <h4 class="font-weight-bold">ปีโครงการ</h4>
        {{ Form::select('year', $years, null, ['class' => 'form-control', 'id' => 'year_select']) }}
      </div>
      <hr>
      <form id="strategic_form" onsubmit="onHandleSubmit(event)">
        <div class="form-row">
          <div class="col-lg-8 mx-auto row">
            <div class="col-12 px-0">
              <p>
                <b>ประเด็นยุทธศาสตร์</b>
              </p>
            </div>
            <div class="col-12 px-0 mb-2">
              <input class="form-control" name="strategic" type="text" required>
            </div>
            <div class="col-6 px-0">
              <label for="strategy">กลยุทธ์</label>
            </div>
            <div class="col-6 text-right px-0 text-primary">
              <button class="btn btn-link" type="button" onclick="addStrategy()">
                <i class="far fa-plus-square"></i> เพิ่มกลยุทธ์
              </button>
            </div>
            <div class="col-12 px-0" id="strategy_container">
              <div class="input-group mb-2">
                <input class="form-control" name="strategy" type="text" required>
                <div class="input-group-append">
                  <button class="btn" type="button" onclick="removeParent($(this).parent())">
                    <i class="fas fa-times text-danger"></i>
                  </button>
                </div>
              </div>
            </div>
            <div class="col-6 px-0">
              <label for="indicator">ตัวชี้วัด</label>
            </div>
            <div class="col-6 text-right px-0 text-primary">
              <button class="btn btn-link" type="button" onclick="addIndicator()">
                <i class="far fa-plus-square"></i>
                เพิ่มตัวชี้วัด
              </button>
            </div>
            <div class="col-12 px-0" id="indicator_container">
              <div class="input-group mb-2">
                <input class="form-control" name="indicator" type="text" required>
                <div class="input-group-append">
                  <button class="btn" type="button" onclick="removeParent($(this).parent())">
                    <i class="fas fa-times text-danger"></i>
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-8 mx-auto mt-3">
            <button class="btn btn-primary mx-auto d-block" type="submit">เพิ่มประเด็นยุทธศาสตร์</button>
          </div>
        </div>
      </form>
      <hr>
      <div class="row mx-0 col-lg-8 col-md-10 mx-auto" id="display_strategic">
      </div>
      <div class="d-none" id="submit_form">
        <button type="button" class="btn btn-success d-block mx-auto w-50"
          onclick="handleRequest()">บันทึกปีโครงการ</button>
        </form>
      </div>

    </div>
  </div>
</div>
@endsection @section('custom_script')
<script>
  function addIndicator() {
    $(`#indicator_container`).append(`
          <div class="input-group mb-2">
            <input class="form-control" name="indicator" type="text" required>
            <div class="input-group-append">
              <button class="btn" type="button" onclick="removeParent($(this).parent())"><i class="fas fa-times text-danger"></i></button>
            </div>
          </div>
      `);
  }

  function addStrategy() {
    $(`#strategy_container`).append(`
          <div class="input-group mb-2">
            <input class="form-control" name="strategy" type="text" required>
            <div class="input-group-append">
              <button class="btn" type="button" onclick="removeParent($(this).parent())"><i class="fas fa-times text-danger"></i></button>
            </div>
          </div>
      `);
  }

  function removeParent(elem) {
    if (
      $(elem)
        .parent()
        .parent()
        .children().length > 1
    ) {
      $(elem)
        .parent()
        .remove();
    }
  }

  let request_data = {
    _token: '',
    year: 0,
    detail: []
  };

  async function onHandleSubmit(e) {
    e.preventDefault();
    const strategic_data = await $("#strategic_form").serializeArray();
    await $('#strategic_form').trigger('reset')

    while ($('#strategy_container').children().length > 1) {
      $('#strategy_container').children()[1].remove();
    }
    while ($('#indicator_container').children().length > 1) {
      $('#indicator_container').children()[1].remove();
    }
    const strategic_information = {
      strategic: '',
      strategy: [],
      indicator: []
    }
    await strategic_data.forEach(item => {
      if (item.name === "strategic") {
        strategic_information.strategic = item.value;
      } else if (item.name === "strategy") {
        strategic_information.strategy.push(item.value);
      } else if (item.name === "indicator") {
        strategic_information.indicator.push(item.value);
      }
    })
    await request_data.detail.push(strategic_information)
    generateStrategicList();
    $('#submit_form').removeClass('d-none')
  }

  async function generateStrategicList() {
    $('#display_strategic').empty();
    const strategic = request_data.detail;
    strategic.forEach(item => {
      let strategyList = '';
      for (let i = 0; i < item.strategy.length; i++) {
        strategyList += `<li>${item.strategy[i]}</li>`
      }
      let indicatorList = '';
      for (let i = 0; i < item.indicator.length; i++) {
        indicatorList += `<li>${item.indicator[i]}</li>`
      }
      $('#display_strategic').append(`<div class="col-12">
          <h5 class="font-weight-bold">ประเด็นยุทธศาสตร์: ${item.strategic}</h5>
          <div class="ml-3">
            <h6 class="font-weight-bold">กลยุทธ์</h6>
            <ul>
              ${strategyList}
            </ul>
            <h6 class="font-weight-bold">ตัวชี้วัด</h6>
            <ul>
              ${indicatorList}
            </ul>
          </div>
        </div>`)
    })
  }

  async function handleRequest() {
    request_data.year = await $('#year_select').val()
    await $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: "/project_year/create",
      method: "POST",
      data: request_data,
      dataType: "json",
      error: res => {
        if (res.status === 200) {
          window.location.href = '/project_year/edit'
        } else {
          location.reload()
        }
      },
    });
  }
</script>
@endsection