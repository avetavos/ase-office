@extends('layouts.main')
@section('content')
<div class="row p-3">
  <div class="col-12">
    <h3>แก้ไขข้อมูลปีโครงการใหม่</h3>
    <hr>
    @include('inc.alert')
  </div>
  <div class="col-lg-8 mb-3 mx-auto">
    <div class="card p-md-5 p-3">
      {!! Form::open(['action' => ['ProjectController@updateYear', $project_year->id] ,'method' => 'put', 'id' => 'edit_form']) !!}
      <div class="form-row">
        <div class="form-group col-lg-8 mx-auto">
          <p class="text-danger mb-0 text-center"><b>**ปีโครงการที่สร้างจะขึ้นอยู่กับปีการศึกษาล่าสุด
              ขณะนี้ปี {{$current_year->year}}**</b></p>
        </div>

        <div class="col-lg-8 mx-auto row">
          <div class="col-12 px-0">
            <p><b>ประเด็นยุทธศาสตร์ที่ 2 ปรับเปลี่ยนการทำงานวิจัย</b></p>
          </div>
          <div class="col-6 px-0">
            {{ Form::label('research_strategy', 'กลยุทธ์')}}
          </div>
          <div class="col-6 text-right px-0 text-primary">
            <button class="btn btn-link" type="button" id="add_research_strategy">
              <i class="far fa-plus-square"></i> เพิ่มกลยุทธ์
            </button>
          </div>
          <div class="col-12 px-0" id="research_strategy_container">
            @foreach ($project_year->research_strategy as $item)
            <div class="input-group mb-2">
              <input class="form-control" name="research_strategy[]" type="text" value="{{$item}}" required>
              <div class="input-group-append">
                <button class="btn" type="button" onclick="removeParent($(this).parent())"><i
                    class="fas fa-times text-danger"></i></button>
              </div>
            </div>
            @endforeach
          </div>
          <div class="col-6 px-0">
            {{ Form::label('research_indicator', 'ตัวชี้วัด')}}
          </div>
          <div class="col-6 text-right px-0 text-primary">
            <button class="btn btn-link" type="button" id="add_research_indicator">
              <i class="far fa-plus-square"></i> เพิ่มตัวชี้วัด
            </button>
          </div>
          <div class="col-12 px-0" id="research_indicator_container">
            @foreach ($project_year->research_indicator as $item)
            <div class="input-group mb-2">
              <input class="form-control" name="research_indicator[]" type="text" value="{{$item}}" required>
              <div class="input-group-append">
                <button class="btn" type="button" onclick="removeParent($(this).parent())"><i
                    class="fas fa-times text-danger"></i></button>
              </div>
            </div>
            @endforeach
          </div>
        </div>
        <div class="col-lg-8 mx-auto row">
          <div class="col-12 px-0">
            <p><b>ประเด็นยุทธศาสตร์ที่ 3 ปรับเปลี่ยนการบริหารทรัพยากรบุคคล</b></p>
          </div>
          <div class="col-6 px-0">
            {{ Form::label('humanresource_strategy', 'กลยุทธ์')}}
          </div>
          <div class="col-6 text-right px-0 text-primary">
            <button class="btn btn-link" type="button" id="add_humanresource_strategy">
              <i class="far fa-plus-square"></i> เพิ่มกลยุทธ์
            </button>
          </div>
          <div class="col-12 px-0" id="humanresource_strategy_container">
            @foreach ($project_year->humanresource_strategy as $item)
            <div class="input-group mb-2">
              <input class="form-control" name="humanresource_strategy[]" type="text" value="{{$item}}" required>
              <div class="input-group-append">
                <button class="btn" type="button" onclick="removeParent($(this).parent())"><i
                    class="fas fa-times text-danger"></i></button>
              </div>
            </div>
            @endforeach
          </div>
          <div class="col-6 px-0">
            {{ Form::label('humanresource_indicator', 'ตัวชี้วัด')}}
          </div>
          <div class="col-6 text-right px-0 text-primary">
            <button class="btn btn-link" type="button" id="add_humanresource_indicator">
              <i class="far fa-plus-square"></i> เพิ่มตัวชี้วัด
            </button>
          </div>
          <div class="col-12 px-0" id="humanresource_indicator_container">
            @foreach ($project_year->humanresource_indicator as $item)
            <div class="input-group mb-2">
              <input class="form-control" name="humanresource_indicator[]" type="text" value="{{$item}}" required>
              <div class="input-group-append">
                <button class="btn" type="button" onclick="removeParent($(this).parent())"><i
                    class="fas fa-times text-danger"></i></button>
              </div>
            </div>
            @endforeach
          </div>
        </div>
        <div class="col-lg-8 mx-auto row">
          <div class="col-12 px-0">
            <p><b>ประเด็นยุทธศาสตร์ที่ 4 ปรับเปลี่ยนการบริการวิชาการ</b></p>
          </div>
          <div class="col-6 px-0">
            {{ Form::label('academic_strategy', 'กลยุทธ์')}}
          </div>
          <div class="col-6 text-right px-0 text-primary">
            <button class="btn btn-link" type="button" id="add_academic_strategy">
              <i class="far fa-plus-square"></i> เพิ่มกลยุทธ์
            </button>
          </div>
          <div class="col-12 px-0" id="academic_strategy_container">
            @foreach ($project_year->academic_strategy as $item)
            <div class="input-group mb-2">
              <input class="form-control" name="academic_strategy[]" type="text" value="{{$item}}" required>
              <div class="input-group-append">
                <button class="btn" type="button" onclick="removeParent($(this).parent())"><i
                    class="fas fa-times text-danger"></i></button>
              </div>
            </div>
            @endforeach
          </div>
          <div class="col-6 px-0">
            {{ Form::label('academic_indicator', 'ตัวชี้วัด')}}
          </div>
          <div class="col-6 text-right px-0 text-primary">
            <button class="btn btn-link" type="button" id="add_academic_indicator">
              <i class="far fa-plus-square"></i> เพิ่มตัวชี้วัด
            </button>
          </div>
          <div class="col-12 px-0" id="academic_indicator_container">
            @foreach ($project_year->academic_indicator as $item)
            <div class="input-group mb-2">
              <input class="form-control" name="academic_indicator[]" type="text" value="{{$item}}" required>
              <div class="input-group-append">
                <button class="btn" type="button" onclick="removeParent($(this).parent())"><i
                    class="fas fa-times text-danger"></i></button>
              </div>
            </div>
            @endforeach
          </div>
        </div>
        <div class="col-lg-8 mx-auto row">
          <div class="col-12 px-0">
            <p><b>ประเด็นยุทธศาสตร์ที่ 5 ปรับเปลี่ยนการบริหารจัดการองค์กร</b></p>
          </div>
          <div class="col-6 px-0">
            {{ Form::label('organization_strategy', 'กลยุทธ์')}}
          </div>
          <div class="col-6 text-right px-0 text-primary">
            <button class="btn btn-link" type="button" id="add_organization_strategy">
              <i class="far fa-plus-square"></i> เพิ่มกลยุทธ์
            </button>
          </div>
          <div class="col-12 px-0" id="organization_strategy_container">
            @foreach ($project_year->organization_strategy as $item)
            <div class="input-group mb-2">
              <input class="form-control" name="organization_strategy[]" type="text" value="{{$item}}" required>
              <div class="input-group-append">
                <button class="btn" type="button" onclick="removeParent($(this).parent())"><i
                    class="fas fa-times text-danger"></i></button>
              </div>
            </div>
            @endforeach
          </div>
          <div class="col-6 px-0">
            {{ Form::label('organization_indicator', 'ตัวชี้วัด')}}
          </div>
          <div class="col-6 text-right px-0 text-primary">
            <button class="btn btn-link" type="button" id="add_organization_indicator">
              <i class="far fa-plus-square"></i> เพิ่มตัวชี้วัด
            </button>
          </div>
          <div class="col-12 px-0" id="organization_indicator_container">
            @foreach ($project_year->organization_indicator as $item)
            <div class="input-group mb-2">
              <input class="form-control" name="organization_indicator[]" type="text" value="{{$item}}" required>
              <div class="input-group-append">
                <button class="btn" type="button" onclick="removeParent($(this).parent())"><i
                    class="fas fa-times text-danger"></i></button>
              </div>
            </div>
            @endforeach
          </div>
        </div>
        <div class="col-lg-8 mx-auto row">
          <div class="col-12 px-0">
            <p><b>ประเด็นยุทธศาสตร์ที่ 6 สร้างมหาวิทยาลัยขอนแก่นให้เป็นที่น่าทำงาน</b></p>
          </div>
          <div class="col-6 px-0">
            {{ Form::label('workplace_strategy', 'กลยุทธ์')}}
          </div>
          <div class="col-6 text-right px-0 text-primary">
            <button class="btn btn-link" type="button" id="add_workplace_strategy">
              <i class="far fa-plus-square"></i> เพิ่มกลยุทธ์
            </button>
          </div>
          <div class="col-12 px-0" id="workplace_strategy_container">
            @foreach ($project_year->workplace_strategy as $item)
            <div class="input-group mb-2">
              <input class="form-control" name="workplace_strategy[]" type="text" value="{{$item}}" required>
              <div class="input-group-append">
                <button class="btn" type="button" onclick="removeParent($(this).parent())"><i
                    class="fas fa-times text-danger"></i></button>
              </div>
            </div>
            @endforeach
          </div>
  @endsection

  @section('custom_script')
  <script>
    var projectYearData = {!! $project_year !!};
    $(document).ready(() => {
      projectYearData.forEach((strategicItem, strategicIndex)=> {
        let strategyList = '';
        for (let i = 0; i < strategicItem.strategy.length; i++) {
          strategyList += `<div class="input-group mb-2">
              <input class="form-control" name="${strategicIndex}_strategy[]" type="text" value="{{$item}}" required>
              <div class="input-group-append">
                <button class="btn" type="button" onclick="removeParent($(this).parent())"><i
                    class="fas fa-times text-danger"></i></button>
              </div>
            </div>${strategicItem.strategy[i]}</li>`
        }
        let indicatorList = '';
        for (let i = 0; i < strategicItem.indicator.length; i++) {
          indicatorList += `<li>${strategicItem.indicator[i]}</li>`
        }
        $('#edit_form').append(`
        <div class="col-lg-8 mx-auto row">
          <div class="col-12 px-0">
            <p><b>ประเด็นยุทธศาสตร์: ${strategicItem.strategic}</b></p>
          </div>
          <div class="col-6 px-0">
            <label for="strategy">กลยุทธ์</label>
          </div>
          <div class="col-6 text-right px-0 text-primary">
            <button class="btn btn-link" type="button" onclick="addStrategy('education')">
              <i class="far fa-plus-square"></i> เพิ่มกลยุทธ์
            </button>
          </div>
          <div class="col-12 px-0" id="education_strategy_container">
            @foreach ($project_year->education_strategy as $item)
            
            @endforeach
          </div>
          <div class="col-6 px-0">
            <label for="indicator">ตัวชี้วัด</label>
          </div>
          <div class="col-6 text-right px-0 text-primary">
            <button class="btn btn-link" type="button" onclick="addIndicator('education')">
              <i class="far fa-plus-square"></i> เพิ่มตัวชี้วัด
            </button>
          </div>
          <div class="col-12 px-0" id="education_indicator_container">
            @foreach ($project_year->education_indicator as $item)
            <div class="input-group mb-2">
              <input class="form-control" name="education_indicator[]" type="text" value="{{$item}}" required>
              <div class="input-group-append">
                <button class="btn" type="button" onclick="removeParent($(this).parent())"><i
                    class="fas fa-times text-danger"></i></button>
              </div>
            </div>
            @endforeach
          </div>
        </div>
        `)
      })
    })
    function addIndicator(type) {
      $(`#${type}_indicator_container`).append(`
          <div class="input-group mb-2">
            <input class="form-control" name="${type}_indicator[]" type="text" required>
            <div class="input-group-append">
              <button class="btn" type="button" onclick="removeParent($(this).parent())"><i class="fas fa-times text-danger"></i></button>
            </div>
          </div>
      `);
    }

    function addStrategy(type) {
      $(`#${type}_strategy_container`).append(`
          <div class="input-group mb-2">
            <input class="form-control" name="${type}_strategy[]" type="text" required>
            <div class="input-group-append">
              <button class="btn" type="button" onclick="removeParent($(this).parent())"><i class="fas fa-times text-danger"></i></button>
            </div>
          </div>
      `);
    }

    function removeParent(elem) {
      if ($(elem).parent().parent().children().length > 1) {
        $(elem).parent().remove();
      }
    }
  </script>
  @endsection