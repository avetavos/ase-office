@extends('layouts.main')
@section('content')
<div class="row p-3">
  <div class="col-12">
    <h3>สร้างโครงการใหม่</h3>
    <hr>
    @include('inc.alert')
  </div>
  <div class="col-lg-10 col-xl-8 mb-3 mx-auto">
    <div class="card p-md-5 p-3">
      {!! Form::open (['action' => ['ProjectController@update', $project->id], 'method' => 'PUT']) !!}
      <div class="form-row">
        <div class="form-group col-lg-8 mx-auto">
          <p class="text-danger mb-0 text-center"><b>**ปีโครงการที่สร้างจะขึ้นอยู่กับปีการศึกษาล่าสุด
              ขณะนี้ปี {{$current_year->year}}**</b></p>
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('name' ,'ชื่อโครงการ') }}
          {{ Form::input('text','name', $project->name,['class' => 'form-control']) }}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('type' ,'ประเภทโครงการ') }}
          {{ Form::select('type',
                        [
                          '1' => 'ประเด็นยุทธ์ศาสตร์ที่ 1 ปรับเปลี่ยนการจัดการศึกษา',
                          '2' => 'ประเด็นยุทธ์ศาสตร์ที่ 2 ปรับเปลี่ยนการทำงานวิจัย',
                          '3' => 'ประเด็นยุทธ์ศาสตร์ที่ 3 ปรับเปลี่ยนการบริหารทรัพยากรบุคคล',
                          '4' => 'ประเด็นยุทธ์ศาสตร์ที่ 4 ปรับเปลี่ยนการบริการวิชาการ',
                          '5' => 'ประเด็นยุทธ์ศาสตร์ที่ 5 ปรับเปลี่ยนการบริหารจัดการองค์กร',
                          '6' => 'ประเด็นยุทธ์ศาสตร์ที่ 6 สร้างมหาวิทยาลัยขอนแก่นให้เป็นที่น่าทำงาน',
                          '7' => 'ประเด็นยุทธ์ศาสตร์ที่ 7 สร้างมหาวิทยาลัยให้เป็นที่น่าอยู่',
                          '8' => 'ประเด็นยุทธ์ศาสตร์ที่ 8 ปรับเปลี่ยนองค์กรให้ก้าวสู่ยุคดิจิทัล',
                          '9' => 'ประเด็นยุทธ์ศาสตร์ที่ 9 การนำมหาลัยสู่ความเป็นนานาชาติ',
                          '10' => 'ประเด็นยุทธ์ศาสตร์ที่ 10 การบริหารโดยใช้หลักธรรมาภิบาล',
                          '11' => 'ประเด็นยุทธ์ศาสตร์ที่ 11 เสริมสร้างความร่วมมือเพื่อการพัฒนา'
                        ], 
                        $project->type,['class' => 'form-control']) }}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          <label for="strategy">กลยุทธ์</label>
          <select class="form-control" id="strategy" name="strategy">
            @if ($project->type == 1)
            @foreach ($project_year->education_strategy as $item)
            @if ($item == $project->strategy)
            <option value="{{ $item }}" selected>{{ $item }}</option>
            @else
            <option value="{{ $item }}">{{ $item }}</option>
            @endif
            @endforeach
            @endif
            @if ($project->type == 2)
            @foreach ($project_year->research_strategy as $item)
            @if ($item == $project->strategy)
            <option value="{{ $item }}" selected>{{ $item }}</option>
            @else
            <option value="{{ $item }}">{{ $item }}</option>
            @endif
            @endforeach
            @endif
            @if ($project->type == 3)
            @foreach ($project_year->humanresource_strategy as $item)
            @if ($item == $project->strategy)
            <option value="{{ $item }}" selected>{{ $item }}</option>
            @else
            <option value="{{ $item }}">{{ $item }}</option>
            @endif
            @endforeach
            @endif
            @if ($project->type == 4)
            @foreach ($project_year->academic_strategy as $item)
            @if ($item == $project->strategy)
            <option value="{{ $item }}" selected>{{ $item }}</option>
            @else
            <option value="{{ $item }}">{{ $item }}</option>
            @endif
            @endforeach
            @endif
            @if ($project->type == 5)
            @foreach ($project_year->organization_strategy as $item)
            @if ($item == $project->strategy)
            <option value="{{ $item }}" selected>{{ $item }}</option>
            @else
            <option value="{{ $item }}">{{ $item }}</option>
            @endif
            @endforeach
            @endif
            @if ($project->type == 6)
            @foreach ($project_year->workplace_strategy as $item)
            @if ($item == $project->strategy)
            <option value="{{ $item }}" selected>{{ $item }}</option>
            @else
            <option value="{{ $item }}">{{ $item }}</option>
            @endif
            @endforeach
            @endif
            @if ($project->type == 7)
            @foreach ($project_year->greencampus_strategy as $item)
            @if ($item == $project->strategy)
            <option value="{{ $item }}" selected>{{ $item }}</option>
            @else
            <option value="{{ $item }}">{{ $item }}</option>
            @endif
            @endforeach
            @endif
            @if ($project->type == 8)
            @foreach ($project_year->digital_strategy as $item)
            @if ($item == $project->strategy)
            <option value="{{ $item }}" selected>{{ $item }}</option>
            @else
            <option value="{{ $item }}">{{ $item }}</option>
            @endif
            @endforeach
            @endif
            @if ($project->type == 9)
            @foreach ($project_year->international_strategy as $item)
            @if ($item == $project->strategy)
            <option value="{{ $item }}" selected>{{ $item }}</option>
            @else
            <option value="{{ $item }}">{{ $item }}</option>
            @endif
            @endforeach
            @endif
            @if ($project->type == 10)
            @foreach ($project_year->governance_strategy as $item)
            @if ($item == $project->strategy)
            <option value="{{ $item }}" selected>{{ $item }}</option>
            @else
            <option value="{{ $item }}">{{ $item }}</option>
            @endif
            @endforeach
            @endif
            @if ($project->type == 11)
            @foreach ($project_year->cooperate_strategy as $item)
            @if ($item == $project->strategy)
            <option value="{{ $item }}" selected>{{ $item }}</option>
            @else
            <option value="{{ $item }}">{{ $item }}</option>
            @endif
            @endforeach
            @endif
          </select>
        </div>
        @if ($project->type == 1)
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('indicator', 'ตัวชี้วัด') }}
          {{ Form::select('indicator', $project_year->education_indicator, $project->indicator, ['class' => 'form-control','name' => 'indicator[]','multiple' => 'multiple']) }}
        </div>
        @endif
        @if ($project->type == 2)
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('indicator', 'ตัวชี้วัด') }}
          {{ Form::select('indicator', $project_year->research_indicator, $project->indicator, ['class' => 'form-control','name' => 'indicator[]','multiple' => 'multiple']) }}
        </div>
        @endif
        @if ($project->type == 3)
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('indicator', 'ตัวชี้วัด') }}
          {{ Form::select('indicator', $project_year->humanresource_indicator, $project->indicator, ['class' => 'form-control','name' => 'indicator[]','multiple' => 'multiple']) }}
        </div>
        @endif
        @if ($project->type == 4)
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('indicator', 'ตัวชี้วัด') }}
          {{ Form::select('indicator', $project_year->academic_indicator, $project->indicator, ['class' => 'form-control','name' => 'indicator[]','multiple' => 'multiple']) }}
        </div>
        @endif
        @if ($project->type == 5)
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('indicator', 'ตัวชี้วัด') }}
          {{ Form::select('indicator', $project_year->organization_indicator, $project->indicator, ['class' => 'form-control','name' => 'indicator[]','multiple' => 'multiple']) }}
        </div>
        @endif
        @if ($project->type == 6)
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('indicator', 'ตัวชี้วัด') }}
          {{ Form::select('indicator', $project_year->workplace_indicator, $project->indicator, ['class' => 'form-control','name' => 'indicator[]','multiple' => 'multiple']) }}
        </div>
        @endif
        @if ($project->type == 7)
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('indicator', 'ตัวชี้วัด') }}
          {{ Form::select('indicator', $project_year->greencampus_indicator, $project->indicator, ['class' => 'form-control','name' => 'indicator[]','multiple' => 'multiple']) }}
        </div>
        @endif
        @if ($project->type == 8)
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('indicator', 'ตัวชี้วัด') }}
          {{ Form::select('indicator', $project_year->digital_indicator, $project->indicator, ['class' => 'form-control','name' => 'indicator[]','multiple' => 'multiple']) }}
        </div>
        @endif
        @if ($project->type == 9)
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('indicator', 'ตัวชี้วัด') }}
          {{ Form::select('indicator', $project_year->international_indicator, $project->indicator, ['class' => 'form-control','name' => 'indicator[]','multiple' => 'multiple']) }}
        </div>
        @endif
        @if ($project->type == 10)
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('indicator', 'ตัวชี้วัด') }}
          {{ Form::select('indicator', $project_year->governance_indicator, $project->indicator, ['class' => 'form-control','name' => 'indicator[]','multiple' => 'multiple']) }}
        </div>
        @endif
        @if ($project->type == 11)
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('indicator', 'ตัวชี้วัด') }}
          {{ Form::select('indicator', $project_year->cooperate_indicator, $project->indicator, ['class' => 'form-control','name' => 'indicator[]','multiple' => 'multiple']) }}
        </div>
        @endif
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('user_id' ,'ผู้รับผิดชอบโครงการ') }}
          {{ Form::select('user_id',$users, null,['class' => 'form-control']) }}
        </div>
        <div class="col-lg-8 mx-auto text-center mt-3">
          {{ Form::input('submit', null, 'บันทึก', ['class' => 'btn btn-primary px-5'])}}
        </div>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
  <div class="col-lg-10 col-xl-8 mb-3 mx-auto">
    <div class="card p-3">
      {!! Form::open(['action' => ['ProjectController@destroy', $project->id], 'method' => 'delete']) !!}
      <div class="col-lg-8 mx-auto text-center">
        <h4 class="text-danger">ลบโครงการ <b>{{$project->name}}</b></h4>
        {{ Form::input('hidden', 'name', $project->name)}}
        {{ Form::input('submit', null, 'ลบ', ['class' => 'btn btn-danger px-5'])}}
      </div>
    </div>
    {!! Form::close() !!}
  </div>
</div>
@endsection

@section('custom_script')
<script>
  var projectYearDetail = {!! json_encode($project_year)!!}
  $(document).ready(() => {
    $('#indicator').select2({ theme: "bootstrap4" });
  })
  $('#type').change(function () {
    $('#indicator').empty();
    $('#strategy').empty();
    if (this.value == 1) {
      for (let value in projectYearDetail['education_indicator']) {
        $('#indicator').append(`<option value="${value}">${value}</option>`)
      }
      projectYearDetail['education_strategy'].forEach(function (item) {
        $('#strategy').append(`<option value="${item}">${item}</option>`)
      });
    } else if (this.value == 2) {
      for (let value in projectYearDetail['research_indicator']) {
        $('#indicator').append(`<option value="${value}">${value}</option>`)
      }
      projectYearDetail['research_strategy'].forEach(function (item) {
        $('#strategy').append(`<option value="${item}">${item}</option>`)
      });
    } else if (this.value == 3) {
      for (let value in projectYearDetail['humanresource_indicator']) {
        $('#indicator').append(`<option value="${value}">${value}</option>`)
      }
      projectYearDetail['humanresource_strategy'].forEach(function (item) {
        $('#strategy').append(`<option value="${item}">${item}</option>`)
      });
    } else if (this.value == 4) {
      for (let value in projectYearDetail['academic_indicator']) {
        $('#indicator').append(`<option value="${value}">${value}</option>`)
      }
      projectYearDetail['academic_strategy'].forEach(function (item) {
        $('#strategy').append(`<option value="${item}">${item}</option>`)
      });
    } else if (this.value == 5) {
      for (let value in projectYearDetail['organization_indicator']) {
        $('#indicator').append(`<option value="${value}">${value}</option>`)
      }
      projectYearDetail['organization_strategy'].forEach(function (item) {
        $('#strategy').append(`<option value="${item}">${item}</option>`)
      });
    } else if (this.value == 6) {
      for (let value in projectYearDetail['workplace_indicator']) {
        $('#indicator').append(`<option value="${value}">${value}</option>`)
      }
      projectYearDetail['workplace_strategy'].forEach(function (item) {
        $('#strategy').append(`<option value="${item}">${item}</option>`)
      });
    } else if (this.value == 7) {
      for (let value in projectYearDetail['greencampus_indicator']) {
        $('#indicator').append(`<option value="${value}">${value}</option>`)
      }
      projectYearDetail['greencampus_strategy'].forEach(function (item) {
        $('#strategy').append(`<option value="${item}">${item}</option>`)
      });
    } else if (this.value == 8) {
      for (let value in projectYearDetail['digital_indicator']) {
        $('#indicator').append(`<option value="${value}">${value}</option>`)
      }
      projectYearDetail['digital_strategy'].forEach(function (item) {
        $('#strategy').append(`<option value="${item}">${item}</option>`)
      });
    } else if (this.value == 9) {
      for (let value in projectYearDetail['international_indicator']) {
        $('#indicator').append(`<option value="${value}">${value}</option>`)
      }
      projectYearDetail['international_strategy'].forEach(function (item) {
        $('#strategy').append(`<option value="${item}">${item}</option>`)
      });
    } else if (this.value == 10) {
      for (let value in projectYearDetail['governance_indicator']) {
        $('#indicator').append(`<option value="${value}">${value}</option>`)
      }
      projectYearDetail['governance_strategy'].forEach(function (item) {
        $('#strategy').append(`<option value="${item}">${item}</option>`)
      });
    } else if (this.value == 11) {
      for (let value in projectYearDetail['cooperate_indicator']) {
        $('#indicator').append(`<option value="${value}">${value}</option>`)
      }
      projectYearDetail['cooperate_strategy'].forEach(function (item) {
        $('#strategy').append(`<option value="${item}">${item}</option>`)
      });
    }
  })
</script>
@endsection