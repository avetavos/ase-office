@extends('layouts.main')
@section('content')
<div class="row p-3">
    <div class="col-12">
        <div class="row">
            <div class="col-lg-6">
                <h3 class="mb-md-0 mb-3">โครงการทั้งหมด</h3>
            </div>
            <div class="col-lg-6 text-right">
                {!! Form::open(['id' => 'project_years_form', 'method' => 'get']) !!} {{ Form::label('year', 'ปีการศึกษา', ['class' =>
                      'sr-only']) }}
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">ปีโครงการ</div>
                    </div>
                    {{ Form::select('year', $years, $current_year, ['class' => 'form-control', 'onchange' => 'this.form.submit()']) }}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <hr>
        @include('inc.alert')
    </div>
    <div class="col-lg-10 mx-auto">
        <div class="col-12 d-flex align-items-center mx-0 justify-content-around justify-content-md-end mb-3 px-0">
            <div class="px-2">
                <i class="fa fa-circle text-success"></i><span> ดำเนินการแล้ว</span>
            </div>
            <div class="px-2">
                <i class="fa fa-circle text-warning"></i><span> กำลังดำเนินการ</span>
            </div>
            <div class="px-2">
                <i class="fa fa-circle text-danger"></i><span> ยังไม่ดำเนินการ</span>
            </div>
        </div>
        <div class="card shadow mb-3">
            <div class="card-header bg-kku text-light">
                <h5 class="card-title mb-0">
                    ประเด็นยุทธศาสตร์ 1 : ปรับเปลี่ยนการจัดการศึกษา
                </h5>
            </div>
            <div class="card-body">
                @if (count($education) > 0)
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th scope="row" class="text-center">ลำดับ</th>
                                <th class="text-center">ชื่อโครงการ</th>
                                <th class="text-center">ตัวชี้วัด</th>
                                <th class="text-center">กลยุทธ์</th>
                                <th class="text-center">ผู้รับผิดชอบ</th>
                                <th class="text-center">แผนการดำเนินการ</th>
                                <th class="text-center">ผลการดำเนินการ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($education as $key=>$value)
                            @if ($value->processing_file == '' && $value->processed_file == '')
                            <tr class="table-danger">
                                @elseif ($value->processing_file != '' && $value->processed_file == '')
                            <tr class="table-warning">
                                @else
                            <tr class="table-success">
                                @endif
                                <th scope="row" class="text-center">{{$key + 1}}</td>
                                <td>{{$value->name}}</td>
                                <td>
                                    <ul class="mb-0 pl-3">
                                        @foreach ($value->indicator as $item)
                                        <li>{{$item}}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>{{$value->strategy}}</td>
                                <td class="text-center">{{$value->authorize}}</td>
                                @if($value->user_id == Auth::user()->id)
                                <td class="text-center">
                                    @if ($value->processing_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processing_file_uploads/'.$value['processing_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                    {!! Form::open(
                                    ['action' => ['ProjectController@uploadProcessingFile',$value->id],
                                    'method' => 'put', 'id' => 'form_3_'.$value->id,
                                    'enctype' => 'multipart/form-data', 'class' => 'd-md-inline'
                                    ]) !!}
                                    <label for="processing_file{{$value->id}}" class="btn btn-primary mb-0"><i
                                            class="fas fa-file-upload"></i></label>
                                    {{ Form::input(
                                        'file', 'processing_file', null, 
                                        ['class' => 'd-none', 'onchange' => 'submitParent(`form_3_'.$value->id.'`,`processing_file'.$value->id.'`)',
                                        'id' => 'processing_file'.$value->id])}}
                                    {{ Form::input('hidden', 'path_url', '/project_user/index', ['class' => 'old-path'])}}
                                    {!! Form::close() !!}
                                </td>@else
                                <td class="text-center">
                                    @if ($value->processing_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processing_file_uploads/'.$value['processing_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                </td>
                                @endif
                                @if($value->user_id == Auth::user()->id)
                                <td class="text-center">
                                    @if ($value->processed_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processed_file_upload/'.$value['processed_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                    {!! Form::open(
                                    ['action' => ['ProjectController@uploadProcessedFile',$value->id],
                                    'method' => 'put', 'id' => 'form_5_'.$value->id,
                                    'enctype' => 'multipart/form-data', 'class' => 'd-md-inline'
                                    ]) !!}
                                    <label for="processed_file{{$value->id}}" class="btn btn-primary mb-0"><i
                                            class="fas fa-file-upload"></i></label>
                                    {{ Form::input(
                                          'file', 'processed_file', null, 
                                          ['class' => 'd-none', 'onchange' => 'submitParent(`form_5_'.$value->id.'`,`processed_file'.$value->id.'`)',
                                          'id' => 'processed_file'.$value->id])}}
                                    {{ Form::input('hidden', 'path_url', '/project_user/index', ['class' => 'old-path'])}}
                                    {!! Form::close() !!}
                                </td>@else
                                <td class="text-center">
                                    @if ($value->processed_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processed_file_uploads/'.$value['processed_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <div class="row">
                    <div class="col-12 text-center">
                        <h4>ยังไม่มีโครงการ</h4>
                    </div>
                </div>
                @endif
            </div>
        </div>

        <div class="card shadow mb-3">
            <div class="card-header bg-kku text-light">
                <h5 class="card-title mb-0">
                    ประเด็นยุทธศาสตร์ 2 : ปรับเปลี่ยนการทำงานวิจัย
                </h5>
            </div>
            <div class="card-body">
                @if (count($research) > 0)
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th scope="row" class="text-center">ลำดับ</th>
                                <th class="text-center">ชื่อโครงการ</th>
                                <th class="text-center">ตัวชี้วัด</th>
                                <th class="text-center">กลยุทธ์</th>
                                <th class="text-center">ผู้รับผิดชอบ</th>
                                <th class="text-center">แผนการดำเนินการ</th>
                                <th class="text-center">ผลการดำเนินการ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($research as $key=>$value)
                            @if ($value->processing_file == '' && $value->processed_file == '')
                            <tr class="table-danger">
                                @elseif ($value->processing_file != '' && $value->processed_file == '')
                            <tr class="table-warning">
                                @else
                            <tr class="table-success">
                                @endif
                                <th scope="row" class="text-center">{{$key + 1}}</td>
                                <td>{{$value->name}}</td>
                                <td>
                                    <ul class="mb-0 pl-3">
                                        @foreach ($value->indicator as $item)
                                        <li>{{$item}}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>{{$value->strategy}}</td>
                                <td class="text-center">{{$value->authorize}}</td>
                                @if($value->user_id == Auth::user()->id)
                                <td class="text-center">
                                    @if ($value->processing_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processing_file_uploads/'.$value['processing_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                    {!! Form::open(
                                    ['action' => ['ProjectController@uploadProcessingFile',$value->id],
                                    'method' => 'put', 'id' => 'form_3_'.$value->id,
                                    'enctype' => 'multipart/form-data', 'class' => 'd-md-inline'
                                    ]) !!}
                                    <label for="processing_file{{$value->id}}" class="btn btn-primary mb-0"><i
                                            class="fas fa-file-upload"></i></label>
                                    {{ Form::input(
                                        'file', 'processing_file', null, 
                                        ['class' => 'd-none', 'onchange' => 'submitParent(`form_3_'.$value->id.'`,`processing_file'.$value->id.'`)',
                                        'id' => 'processing_file'.$value->id])}}
                                    {{ Form::input('hidden', 'path_url', '/project_user/index', ['class' => 'old-path'])}}
                                    {!! Form::close() !!}
                                </td>@else
                                <td class="text-center">
                                    @if ($value->processing_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processing_file_uploads/'.$value['processing_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                </td>
                                @endif

                                @if($value->user_id == Auth::user()->id)
                                <td class="text-center">
                                    @if ($value->processed_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processed_file_uploads/'.$value['processed_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                    {!! Form::open(
                                    ['action' => ['ProjectController@uploadProcessedFile',$value->id],
                                    'method' => 'put', 'id' => 'form_5_'.$value->id,
                                    'enctype' => 'multipart/form-data', 'class' => 'd-md-inline'
                                    ]) !!}
                                    <label for="processed_file{{$value->id}}" class="btn btn-primary mb-0"><i
                                            class="fas fa-file-upload"></i></label>
                                    {{ Form::input(
                                          'file', 'processed_file', null, 
                                          ['class' => 'd-none', 'onchange' => 'submitParent(`form_5_'.$value->id.'`,`processed_file'.$value->id.'`)',
                                          'id' => 'processed_file'.$value->id])}}
                                    {{ Form::input('hidden', 'path_url', '/project_user/index', ['class' => 'old-path'])}}
                                    {!! Form::close() !!}
                                </td>@else
                                <td class="text-center">
                                    @if ($value->processed_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processed_file_uploads/'.$value['processed_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <div class="row">
                    <div class="col-12 text-center">
                        <h4>ยังไม่มีโครงการ</h4>
                    </div>
                </div>
                @endif
            </div>
        </div>

        <div class="card shadow mb-3">
            <div class="card-header bg-kku text-light">
                <h5 class="card-title mb-0">
                    ประเด็นยุทธศาสตร์ 3 : ปรับเปลี่ยนการบริหารทรัพยากรบุคคล
                </h5>
            </div>
            <div class="card-body">
                @if (count($humanresource) > 0)
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th scope="row" class="text-center">ลำดับ</th>
                                <th class="text-center">ชื่อโครงการ</th>
                                <th class="text-center">ตัวชี้วัด</th>
                                <th class="text-center">กลยุทธ์</th>
                                <th class="text-center">ผู้รับผิดชอบ</th>
                                <th class="text-center">แผนการดำเนินการ</th>
                                <th class="text-center">ผลการดำเนินการ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($humanresource as $key=>$value)
                            @if ($value->processing_file == '' && $value->processed_file == '')
                            <tr class="table-danger">
                                @elseif ($value->processing_file != '' && $value->processed_file == '')
                            <tr class="table-warning">
                                @else
                            <tr class="table-success">
                                @endif
                                <th scope="row" class="text-center">{{$key + 1}}</td>
                                <td>{{$value->name}}</td>
                                <td>
                                    <ul class="mb-0 pl-3">
                                        @foreach ($value->indicator as $item)
                                        <li>{{$item}}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>{{$value->strategy}}</td>
                                <td class="text-center">{{$value->authorize}}</td>
                                @if($value->user_id == Auth::user()->id)
                                <td class="text-center">
                                    @if ($value->processing_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processing_file_uploads/'.$value['processing_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                    {!! Form::open(
                                    ['action' => ['ProjectController@uploadProcessingFile',$value->id],
                                    'method' => 'put', 'id' => 'form_3_'.$value->id,
                                    'enctype' => 'multipart/form-data', 'class' => 'd-md-inline'
                                    ]) !!}
                                    <label for="processing_file{{$value->id}}" class="btn btn-primary mb-0"><i
                                            class="fas fa-file-upload"></i></label>
                                    {{ Form::input(
                                        'file', 'processing_file', null, 
                                        ['class' => 'd-none', 'onchange' => 'submitParent(`form_3_'.$value->id.'`,`processing_file'.$value->id.'`)',
                                        'id' => 'processing_file'.$value->id])}}
                                    {{ Form::input('hidden', 'path_url', '/project_user/index', ['class' => 'old-path'])}}
                                    {!! Form::close() !!}
                                </td>@else
                                <td class="text-center">
                                    @if ($value->processing_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processing_file_uploads/'.$value['processing_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                </td>
                                @endif

                                @if($value->user_id == Auth::user()->id)
                                <td class="text-center">
                                    @if ($value->processed_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processed_file_uploads/'.$value['processed_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                    {!! Form::open(
                                    ['action' => ['ProjectController@uploadProcessedFile',$value->id],
                                    'method' => 'put', 'id' => 'form_5_'.$value->id,
                                    'enctype' => 'multipart/form-data', 'class' => 'd-md-inline'
                                    ]) !!}
                                    <label for="processed_file{{$value->id}}" class="btn btn-primary mb-0"><i
                                            class="fas fa-file-upload"></i></label>
                                    {{ Form::input(
                                          'file', 'processed_file', null, 
                                          ['class' => 'd-none', 'onchange' => 'submitParent(`form_5_'.$value->id.'`,`processed_file'.$value->id.'`)',
                                          'id' => 'processed_file'.$value->id])}}
                                    {{ Form::input('hidden', 'path_url', '/project_user/index', ['class' => 'old-path'])}}
                                    {!! Form::close() !!}
                                </td>@else
                                <td class="text-center">
                                    @if ($value->processed_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processed_file_uploads/'.$value['processed_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <div class="row">
                    <div class="col-12 text-center">
                        <h4>ยังไม่มีโครงการ</h4>
                    </div>
                </div>
                @endif
            </div>
        </div>

        <div class="card shadow mb-3">
            <div class="card-header bg-kku text-light">
                <h5 class="card-title mb-0">
                    ประเด็นยุทธศาสตร์ 4 : ปรับเปลี่ยนการบริการวิชาการ
                </h5>
            </div>
            <div class="card-body">
                @if (count($academic) > 0)
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th scope="row" class="text-center">ลำดับ</th>
                                <th class="text-center">ชื่อโครงการ</th>
                                <th class="text-center">ตัวชี้วัด</th>
                                <th class="text-center">กลยุทธ์</th>
                                <th class="text-center">ผู้รับผิดชอบ</th>
                                <th class="text-center">แผนการดำเนินการ</th>
                                <th class="text-center">ผลการดำเนินการ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($academic as $key=>$value)
                            @if ($value->processing_file == '' && $value->processed_file == '')
                            <tr class="table-danger">
                                @elseif ($value->processing_file != '' && $value->processed_file == '')
                            <tr class="table-warning">
                                @else
                            <tr class="table-success">
                                @endif
                                <th scope="row" class="text-center">{{$key + 1}}</td>
                                <td>{{$value->name}}</td>
                                <td>
                                    <ul class="mb-0 pl-3">
                                        @foreach ($value->indicator as $item)
                                        <li>{{$item}}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>{{$value->strategy}}</td>
                                <td class="text-center">{{$value->authorize}}</td>
                                @if($value->user_id == Auth::user()->id)
                                <td class="text-center">
                                    @if ($value->processing_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processing_file_uploads/'.$value['processing_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                    {!! Form::open(
                                    ['action' => ['ProjectController@uploadProcessingFile',$value->id],
                                    'method' => 'put', 'id' => 'form_3_'.$value->id,
                                    'enctype' => 'multipart/form-data', 'class' => 'd-md-inline'
                                    ]) !!}
                                    <label for="processing_file{{$value->id}}" class="btn btn-primary mb-0"><i
                                            class="fas fa-file-upload"></i></label>
                                    {{ Form::input(
                                        'file', 'processing_file', null, 
                                        ['class' => 'd-none', 'onchange' => 'submitParent(`form_3_'.$value->id.'`,`processing_file'.$value->id.'`)',
                                        'id' => 'processing_file'.$value->id])}}
                                    {{ Form::input('hidden', 'path_url', '/project_user/index', ['class' => 'old-path'])}}
                                    {!! Form::close() !!}
                                </td>@else
                                <td class="text-center">
                                    @if ($value->processing_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processing_file_uploads/'.$value['processing_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                </td>
                                @endif

                                @if($value->user_id == Auth::user()->id)
                                <td class="text-center">
                                    @if ($value->processed_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processed_file_uploads/'.$value['processed_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                    {!! Form::open(
                                    ['action' => ['ProjectController@uploadProcessedFile',$value->id],
                                    'method' => 'put', 'id' => 'form_5_'.$value->id,
                                    'enctype' => 'multipart/form-data', 'class' => 'd-md-inline'
                                    ]) !!}
                                    <label for="processed_file{{$value->id}}" class="btn btn-primary mb-0"><i
                                            class="fas fa-file-upload"></i></label>
                                    {{ Form::input(
                                          'file', 'processed_file', null, 
                                          ['class' => 'd-none', 'onchange' => 'submitParent(`form_5_'.$value->id.'`,`processed_file'.$value->id.'`)',
                                          'id' => 'processed_file'.$value->id])}}
                                    {{ Form::input('hidden', 'path_url', '/project_user/index', ['class' => 'old-path'])}}
                                    {!! Form::close() !!}
                                </td>@else
                                <td class="text-center">
                                    @if ($value->processed_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processed_file_uploads/'.$value['processed_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <div class="row">
                    <div class="col-12 text-center">
                        <h4>ยังไม่มีโครงการ</h4>
                    </div>
                </div>
                @endif
            </div>
        </div>

        <div class="card shadow mb-3">
            <div class="card-header bg-kku text-light">
                <h5 class="card-title mb-0">
                    ประเด็นยุทธศาสตร์ 5: ปรับเปลี่ยนการบริหารจัดการองค์กร
                </h5>
            </div>
            <div class="card-body">
                @if (count($organization) > 0)
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th scope="row" class="text-center">ลำดับ</th>
                                <th class="text-center">ชื่อโครงการ</th>
                                <th class="text-center">ตัวชี้วัด</th>
                                <th class="text-center">กลยุทธ์</th>
                                <th class="text-center">ผู้รับผิดชอบ</th>
                                <th class="text-center">แผนการดำเนินการ</th>
                                <th class="text-center">ผลการดำเนินการ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($organization as $key=>$value)
                            @if ($value->processing_file == '' && $value->processed_file == '')
                            <tr class="table-danger">
                                @elseif ($value->processing_file != '' && $value->processed_file == '')
                            <tr class="table-warning">
                                @else
                            <tr class="table-success">
                                @endif
                                <th scope="row" class="text-center">{{$key + 1}}</td>
                                <td>{{$value->name}}</td>
                                <td>
                                    <ul class="mb-0 pl-3">
                                        @foreach ($value->indicator as $item)
                                        <li>{{$item}}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>{{$value->strategy}}</td>
                                <td class="text-center">{{$value->authorize}}</td>
                                @if($value->user_id == Auth::user()->id)
                                <td class="text-center">
                                    @if ($value->processing_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processing_file_uploads/'.$value['processing_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                    {!! Form::open(
                                    ['action' => ['ProjectController@uploadProcessingFile',$value->id],
                                    'method' => 'put', 'id' => 'form_3_'.$value->id,
                                    'enctype' => 'multipart/form-data', 'class' => 'd-md-inline'
                                    ]) !!}
                                    <label for="processing_file{{$value->id}}" class="btn btn-primary mb-0"><i
                                            class="fas fa-file-upload"></i></label>
                                    {{ Form::input(
                                        'file', 'processing_file', null, 
                                        ['class' => 'd-none', 'onchange' => 'submitParent(`form_3_'.$value->id.'`,`processing_file'.$value->id.'`)',
                                        'id' => 'processing_file'.$value->id])}}
                                    {{ Form::input('hidden', 'path_url', '/project_user/index', ['class' => 'old-path'])}}
                                    {!! Form::close() !!}
                                </td>@else
                                <td class="text-center">
                                    @if ($value->processing_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processing_file_uploads/'.$value['processing_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                </td>
                                @endif

                                @if($value->user_id == Auth::user()->id)
                                <td class="text-center">
                                    @if ($value->processed_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processed_file_uploads/'.$value['processed_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                    {!! Form::open(
                                    ['action' => ['ProjectController@uploadProcessedFile',$value->id],
                                    'method' => 'put', 'id' => 'form_5_'.$value->id,
                                    'enctype' => 'multipart/form-data', 'class' => 'd-md-inline'
                                    ]) !!}
                                    <label for="processed_file{{$value->id}}" class="btn btn-primary mb-0"><i
                                            class="fas fa-file-upload"></i></label>
                                    {{ Form::input(
                                          'file', 'processed_file', null, 
                                          ['class' => 'd-none', 'onchange' => 'submitParent(`form_5_'.$value->id.'`,`processed_file'.$value->id.'`)',
                                          'id' => 'processed_file'.$value->id])}}
                                    {{ Form::input('hidden', 'path_url', '/project_user/index', ['class' => 'old-path'])}}
                                    {!! Form::close() !!}
                                </td>@else
                                <td class="text-center">
                                    @if ($value->processed_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processed_file_uploads/'.$value['processed_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <div class="row">
                    <div class="col-12 text-center">
                        <h4>ยังไม่มีโครงการ</h4>
                    </div>
                </div>
                @endif
            </div>
        </div>

        <div class="card shadow mb-3">
            <div class="card-header bg-kku text-light">
                <h5 class="card-title mb-0">
                    ประเด็นยุทธศาสตร์ 6: สร้างมหาวิทยาลัยขอนแก่นให้เป็นที่น่าทำงาน
                </h5>
            </div>
            <div class="card-body">
                @if (count($workplace) > 0)
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th scope="row" class="text-center">ลำดับ</th>
                                <th class="text-center">ชื่อโครงการ</th>
                                <th class="text-center">ตัวชี้วัด</th>
                                <th class="text-center">กลยุทธ์</th>
                                <th class="text-center">ผู้รับผิดชอบ</th>
                                <th class="text-center">แผนการดำเนินการ</th>
                                <th class="text-center">ผลการดำเนินการ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($workplace as $key=>$value)
                            @if ($value->processing_file == '' && $value->processed_file == '')
                            <tr class="table-danger">
                                @elseif ($value->processing_file != '' && $value->processed_file == '')
                            <tr class="table-warning">
                                @else
                            <tr class="table-success">
                                @endif
                                <th scope="row" class="text-center">{{$key + 1}}</td>
                                <td>{{$value->name}}</td>
                                <td>
                                    <ul class="mb-0 pl-3">
                                        @foreach ($value->indicator as $item)
                                        <li>{{$item}}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>{{$value->strategy}}</td>
                                <td class="text-center">{{$value->authorize}}</td>
                                @if($value->user_id == Auth::user()->id)
                                <td class="text-center">
                                    @if ($value->processing_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processing_file_uploads/'.$value['processing_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                    {!! Form::open(
                                    ['action' => ['ProjectController@uploadProcessingFile',$value->id],
                                    'method' => 'put', 'id' => 'form_3_'.$value->id,
                                    'enctype' => 'multipart/form-data', 'class' => 'd-md-inline'
                                    ]) !!}
                                    <label for="processing_file{{$value->id}}" class="btn btn-primary mb-0"><i
                                            class="fas fa-file-upload"></i></label>
                                    {{ Form::input(
                                        'file', 'processing_file', null, 
                                        ['class' => 'd-none', 'onchange' => 'submitParent(`form_3_'.$value->id.'`,`processing_file'.$value->id.'`)',
                                        'id' => 'processing_file'.$value->id])}}
                                    {{ Form::input('hidden', 'path_url', '/project_user/index', ['class' => 'old-path'])}}
                                    {!! Form::close() !!}
                                </td>@else
                                <td class="text-center">
                                    @if ($value->processing_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processing_file_uploads/'.$value['processing_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                </td>
                                @endif

                                @if($value->user_id == Auth::user()->id)
                                <td class="text-center">
                                    @if ($value->processed_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processed_file_uploads/'.$value['processed_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                    {!! Form::open(
                                    ['action' => ['ProjectController@uploadProcessedFile',$value->id],
                                    'method' => 'put', 'id' => 'form_5_'.$value->id,
                                    'enctype' => 'multipart/form-data', 'class' => 'd-md-inline'
                                    ]) !!}
                                    <label for="processed_file{{$value->id}}" class="btn btn-primary mb-0"><i
                                            class="fas fa-file-upload"></i></label>
                                    {{ Form::input(
                                          'file', 'processed_file', null, 
                                          ['class' => 'd-none', 'onchange' => 'submitParent(`form_5_'.$value->id.'`,`processed_file'.$value->id.'`)',
                                          'id' => 'processed_file'.$value->id])}}
                                    {{ Form::input('hidden', 'path_url', '/project_user/index', ['class' => 'old-path'])}}
                                    {!! Form::close() !!}
                                </td>@else
                                <td class="text-center">
                                    @if ($value->processed_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processed_file_uploads/'.$value['processed_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <div class="row">
                    <div class="col-12 text-center">
                        <h4>ยังไม่มีโครงการ</h4>
                    </div>
                </div>
                @endif
            </div>
        </div>

        <div class="card shadow mb-3">
            <div class="card-header bg-kku text-light">
                <h5 class="card-title mb-0">
                    ประเด็นยุทธศาสตร์ 7: สร้างมหาวิทยาลัยให้เป็นที่น่าอยู่
                </h5>
            </div>
            <div class="card-body">
                @if (count($greencampus) > 0)
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th scope="row" class="text-center">ลำดับ</th>
                                <th class="text-center">ชื่อโครงการ</th>
                                <th class="text-center">ตัวชี้วัด</th>
                                <th class="text-center">กลยุทธ์</th>
                                <th class="text-center">ผู้รับผิดชอบ</th>
                                <th class="text-center">แผนการดำเนินการ</th>
                                <th class="text-center">ผลการดำเนินการ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($greencampus as $key=>$value)
                            @if ($value->processing_file == '' && $value->processed_file == '')
                            <tr class="table-danger">
                                @elseif ($value->processing_file != '' && $value->processed_file == '')
                            <tr class="table-warning">
                                @else
                            <tr class="table-success">
                                @endif
                                <th scope="row" class="text-center">{{$key + 1}}</td>
                                <td>{{$value->name}}</td>
                                <td>
                                    <ul class="mb-0 pl-3">
                                        @foreach ($value->indicator as $item)
                                        <li>{{$item}}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>{{$value->strategy}}</td>
                                <td class="text-center">{{$value->authorize}}</td>
                                @if($value->user_id == Auth::user()->id)
                                <td class="text-center">
                                    @if ($value->processing_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processing_file_uploads/'.$value['processing_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                    {!! Form::open(
                                    ['action' => ['ProjectController@uploadProcessingFile',$value->id],
                                    'method' => 'put', 'id' => 'form_3_'.$value->id,
                                    'enctype' => 'multipart/form-data', 'class' => 'd-md-inline'
                                    ]) !!}
                                    <label for="processing_file{{$value->id}}" class="btn btn-primary mb-0"><i
                                            class="fas fa-file-upload"></i></label>
                                    {{ Form::input(
                                        'file', 'processing_file', null, 
                                        ['class' => 'd-none', 'onchange' => 'submitParent(`form_3_'.$value->id.'`,`processing_file'.$value->id.'`)',
                                        'id' => 'processing_file'.$value->id])}}
                                    {{ Form::input('hidden', 'path_url', '/project_user/index', ['class' => 'old-path'])}}
                                    {!! Form::close() !!}
                                </td>@else
                                <td class="text-center">
                                    @if ($value->processing_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processing_file_uploads/'.$value['processing_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                </td>
                                @endif

                                @if($value->user_id == Auth::user()->id)
                                <td class="text-center">
                                    @if ($value->processed_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processed_file_uploads/'.$value['processed_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                    {!! Form::open(
                                    ['action' => ['ProjectController@uploadProcessedFile',$value->id],
                                    'method' => 'put', 'id' => 'form_5_'.$value->id,
                                    'enctype' => 'multipart/form-data', 'class' => 'd-md-inline'
                                    ]) !!}
                                    <label for="processed_file{{$value->id}}" class="btn btn-primary mb-0"><i
                                            class="fas fa-file-upload"></i></label>
                                    {{ Form::input(
                                          'file', 'processed_file', null, 
                                          ['class' => 'd-none', 'onchange' => 'submitParent(`form_5_'.$value->id.'`,`processed_file'.$value->id.'`)',
                                          'id' => 'processed_file'.$value->id])}}
                                    {{ Form::input('hidden', 'path_url', '/project_user/index', ['class' => 'old-path'])}}
                                    {!! Form::close() !!}
                                </td>@else
                                <td class="text-center">
                                    @if ($value->processed_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processed_file_uploads/'.$value['processed_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <div class="row">
                    <div class="col-12 text-center">
                        <h4>ยังไม่มีโครงการ</h4>
                    </div>
                </div>
                @endif
            </div>
        </div>

        <div class="card shadow mb-3">
            <div class="card-header bg-kku text-light">
                <h5 class="card-title mb-0">
                    ประเด็นยุทธศาสตร์ 8: ปรับเปลี่ยนองค์กรให้ก้าวสู่ยุคดิจิทัล
                </h5>
            </div>
            <div class="card-body">
                @if (count($digital) > 0)
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th scope="row" class="text-center">ลำดับ</th>
                                <th class="text-center">ชื่อโครงการ</th>
                                <th class="text-center">ตัวชี้วัด</th>
                                <th class="text-center">กลยุทธ์</th>
                                <th class="text-center">ผู้รับผิดชอบ</th>
                                <th class="text-center">แผนการดำเนินการ</th>
                                <th class="text-center">ผลการดำเนินการ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($digital as $key=>$value)
                            @if ($value->processing_file == '' && $value->processed_file == '')
                            <tr class="table-danger">
                                @elseif ($value->processing_file != '' && $value->processed_file == '')
                            <tr class="table-warning">
                                @else
                            <tr class="table-success">
                                @endif
                                <th scope="row" class="text-center">{{$key + 1}}</td>
                                <td>{{$value->name}}</td>
                                <td>
                                    <ul class="mb-0 pl-3">
                                        @foreach ($value->indicator as $item)
                                        <li>{{$item}}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>{{$value->strategy}}</td>
                                <td class="text-center">{{$value->authorize}}</td>
                                @if($value->user_id == Auth::user()->id)
                                <td class="text-center">
                                    @if ($value->processing_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processing_file_uploads/'.$value['processing_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                    {!! Form::open(
                                    ['action' => ['ProjectController@uploadProcessingFile',$value->id],
                                    'method' => 'put', 'id' => 'form_3_'.$value->id,
                                    'enctype' => 'multipart/form-data', 'class' => 'd-md-inline'
                                    ]) !!}
                                    <label for="processing_file{{$value->id}}" class="btn btn-primary mb-0"><i
                                            class="fas fa-file-upload"></i></label>
                                    {{ Form::input(
                                        'file', 'processing_file', null, 
                                        ['class' => 'd-none', 'onchange' => 'submitParent(`form_3_'.$value->id.'`,`processing_file'.$value->id.'`)',
                                        'id' => 'processing_file'.$value->id])}}
                                    {{ Form::input('hidden', 'path_url', '/project_user/index', ['class' => 'old-path'])}}
                                    {!! Form::close() !!}
                                </td>@else
                                <td class="text-center">
                                    @if ($value->processing_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processing_file_uploads/'.$value['processing_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                </td>
                                @endif

                                @if($value->user_id == Auth::user()->id)
                                <td class="text-center">
                                    @if ($value->processed_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processed_file_uploads/'.$value['processed_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                    {!! Form::open(
                                    ['action' => ['ProjectController@uploadProcessedFile',$value->id],
                                    'method' => 'put', 'id' => 'form_5_'.$value->id,
                                    'enctype' => 'multipart/form-data', 'class' => 'd-md-inline'
                                    ]) !!}
                                    <label for="processed_file{{$value->id}}" class="btn btn-primary mb-0"><i
                                            class="fas fa-file-upload"></i></label>
                                    {{ Form::input(
                                          'file', 'processed_file', null, 
                                          ['class' => 'd-none', 'onchange' => 'submitParent(`form_5_'.$value->id.'`,`processed_file'.$value->id.'`)',
                                          'id' => 'processed_file'.$value->id])}}
                                    {{ Form::input('hidden', 'path_url', '/project_user/index', ['class' => 'old-path'])}}
                                    {!! Form::close() !!}
                                </td>@else
                                <td class="text-center">
                                    @if ($value->processed_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processed_file_uploads/'.$value['processed_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <div class="row">
                    <div class="col-12 text-center">
                        <h4>ยังไม่มีโครงการ</h4>
                    </div>
                </div>
                @endif
            </div>
        </div>

        <div class="card shadow mb-3">
            <div class="card-header bg-kku text-light">
                <h5 class="card-title mb-0">
                    ประเด็นยุทธศาสตร์ 9: การนำมหาลัยสู่ความเป็นนานาชาติ
                </h5>
            </div>
            <div class="card-body">
                @if (count($international) > 0)
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th scope="row" class="text-center">ลำดับ</th>
                                <th class="text-center">ชื่อโครงการ</th>
                                <th class="text-center">ตัวชี้วัด</th>
                                <th class="text-center">กลยุทธ์</th>
                                <th class="text-center">ผู้รับผิดชอบ</th>
                                <th class="text-center">แผนการดำเนินการ</th>
                                <th class="text-center">ผลการดำเนินการ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($international as $key=>$value)
                            @if ($value->processing_file == '' && $value->processed_file == '')
                            <tr class="table-danger">
                                @elseif ($value->processing_file != '' && $value->processed_file == '')
                            <tr class="table-warning">
                                @else
                            <tr class="table-success">
                                @endif
                                <th scope="row" class="text-center">{{$key + 1}}</td>
                                <td>{{$value->name}}</td>
                                <td>
                                    <ul class="mb-0 pl-3">
                                        @foreach ($value->indicator as $item)
                                        <li>{{$item}}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>{{$value->strategy}}</td>
                                <td class="text-center">{{$value->authorize}}</td>
                                @if($value->user_id == Auth::user()->id)
                                <td class="text-center">
                                    @if ($value->processing_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processing_file_uploads/'.$value['processing_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                    {!! Form::open(
                                    ['action' => ['ProjectController@uploadProcessingFile',$value->id],
                                    'method' => 'put', 'id' => 'form_3_'.$value->id,
                                    'enctype' => 'multipart/form-data', 'class' => 'd-md-inline'
                                    ]) !!}
                                    <label for="processing_file{{$value->id}}" class="btn btn-primary mb-0"><i
                                            class="fas fa-file-upload"></i></label>
                                    {{ Form::input(
                                        'file', 'processing_file', null, 
                                        ['class' => 'd-none', 'onchange' => 'submitParent(`form_3_'.$value->id.'`,`processing_file'.$value->id.'`)',
                                        'id' => 'processing_file'.$value->id])}}
                                    {{ Form::input('hidden', 'path_url', '/project_user/index', ['class' => 'old-path'])}}
                                    {!! Form::close() !!}
                                </td>@else
                                <td class="text-center">
                                    @if ($value->processing_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processing_file_uploads/'.$value['processing_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                </td>
                                @endif

                                @if($value->user_id == Auth::user()->id)
                                <td class="text-center">
                                    @if ($value->processed_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processed_file_uploads/'.$value['processed_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                    {!! Form::open(
                                    ['action' => ['ProjectController@uploadProcessedFile',$value->id],
                                    'method' => 'put', 'id' => 'form_5_'.$value->id,
                                    'enctype' => 'multipart/form-data', 'class' => 'd-md-inline'
                                    ]) !!}
                                    <label for="processed_file{{$value->id}}" class="btn btn-primary mb-0"><i
                                            class="fas fa-file-upload"></i></label>
                                    {{ Form::input(
                                          'file', 'processed_file', null, 
                                          ['class' => 'd-none', 'onchange' => 'submitParent(`form_5_'.$value->id.'`,`processed_file'.$value->id.'`)',
                                          'id' => 'processed_file'.$value->id])}}
                                    {{ Form::input('hidden', 'path_url', '/project_user/index', ['class' => 'old-path'])}}
                                    {!! Form::close() !!}
                                </td>@else
                                <td class="text-center">
                                    @if ($value->processed_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processed_file_uploads/'.$value['processed_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <div class="row">
                    <div class="col-12 text-center">
                        <h4>ยังไม่มีโครงการ</h4>
                    </div>
                </div>
                @endif
            </div>
        </div>

        <div class="card shadow mb-3">
            <div class="card-header bg-kku text-light">
                <h5 class="card-title mb-0">
                    ประเด็นยุทธศาสตร์ 10: การบริหารโดยใช้หลักธรรมาภิบาล
                </h5>
            </div>
            <div class="card-body">
                @if (count($governance) > 0)
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th scope="row" class="text-center">ลำดับ</th>
                                <th class="text-center">ชื่อโครงการ</th>
                                <th class="text-center">ตัวชี้วัด</th>
                                <th class="text-center">กลยุทธ์</th>
                                <th class="text-center">ผู้รับผิดชอบ</th>
                                <th class="text-center">แผนการดำเนินการ</th>
                                <th class="text-center">ผลการดำเนินการ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($governance as $key=>$value)
                            @if ($value->processing_file == '' && $value->processed_file == '')
                            <tr class="table-danger">
                                @elseif ($value->processing_file != '' && $value->processed_file == '')
                            <tr class="table-warning">
                                @else
                            <tr class="table-success">
                                @endif
                                <th scope="row" class="text-center">{{$key + 1}}</td>
                                <td>{{$value->name}}</td>
                                <td>
                                    <ul class="mb-0 pl-3">
                                        @foreach ($value->indicator as $item)
                                        <li>{{$item}}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>{{$value->strategy}}</td>
                                <td class="text-center">{{$value->authorize}}</td>
                                @if($value->user_id == Auth::user()->id)
                                <td class="text-center">
                                    @if ($value->processing_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processing_file_uploads/'.$value['processing_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                    {!! Form::open(
                                    ['action' => ['ProjectController@uploadProcessingFile',$value->id],
                                    'method' => 'put', 'id' => 'form_3_'.$value->id,
                                    'enctype' => 'multipart/form-data', 'class' => 'd-md-inline'
                                    ]) !!}
                                    <label for="processing_file{{$value->id}}" class="btn btn-primary mb-0"><i
                                            class="fas fa-file-upload"></i></label>
                                    {{ Form::input(
                                        'file', 'processing_file', null, 
                                        ['class' => 'd-none', 'onchange' => 'submitParent(`form_3_'.$value->id.'`,`processing_file'.$value->id.'`)',
                                        'id' => 'processing_file'.$value->id])}}
                                    {{ Form::input('hidden', 'path_url', '/project_user/index', ['class' => 'old-path'])}}
                                    {!! Form::close() !!}
                                </td>@else
                                <td class="text-center">
                                    @if ($value->processing_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processing_file_uploads/'.$value['processing_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                </td>
                                @endif

                                @if($value->user_id == Auth::user()->id)
                                <td class="text-center">
                                    @if ($value->processed_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processed_file_uploads/'.$value['processed_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                    {!! Form::open(
                                    ['action' => ['ProjectController@uploadProcessedFile',$value->id],
                                    'method' => 'put', 'id' => 'form_5_'.$value->id,
                                    'enctype' => 'multipart/form-data', 'class' => 'd-md-inline'
                                    ]) !!}
                                    <label for="processed_file{{$value->id}}" class="btn btn-primary mb-0"><i
                                            class="fas fa-file-upload"></i></label>
                                    {{ Form::input(
                                          'file', 'processed_file', null, 
                                          ['class' => 'd-none', 'onchange' => 'submitParent(`form_5_'.$value->id.'`,`processed_file'.$value->id.'`)',
                                          'id' => 'processed_file'.$value->id])}}
                                    {{ Form::input('hidden', 'path_url', '/project_user/index', ['class' => 'old-path'])}}
                                    {!! Form::close() !!}
                                </td>@else
                                <td class="text-center">
                                    @if ($value->processed_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processed_file_uploads/'.$value['processed_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <div class="row">
                    <div class="col-12 text-center">
                        <h4>ยังไม่มีโครงการ</h4>
                    </div>
                </div>
                @endif
            </div>
        </div>

        <div class="card shadow mb-3">
            <div class="card-header bg-kku text-light">
                <h5 class="card-title mb-0">
                    ประเด็นยุทธศาสตร์ 11: เสริมสร้างความร่วมมือเพื่อการพัฒนา
                </h5>
            </div>
            <div class="card-body">
                @if (count($cooperate) > 0)
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th scope="row" class="text-center">ลำดับ</th>
                                <th class="text-center">ชื่อโครงการ</th>
                                <th class="text-center">ตัวชี้วัด</th>
                                <th class="text-center">กลยุทธ์</th>
                                <th class="text-center">ผู้รับผิดชอบ</th>
                                <th class="text-center">แผนการดำเนินการ</th>
                                <th class="text-center">ผลการดำเนินการ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($cooperate as $key=>$value)
                            @if ($value->processing_file == '' && $value->processed_file == '')
                            <tr class="table-danger">
                                @elseif ($value->processing_file != '' && $value->processed_file == '')
                            <tr class="table-warning">
                                @else
                            <tr class="table-success">
                                @endif
                                <th scope="row" class="text-center">{{$key + 1}}</td>
                                <td>{{$value->name}}</td>
                                <td>
                                    <ul class="mb-0 pl-3">
                                        @foreach ($value->indicator as $item)
                                        <li>{{$item}}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>{{$value->strategy}}</td>
                                <td class="text-center">{{$value->authorize}}</td>
                                @if($value->user_id == Auth::user()->id)
                                <td class="text-center">
                                    @if ($value->processing_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processing_file_uploads/'.$value['processing_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                    {!! Form::open(
                                    ['action' => ['ProjectController@uploadProcessingFile',$value->id],
                                    'method' => 'put', 'id' => 'form_3_'.$value->id,
                                    'enctype' => 'multipart/form-data', 'class' => 'd-md-inline'
                                    ]) !!}
                                    <label for="processing_file{{$value->id}}" class="btn btn-primary mb-0"><i
                                            class="fas fa-file-upload"></i></label>
                                    {{ Form::input(
                                        'file', 'processing_file', null, 
                                        ['class' => 'd-none', 'onchange' => 'submitParent(`form_3_'.$value->id.'`,`processing_file'.$value->id.'`)',
                                        'id' => 'processing_file'.$value->id])}}
                                    {{ Form::input('hidden', 'path_url', '/project_user/index', ['class' => 'old-path'])}}
                                    {!! Form::close() !!}
                                </td>@else
                                <td class="text-center">
                                    @if ($value->processing_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processing_file_uploads/'.$value['processing_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                </td>
                                @endif

                                @if($value->user_id == Auth::user()->id)
                                <td class="text-center">
                                    @if ($value->processed_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processed_file_uploads/'.$value['processed_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                    {!! Form::open(
                                    ['action' => ['ProjectController@uploadProcessedFile',$value->id],
                                    'method' => 'put', 'id' => 'form_5_'.$value->id,
                                    'enctype' => 'multipart/form-data', 'class' => 'd-md-inline'
                                    ]) !!}
                                    <label for="processed_file{{$value->id}}" class="btn btn-primary mb-0"><i
                                            class="fas fa-file-upload"></i></label>
                                    {{ Form::input(
                                          'file', 'processed_file', null, 
                                          ['class' => 'd-none', 'onchange' => 'submitParent(`form_5_'.$value->id.'`,`processed_file'.$value->id.'`)',
                                          'id' => 'processed_file'.$value->id])}}
                                    {{ Form::input('hidden', 'path_url', '/project_user/index', ['class' => 'old-path'])}}
                                    {!! Form::close() !!}
                                </td>@else
                                <td class="text-center">
                                    @if ($value->processed_file)
                                    <div class="d-md-inline">
                                        <a role="button" class="btn btn-success"
                                            href="{{URL::to('/storage/processed_file_uploads/'.$value['processed_file'])}}"
                                            target="_blank" download>
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                    </div>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <div class="row">
                    <div class="col-12 text-center">
                        <h4>ยังไม่มีโครงการ</h4>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

</div>
@endsection
@section('custom_script')
<script>
    function submitParent(formId, inputId) {
        if ($('#' + inputId)[0].files[0].size > 8000000) {
        alert(`ไฟล์มีขนาดเกิน 8MB`)
        $(`#${formId}`)[0].reset();
        } else {
        $(`#${formId}`).submit();
        }
    }
</script>
@endsection