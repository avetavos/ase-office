@extends('layouts.main')
@section('content')
<div class="row">
  <div class="col-12 px-0">
    <h3>{{$header}}</h3>
    <hr>
  </div>
  <a href="{{url('/report_survey/export/'.$survey->id)}}" class="export-excel p-3 shadow">
    <span class="d-block d-md-none"><i class="far fa-file-excel"></i></span>
    <span class="d-none d-md-block">บันทึกเป็น Excel</span>
  </a>
  <div class="col-lg-8 col-md-10 col-12 mx-auto px-0">
    <div class="card shadow">
      <div class="card-header bg-kku">
        <div class="row">
          <div class="col-12 d-flex justify-content-between">
            <h4>การตอบกลับ</h4>
            <h4>{{$count}}/{{$survey->amount}}</h4>
          </div>
        </div>
      </div>
      <div class="card-body">
        {{-- part-1 --}}
        <h5><b><u>ตอนที่ 1 ข้อมูลทั่วไปของผู้ตอบแบบสอบถาม</u></b></h5>
        <h5 class="mt-3">ปัจจุบันท่านดำรงตาแหน่ง *</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_1_1"></canvas>
        </div>
        <h5 class="mt-3">วุฒิการศึกษาสูงสุดของท่าน</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_1_2"></canvas>
        </div>
        <h5 class="mt-3">ประเภทหน่วยงานของท่าน</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_1_3"></canvas>
        </div>

        {{-- part-2 --}}
        <hr>
        <h5><b><u>ตอนที่ 2 ความคิดเห็นของท่านที่มีต่อบัณฑิต</u></b></h5>
        <h5 class="mt-3">1 ด้านคุณธรรม จริยธรรม</h5>
        <h6>(1) มีระเบียบวินัย/ ตรงต่อเวลา</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_2_1_(1)"></canvas>
        </div>
        <h6>(2) ซื่อสัตย์สุจริต</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_2_1_(2)"></canvas>
        </div>
        <h6>(3) เสียสละเห็นแก่ประโยชน์ส่วนรวม</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_2_1_(3)"></canvas>
        </div>
        <h6>(4) รัก ศรัทธาและมีจรรยาบรรณต่อวิชาชีพ</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_2_1_(4)"></canvas>
        </div>

        {{-- part-3 --}}
        <hr>
        <h5><b><u>ด้านความรู้ ความสามารถทางวิชาการ</u></b></h5>
        <h6>(1) มีองค์ความรู้ในสาขาวิชาที่เรียนและเข้าใจขั้นตอน วิธีการปฏิบัติงาน รวมถึงกฎระเบียบข้อบังคับในสาขาวิชาชีพ
        </h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_3_1_(1)"></canvas>
        </div>
        <h6>(2) มีความสามารถในการนาความรู้ประยุกต์ใช้ในการปฏิบัติงาน และมีความรอบรู้ในการเพิ่มพูนหรือต่อยอดองค์ความรู้
        </h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_3_1_(2)"></canvas>
        </div>
        <h6>(3) ความสามารถในการถ่ายทอดและเผยแพร่ความรู้</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_3_1_(3)"></canvas>
        </div>

        {{-- part-4 --}}
        <hr>
        <h5><b><u>ด้านทักษะทางปัญญา</u></b></h5>
        <h6>(1) มีความสามารถในการวางแผน การทางานอย่างมีประสิทธิภาพและเป็นระบบ</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_4_1_(1)"></canvas>
        </div>
        <h6>(2) มีความสามารถในการวิเคราะห์ แก้ไขปัญหาและการตัดสินใจ</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_4_1_(2)"></canvas>
        </div>
        <h6>(3) มีความคิดสร้างสรรค์/กล้าแสดงออกในการทางาน</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_4_1_(3)"></canvas>
        </div>
        <h6>(4) มีความเชื่อมั่นในตนเอง ทักษะความเป็นผู้นา มีวิสัยทัศน์</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_4_1_(4)"></canvas>
        </div>

        {{-- part-5 --}}
        <hr>
        <h5><b><u>ด้านทักษะความสัมพันธ์ระหว่างบุคคลและความรับผิดชอบ</u></b></h5>
        <h6>(1) มีความรับผิดชอบต่อหน้าที่</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_5_1_(1)"></canvas>
        </div>
        <h6>(2) มีน้าใจ เอื้อเฟื้อเผื่อแผ่ต่อเพื่อนร่วมงานและผู้อื่น</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_5_1_(2)"></canvas>
        </div>
        <h6>(3) มีความสามารถในการปรับตัว การทางานเป็นทีม รับฟังความคิดเห็นผู้อื่น</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_5_1_(3)"></canvas>
        </div>
        <h6>(4) มีการพัฒนาตนเองและพัฒนางาน</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_5_1_(4)"></canvas>
        </div>

        {{-- part-6 --}}
        <hr>
        <h5><b><u>ด้านทักษะการวิเคราะห์เชิงตัวเลข การสื่อสารและการใช้เทคโนโลยี</u></b></h5>
        <h6>(1) มีความรู้และทักษะในการสื่อสารภาษาต่างประเทศระดับพื้นฐาน</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_6_1_(1)"></canvas>
        </div>
        <h6>(2) มีความรู้และทักษะในการใช้คอมพิวเตอร์ และเทคโนโลยีสารสนเทศที่ทันสมัย</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_6_1_(2)"></canvas>
        </div>
        <h6>(3) มีความสามารถในการรวบรวม วิเคราะห์ ประมวลผล ข้อมูลสารสนเทศ</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_6_1_(3)"></canvas>
        </div>
        <h6>(4) สามารถประยุกต์ใช้เทคนิคทางสถิติหรือคณิตศาสตร์ ในการวิเคราะห์ข้อมูลในการทางาน</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_6_1_(4)"></canvas>
        </div>

        {{-- part-7 --}}
        <hr>
        <h5><b><u>ตอนที่ 3 ข้อเสนอแนะ</u></b></h5>
        <h5>ท่านคิดว่าหลักสูตรของมหาวิทยาลัย
          ควรเพิ่มทักษะหรือความรู้เรื่องใดที่จะเอื้อประโยชน์ต่อการทางานของบัณฑิตมากยิ่งขึ้น
          (ตอบได้มากกว่า 1 ข้อ)</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_7_1"></canvas>
        </div>
        <h5 class="mt-3">ข้อเสนอแนะเกี่ยวกับการพัฒนาหลักสูตร</h5>
        <div class="col-12" id="chart_7_2"></div>
      </div>
    </div>
  </div>
  @endsection

  @section('custom_script')
  @include('report_survey.chart_render')
  <script>
    var surveyData = {!! $surveyData !!};
  var surveyChoice = {
    "1_1": ["ผู้บริหารระดับสูง", "ผู้บริหารระดับกลาง","ผู้บริหารระดับต้น","อื่นๆ"],
    "1_2": ["ต่ากว่าปริญญาตรี", "ปริญญาตรี", "ปริญญาโท", "ปริญญาเอก"],
    "1_3": ["ส่วนรัฐบาล", "รัฐวิสาหกิจ", "บริษัทเอกชน", "อื่นๆ"],
    "7_1": ["ภาษาอังกฤษ","คอมพิวเตอร์","ทักษะการคานวณ","การค้นคว้าหาข้อมูล","การลงมือปฏิบัติจริง","เทคนิคการวิจัย","ทักษะการวิเคราะห์ปัญหา","ทักษะความคิดสร้างสรรค์","ทักษะการพูดในที่ชุมชน","อื่นๆ"],
  }
  Chart.defaults.global.legend.display = false;
  window.onload = function () {
      pieChartRender('chart_1_1', surveyData['1_1'], surveyChoice["1_1"]);
      pieChartRender('chart_1_2', surveyData['1_2'], surveyChoice["1_2"]);
      pieChartRender('chart_1_3', surveyData['1_3'], surveyChoice["1_3"]);

      horizontalBarChartRender('chart_2_1_(1)', surveyData['2_1_(1)']);
      horizontalBarChartRender('chart_2_1_(2)', surveyData['2_1_(2)']);
      horizontalBarChartRender('chart_2_1_(3)', surveyData['2_1_(3)']);
      horizontalBarChartRender('chart_2_1_(4)', surveyData['2_1_(4)']);

      horizontalBarChartRender('chart_3_1_(1)', surveyData['3_1_(1)']);
      horizontalBarChartRender('chart_3_1_(2)', surveyData['3_1_(2)']);
      horizontalBarChartRender('chart_3_1_(3)', surveyData['3_1_(3)']);

      horizontalBarChartRender('chart_4_1_(1)', surveyData['4_1_(1)']);
      horizontalBarChartRender('chart_4_1_(2)', surveyData['4_1_(2)']);
      horizontalBarChartRender('chart_4_1_(3)', surveyData['4_1_(3)']);
      horizontalBarChartRender('chart_4_1_(4)', surveyData['4_1_(4)']);

      horizontalBarChartRender('chart_5_1_(1)', surveyData['5_1_(1)']);
      horizontalBarChartRender('chart_5_1_(2)', surveyData['5_1_(2)']);
      horizontalBarChartRender('chart_5_1_(3)', surveyData['5_1_(3)']);
      horizontalBarChartRender('chart_5_1_(4)', surveyData['5_1_(4)']);

      horizontalBarChartRender('chart_6_1_(1)', surveyData['6_1_(1)']);
      horizontalBarChartRender('chart_6_1_(2)', surveyData['6_1_(2)']);
      horizontalBarChartRender('chart_6_1_(3)', surveyData['6_1_(3)']);
      horizontalBarChartRender('chart_6_1_(4)', surveyData['6_1_(4)']);

      verticalBarChartRender('chart_7_1', surveyData['7_1'], surveyChoice["7_1"]);
      listRender('chart_7_2', surveyData['7_2']);


  };
  </script>
  @endsection