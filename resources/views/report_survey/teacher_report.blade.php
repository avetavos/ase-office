@extends('layouts.main')
@section('content')
<div class="row">
  <div class="col-12 px-0">
    <h3>{{$header}}</h3>
    <hr>
  </div>
  <a href="{{url('/report_survey/export/'.$survey->id)}}" class="export-excel p-3 shadow">
    <span class="d-block d-md-none"><i class="far fa-file-excel"></i></span>
    <span class="d-none d-md-block">บันทึกเป็น Excel</span>
  </a>
  <div class="col-lg-8 col-md-10 col-12 mx-auto px-0">
    <div class="card shadow">
      <div class="card-header bg-kku">
        <div class="row">
          <div class="col-12 d-flex justify-content-between">
            <h4>การตอบกลับ</h4>
            <h4>{{$count}}/{{$survey->amount}}</h4>
          </div>
        </div>
      </div>
      <div class="card-body">
        {{-- part-1 --}}
        <h5><b><u>ตอนที่ 1 ข้อมูลทั่วไปของผู้ตอบแบบสอบถาม</u></b></h5>
        <h5 class="mt-3">เพศ *</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_1_1"></canvas>
        </div>
        <h5 class="mt-3">ชั้นปี</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_1_2"></canvas>
        </div>
        <h5 class="mt-3">เกรดเฉลี่ย</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_1_3"></canvas>
        </div>

        {{-- part-2 --}}
        <hr>
        <h5><b><u>ด้านระบบอาจารย์ที่ปรึกษา</u></b></h5>
        <h5 class="mt-3">ระดับความพึงพอใจระบบอาจารย์ที่ปรึกษา *</h5>
        <h6>(1) ความสัมพันธ์อันดีระหว่างนักศึกษากับอาจารย์ที่ปรึกษา</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_2_1_(1)"></canvas>
        </div>
        <h6>(2) โอกาสที่นักศึกษาเข้าพบอาจารย์เพื่อขอรับคำปรึกษา</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_2_1_(2)"></canvas>
        </div>
        <h6>(3) การให้คำแนะนำนักศึกษาในการค้นหาข้อมูลเกี่ยวกับกฏ ระเบียบ ข้อบังคับ หลักสูตร</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_2_1_(3)"></canvas>
        </div>
        <h6>(4) การให้คำปรึกษาแก่นักศึกษาเกี่ยวกับการวางแผนการศึกษา หลักสูตรและการลงทะเบียน และอาชีพ</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_2_1_(4)"></canvas>
        </div>
        <h6>(5) การให้คำแนะนำแก่นักศึกษาเกี่ยวกับบริการต่างๆในวิทยาลัยและชุมชน</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_2_1_(5)"></canvas>
        </div>
        <h6>(6) การให้คำปรึกษาเบื้องต้นแก่นักศึกษาเกี่ยวกับสุขภาพกายและสุขภาพจิต</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_2_1_(6)"></canvas>
        </div>
        <h6>(7) การให้คำปรึกษาแก่นักศึกษาเกี่ยวกับการดำเนินชีวิตอย่างปลอดภัย</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_2_1_(7)"></canvas>
        </div>
        <h6>(8)ความพึงพอใจโดยรวมต่อระบบอาจารย์ที่ปรึกษา</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_2_1_(8)"></canvas>
        </div>
        <h5 class="mt-3">ข้อเสนอแนะต่อระบบอาจารย์ที่ปรึกษา</h5>
        <div class="col-12" id="chart_2_2">
        </div>

        {{-- part-3 --}}
        <hr>
        <h5><b><u>แฟ้มป์ระเบียนประวัติ</u></b></h5>
        <h6>ความซับซ้อนของแฟ้มระเบียนประวัติ</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_3_1_(1)"></canvas>
        </div>
        <h6>ประโยชน์ในการมีแฟ้มระเบียนประวัติ</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_3_1_(2)"></canvas>
        </div>
        <h5 class="mt-3">ข้อเสนอแนะต่อระบบอาจารย์ที่ปรึกษา</h5>
        <div class="col-12" id="chart_3_2">
        </div>

        {{-- part-4 --}}
        <hr>
        <h5><b><u>ด้านการจัดการข้อร้องเรียน</u></b></h5>
        <h5 class="mt-3">4.1 นักศึกษาเคยมีข้อร้องเรียนต่อการจัดการหลักสูตรหรือคณะหรือไม่ *</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_4_1"></canvas>
        </div>
        <h5 class="mt-3">4.2 หากนักศึกษาเคยมีข้อร้องเรียน ข้อร้องเรียนได้รับการแก้ไขหรือไม่</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_4_2"></canvas>
        </div>
        <h5 class="mt-3">4.3 ในกรณีที่มีการจัดการข้อร้องเรียน ท่านพึงพอใจอยู่ในระดับใด</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_4_3"></canvas>
        </div>
        <h5 class="mt-3">ข้อเสนอแนะต่อระบบอาจารย์ที่ปรึกษา</h5>
        <div class="col-12" id="chart_4_4">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom_script')
@include('report_survey.chart_render')
<script>
  var surveyData = {!! $surveyData !!};
  var surveyChoice = {
    "1_1": ["ชาย", "หญิง"],
    "1_2": ["ชั้นปีที่ 1", "ชั้นปีที่ 2", "ชั้นปีที่ 3", "ชั้นปีที่ 4", "ชั้นปีที่ 4 ขึ้นไป"],
    "1_3": ["1.00 - 2.00", "2.01 - 2.50", "2.51 - 3.00", "3.01 - 3.50", "3.51 - 4.00"],
    "4_1": ["ไม่เคย หากไม่เคยให้สิ้นสุดการทำแบบสำรวจ","เคย หากเคยให้ตอบคำถามในข้อถัดไป"],
    "4_2": ["ได้รับการแก้ไข","ไม่ได้รับการแก้ไข"],
  }
  Chart.defaults.global.legend.display = false;
  window.onload = function () {
      pieChartRender('chart_1_1', surveyData['1_1'], surveyChoice["1_1"]);
      pieChartRender('chart_1_2', surveyData['1_2'], surveyChoice["1_2"]);
      pieChartRender('chart_1_3', surveyData['1_3'], surveyChoice["1_3"]);

      horizontalBarChartRender('chart_2_1_(1)', surveyData['2_1_(1)']);
      horizontalBarChartRender('chart_2_1_(2)', surveyData['2_1_(2)']);
      horizontalBarChartRender('chart_2_1_(3)', surveyData['2_1_(3)']);
      horizontalBarChartRender('chart_2_1_(4)', surveyData['2_1_(4)']);
      horizontalBarChartRender('chart_2_1_(5)', surveyData['2_1_(5)']);
      horizontalBarChartRender('chart_2_1_(6)', surveyData['2_1_(6)']);
      horizontalBarChartRender('chart_2_1_(7)', surveyData['2_1_(7)']);
      horizontalBarChartRender('chart_2_1_(8)', surveyData['2_1_(8)']);
      listRender('chart_2_2', surveyData['2_2']);

      horizontalBarChartRender('chart_3_1_(1)', surveyData['3_1_(1)']);
      horizontalBarChartRender('chart_3_1_(2)', surveyData['3_1_(2)']);
      listRender('chart_3_2', surveyData['3_2']);

      verticalBarChartRender('chart_4_1', surveyData['4_1'], surveyChoice["4_1"]);
      verticalBarChartRender('chart_4_2', surveyData['4_2'], surveyChoice["4_2"]);
      horizontalBarChartRender('chart_4_3', surveyData['4_3']);
      listRender('chart_4_4', surveyData['4_4']);
  };

</script>
@endsection