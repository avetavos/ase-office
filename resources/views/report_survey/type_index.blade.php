@extends('layouts.main')





@section('content')
<div class="row p-3">
  <div class="col-12">
    <h3>ผลตอบกลับแบบประเมินแบบประเมิน</h3>
    <hr>
  @include('inc.alert')
  </div>
  <div class="col-lg-6 mx-auto">
    <h4><u>ประเภทแบบประเมิน</u></h4>
    <h5>ระดับปริญญาตรี</h5>
    <ul class="list-group">
      <li class="list-group-item"><a href="{{url('/report_survey/bachelor')}}">แบบประเมินความพึงพอใจของนักศึกษา</a></li>
      <li class="list-group-item"><a
          href="{{url('/report_survey/bachelor_teacher')}}">แบบประเมินอาจารย์ประจำหลักสูตร</a></li>
    </ul>
    <h5>ระดับบัณฑิตศึกษา</h5>
    <ul class="list-group">
      <li class="list-group-item"><a href="{{url('/report_survey/master')}}">แบบประเมินความพึงพอใจของนักศึกษา</a></li>
      <li class="list-group-item"><a href="{{url('/report_survey/master_teacher')}}">แบบประเมินอาจารย์ประจำหลักสูตร</a>
      </li>
    </ul>
    <h5>อื่นๆ</h5>
    <ul class="list-group">
      <li class="list-group-item"><a
          href="{{url('/report_survey/teacher')}}">แบบประเมินความพึงพอใจระบบอาจารย์ที่ปรึกษา</a></li>
      <li class="list-group-item"><a href="{{url('/report_survey/graduated')}}">แบบสำรวจความพึงพอใจผู้ใช้บัณฑิต</a></li>
    </ul>
  </div>
</div>
@endsection