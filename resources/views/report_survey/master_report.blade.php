@extends('layouts.main')
@section('content')
<div class="row">
  <div class="col-12 px-0">
    <h3>{{$header}}</h3>
    <hr>
  </div>
  <a href="{{url('/report_survey/export/'.$survey->id)}}" class="export-excel p-3 shadow">
    <span class="d-block d-md-none"><i class="far fa-file-excel"></i></span>
    <span class="d-none d-md-block">บันทึกเป็น Excel</span>
  </a>
  <div class="col-lg-8 col-md-10 col-12 mx-auto px-0">
    <div class="card shadow">
      <div class="card-header bg-kku">
        <div class="row">
          <div class="col-12 d-flex justify-content-between">
            <h4>การตอบกลับ</h4>
            <h4>{{$count}}/{{$survey->amount}}</h4>
          </div>
        </div>
      </div>
      <div class="card-body">
        {{-- part-1 --}}
        <h5><b><u>ตอนที่ 1 ข้อมูลทั่วไปของผู้ตอบแบบสอบถาม</u></b></h5>
        <h5 class="mt-3">เพศ *</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_1_1"></canvas>
        </div>
        <h5 class="mt-3">ชั้นปี</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_1_2"></canvas>
        </div>
        <h5 class="mt-3">เกรดเฉลี่ย</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_1_3"></canvas>
        </div>

        {{-- part-2 --}}
        <hr>
        <h5><b><u>ระดับความพึงพอใจการจัดการหลักสูตรและการเรียนการสอน</u></b></h5>
        <h5 class="mt-3">1.1 ความพึงพอใจโดยรวมด้านการจัดการหลักสูตรและการเรียนการสอน</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_2_1"></canvas>
        </div>
        <h5 class="mt-3">1.2 ความพึงพอใจด้านการจัดการหลักสูตรและการเรียนการสอน</h5>
        <h6>(1) มีรายวิชาในหลักสูตรมีความหลายหมายและทันสมัย</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_2_2_(1)"></canvas>
        </div>
        <h6>(2) มีการจัดการเรียนการสอนสอดคล้องกับวัตถุประสงค์ของหลักสูตร</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_2_2_(2)"></canvas>
        </div>
        <h6>(3) มีการจัดแผนการเรียนที่เหมาะสม</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_2_2_(3)"></canvas>
        </div>
        <h6>(4) มีการจัดกิจกรรมเสริมหลักสูตรที่เหมาะสม</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_2_2_(4)"></canvas>
        </div>
        <h6>(5) มีการกระตุ้นทักษะทางปัญญา การวิเคราะห์ และทักษะการแก้ปัญหา</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_2_2_(5)"></canvas>
        </div>
        <h6>(6) มีการใช้สื่อการสอนที่เหมาะสม</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_2_2_(6)"></canvas>
        </div>
        <h6>(7) มีการจัดการเรียนการสอนที่ส่งเสริมทักษะการเรียนรู้ 5 ด้าน ได้แก่ 1) ด้านคุณธรรม จริยธรรม 2) ด้านความรู้
          3) ด้านทักษะทางปัญญา
          4) ด้านทักษะความสัมพันธ์ระหว่างบุคคลและความรับผิดชอบ 5) ด้านทักษะการวิเคราะห์เชิงตัวเลข
          การสื่อสารและการใช้เทคโนโลยีสารสนเทศ</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_2_2_(7)"></canvas>
        </div>
        <h5 class="mt-3">1.3 ท่านไม่พึงพอใจในการจัดการหลักสูตรและการเรียนการสอน เลือกได้มากกว่า 1 ข้อ</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_2_3"></canvas>
        </div>
        <h5 class="mt-3">ข้อคิดเห็นหรือข้อเสนอแนะด้านการจัดการหลักสูตรและการเรียนการสอน</h5>
        <div class="col-12" id="chart_2_4">
        </div>

        {{-- part-3 --}}
        <hr>
        <h5><b><u>ระดับความพึงพอใจด้านอาจารย์ผู้สอน</u></b></h5>
        <h5 class="mt-3">2.1 ความพึงพอใจโดยรวมด้านการอาจารย์ผู้สอน</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_3_1"></canvas>
        </div>
        <h5>2.2 ความพึงพอใจในด้านอาจารย์ผู้สอน</h5>
        <h6>(1) มีความรู้ความสามารถและความเชี่ยวชาญในรายวิชาที่สอน</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_3_2_(1)"></canvas>
        </div>
        <h6>(2) มีคุณธรรม จริยธรรม และเป็นแบบอย่างที่ดี</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_3_2_(2)"></canvas>
        </div>
        <h6>(3) มีการสนับสนุน ส่งเสริมให้นักศึกษาเรียนรู้และพัฒนาตนเองอย่างสม่ำเสมอ</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_3_2_(3)"></canvas>
        </div>
        <h6>(4) มีการสนับสนุน ส่งเสริมให้นักศึกษาเรียนรู้และพัฒนาตนเองอย่างสม่ำเสมอ</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_3_2_(4)"></canvas>
        </div>
        <h6>(5) มีการยอมรับฟังความคิดเห็นหรือข้อเสนอแนะของผู้เรียน</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_3_2_(5)"></canvas>
        </div>
        <h5>2.3 ท่านไม่พึงพอใจในด้านอาจารย์ผู้สอน เลือกได้มากกว่า 1 ข้อ</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_3_3"></canvas>
        </div>
        <h5 class="mt-3">ข้อคิดเห็นหรือข้อเสนอแนะด้านอาจารย์ผู้สอน</h5>
        <div class="col-12" id="chart_3_4">
        </div>

        {{-- part-4 --}}
        <hr>
        <h5><b><u>ด้านการวัดและประเมินผล</u></b></h5>
        <h5 class="mt-3">3.1 ความพึงพอใจโดยรวมด้านการวัดและประเมินผล</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_4_1"></canvas>
        </div>
        <h5 class="mt-3">3.2 ความพึงพอใจด้านการวัดและประเมินผล</h5>
        <h6>(1) มีวิธีการวัดและประเมินผลสอดคล้องกับวัตถุประสงค์การเรียนรู้ของเนื้อหารายวิชา</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_4_2_(1)"></canvas>
        </div>
        <h6>(2) มีการวัดและประเมินที่เป็นธรรม ถูกต้อง โปร่งใส ตรวจสอบได้</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_4_2_(2)"></canvas>
        </div>
        <h6>(3) มีการประเมินความพึงพอใจในการจัดการเรียนการสอนทุกรายวิชาในแต่ละภาคการศึกษา</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_4_2_(3)"></canvas>
        </div>
        <h5>3.3 ท่านไม่พึงพอใจในด้านการวัดและประเมินผลข้อใด เลือกได้มากกว่า 1 ข้อ</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_4_3"></canvas>
        </div>
        <h5 class="mt-3">ข้อคิดเห็นหรือข้อเสนอแนะด้านการวัดและประเมินผล</h5>
        <div class="col-12" id="chart_4_4">
        </div>

        {{-- part-5 --}}
        <hr>
        <h5><b><u>ระดับความพึงพอใจด้านการพัฒนานักศึกษา</u></b></h5>
        <h5 class="mt-3">4.1 ความพึงพอใจโดยรวมด้านการพัฒนานักศึกษา</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_5_1"></canvas>
        </div>
        <h5 class="mt-3">4.2 ความพึงพอใจด้านการพัฒนานักศึกษา</h5>
        <h6>(1) มีการจัดกิจกรรม ที่สอดคล้องกับคุณลักษณะเด่นของบัณฑิตมหาวิทยาลัยขอนแก่นคือ บัณฑิตพร้อมทำงาน (Ready to
          Work) ประกอบด้วย
          1) มีประสบการณ์พร้อมปฏิบัติงานในวิชาชีพ 2) พร้อมต่อการเปลี่ยนแปลง และ 3 ) เรียนรู้ตลอดชีวิต</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_5_2_(1)"></canvas>
        </div>
        <h6>(2) มีการจัดกิจกรรมที่สอดคล้องกับทักษะในศตวรรษที่ 21 ได้แก่ 1) ทักษะด้านการเรียนรู้และนวัตกรรม 2)
          ทักษะสารสนเทศ สื่อและเทคโนโลยี
          3) ทักษะชีวิตและอาชีพ</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_5_2_(2)"></canvas>
        </div>
        <h6>(3) มีการส่งเสริมให้นักศึกษาแก้ปัญหาต่างๆผ่านกิจกรรมพัฒนานักศึกษา</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_5_2_(3)"></canvas>
        </div>
        <h6>(4) มีการส่งเสริมให้นักศึกษาได้เข้าร่วมกิจกรรมเพื่อพัฒนาทักษะทางวิชาการและวิชาชีพของคณะฯหรือมหาวิทยาลัย</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_5_2_(4)"></canvas>
        </div>
        <h6>(5) มีการสนับสนุนทุนการศึกษา เช่น ทุนเรียนดี ทุนกิจกรรมดี ทุนขาดแคลน เป็นต้น</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_5_2_(5)"></canvas>
        </div>
        <h5>4.3 ท่านไม่พึงพอใจด้านการพัฒนานักศึกษาข้อใด เลือกได้มากกว่า 1 ข้อ</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_5_3"></canvas>
        </div>
        <h5 class="mt-3">ข้อคิดเห็นหรือข้อเสนอแนะด้านการพัฒนานักศึกษา</h5>
        <div class="col-12" id="chart_5_4">
        </div>

        {{-- part-6 --}}
        <hr>
        <h5><b><u>ระดับความพึงพอใจด้านสิ่งสนับสนุนการเรียนรู้</u></b></h5>
        <h5 class="mt-3">5.1 ความพึงพอใจโดยรวมด้านสิ่งสนับสนุนการเรียนรู้</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_6_1"></canvas>
        </div>
        <h5 class="mt-3">5.2 ความพึงพอใจด้านสิ่งสนับสนุนการเรียนรู้</h5>
        <h6>(1) มีสื่อการเรียนการสอน อุปกรณ์การศึกษาและห้องปฏิบัติการที่ทันสมัยพร้อมใช้งานและเพียงพอ</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_6_2_(1)"></canvas>
        </div>
        <h6>(2) มีการให้บริการห้องสมุด เอกสาร หนังสือ ตำรา และแหล่งสารสนเทศอื่นๆ ที่ทันสมัย</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_6_2_(2)"></canvas>
        </div>
        <h6>(3) มีการให้บริการด้านเทคโนโลยีสารสนเทศ (โปรแกรมสนับสนุนการเรียน โปรแกรมประยุกต์(Application software)
          คอมพิวเตอร์
          ปริ๊นเตอร์)
        </h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_6_2_(3)"></canvas>
        </div>
        <h6>(4) มีการให้บริการอินเตอร์เน็ตและ WIFI ที่ครอบคลุมพื้นที่การจัดการเรียนการสอน</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_6_2_(4)"></canvas>
        </div>
        <h5>5.3 ท่านไม่พึงพอใจด้านสิ่งสนับสนุนการเรียนรู้ข้อใด เลือกได้มากกว่า 1 ข้อ</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_6_3"></canvas>
        </div>
        <h5 class="mt-3">ข้อคิดเห็นหรือข้อเสนอแนะด้านการพัฒนานักศึกษา</h5>
        <div class="col-12" id="chart_6_4"></div>

        {{-- part-7 --}}
        <hr>
        <h5><b><u>ระดับความพึงพอใจด้านการจัดการข้อร้องเรียน</u></b></h5>
        <h5 class="mt-3">6.1 ความพึงพอใจโดยรวมด้านการจัดการข้อร้องเรียน</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_7_1"></canvas>
        </div>
        <h5 class="mt-3">6.2 ความพึงพอใจด้านการจัดการข้อร้องเรียน</h5>
        <h6>(1) มีช่องทางการรับฟังข้อร้องเรียนจากนักศึกษาที่หลากหลายอ</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_7_2_(1)"></canvas>
        </div>
        <h6>(2) มีการนำข้อร้องเรียนไปพิจารณาดำเนินการอย่างเหมาะสม</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_7_2_(2)"></canvas>
        </div>
        <h6>(3) มีวิธีการติดตาม ประเมินผลความพึงพอใจต่อการจัดการข้อร้องเรียน</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_7_2_(3)"></canvas>
        </div>
        <h5>6.3 ท่านมีความไม่พึงพอใจด้านการจัดการข้อร้องเรียนในข้อใด เลือกได้มากกว่า 1 ข้อ</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_7_3"></canvas>
        </div>
        <h5 class="mt-3">ข้อคิดเห็นหรือข้อเสนอแนะด้านการจัดการข้อร้องเรียน</h5>
        <div class="col-12" id="chart_7_4"></div>

        {{-- part-8 --}}
        <hr>
        <h5><b><u>ตอนที่ 3 ความผูกพันธ์ต่อองค์กร</u></b></h5>
        <h5 class="mt-3">หลังจากที่ท่านได้เข้ามาศึกษาต่อในคณะฯท่านมีความผูกพันธ์ต่อหลักสูตรฯ/คณะฯ ในระดับใด</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_8_1"></canvas>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom_script')
@include('report_survey.chart_render')
<script>
  var surveyData = {!! $surveyData !!};
  var surveyChoice = {
    "1_1": ["ชาย", "หญิง"],
    "1_2": ["ชั้นปีที่ 1", "ชั้นปีที่ 2", "ชั้นปีที่ 3", "ชั้นปีที่ 4 ขึ้นไป"],
    "1_3": ["1.00 - 2.00", "2.01 - 2.50", "2.51 - 3.00", "3.01 - 3.50", "3.51 - 4.00"],
    "2_3": ["มีรายวิชาในหลักสูตรมีความหลายหมายและทันสมัย","มีการจัดการเรียนการสอนสอดคล้องกับวัตถุประสงค์ของหลักสูตร","มีการจัดแผนการเรียนที่เหมาะสม","มีการจัดกิจกรรมเสริมหลักสูตรที่เหมาะสม","มีการกระตุ้นทักษะทางปัญญา การวิเคราะห์ และทักษะการแก้ปัญหา","มีการใช้สื่อการสอนที่เหมาะสม","มีการจัดการเรียนการสอนที่ส่งเสริมทักษะการเรียนรู้ 5 ด้าน ได้แก่ 1) ด้านคุณธรรม จริยธรรม 2) ด้านความรู้ 3) ด้านทักษะทางปัญญา 4) ด้านทักษะความสัมพันธ์ระหว่างบุคคลและความรับผิดชอบ 5) ด้านทักษะการวิเคราะห์เชิงตัวเลขการสื่อสารและการใช้เทคโนโลยีสารสนเทศ","ไม่มี"],
    "3_3": ["มีความรู้ความสามารถและความเชี่ยวชาญในรายวิชาที่สอน","มีคุณธรรม จริยธรรม และเป็นแบบอย่างที่ดี","มีการสนับสนุน ส่งเสริมให้นักศึกษาเรียนรู้และพัฒนาตนเองอย่างสม่ำเสมอ","มีการสอนตรงตามวัตถุประสงค์ โดยใช้วิธีการสอนที่หลากหลายและเน้นผู้เรียนเป็นสำคัญ","มีการยอมรับฟังความคิดเห็นหรือข้อเสนอแนะของผู้เรียน","ไม่มี"],
    "4_3": ["มีวิธีการวัดและประเมินผลสอดคล้องกับวัตถุประสงค์การเรียนรู้ของเนื้อหารายวิชา","มีการวัดและประเมินที่เป็นธรรม ถูกต้อง โปร่งใส ตรวจสอบได้","มีการประเมินความพึงพอใจในการจัดการเรียนการสอนทุกรายวิชาในแต่ละภาคการศึกษา","ไม่มี"],
    "5_3": [
            "มีการจัดกิจกรรมที่สอดคล้องกับคุณลักษณะเด่นของบัณฑิตมหาวิทยาลัยขอนแก่นคือ บัณฑิตพร้อมทำงาน (Ready to Work) ประกอบด้วย 1) มีประสบการณ์พร้อมปฏิบัติงานในวิชาชีพ 2) พร้อมต่อการเปลี่ยนแปลง และ 3) เรียนรู้ตลอดชีวิต",
            "มีการจัดกิจกรรมที่สอดคล้องกับทักษะในศตวรรษที่ 21 ได้แก่ 1) ทักษะด้านการเรียนรู้และนวัตกรรม 2) ทักษะสารสนเทศ สื่อและเทคโนโลยี 3) ทักษะชีวิตและอาชีพ",
            "มีการส่งเสริมให้นักศึกษาแก้ปัญหาต่างๆผ่านกิจกรรมพัฒนานักศึกษา",
            "มีการส่งเสริมให้นักศึกษาได้เข้าร่วมกิจกรรมเพื่อพัฒนาทักษะทางวิชาการและวิชาชีพของคณะฯหรือมหาวิทยาลัย",
            "มีการสนับสนุนทุนการศึกษา เช่น ทุนเรียนดี ทุนกิจกรรมดี ทุนขาดแคลน เป็นต้น",
            "ไม่มี"
            ],
    "6_3": ["มีสื่อการเรียนการสอนอุปกรณ์การศึกษาและห้องปฏิบัติการที่ทันสมัยพร้อมใช้งานและเพียงพอ","มีการให้บริการห้องสมุด เอกสาร หนังสือ ตำรา และแหล่งสารสนเทศอื่นๆที่ทันสมัย","มีการให้บริการด้านเทคโนโลยีสารสนเทศ (โปรแกรมสนับสนุนการเรียนโปรแกรมประยุกต์(Application software) คอมพิวเตอร์ ปริ๊นเตอร์)","มีการให้บริการอินเตอร์เน็ตและ WIFI ที่ครอบคลุมพื้นที่การจัดการเรียนการสอน"],
    "7_3": ["มีช่องทางการรับฟังข้อร้องเรียนจากนักศึกษาที่หลากหลาย","มีการนำข้อร้องเรียนไปพิจารณาดำเนินการอย่างเหมาะสม","มีวิธีการติดตาม ประเมินผลความพึงพอใจต่อการจัดการข้อร้องเรียน","ไม่มี"],
  }
  Chart.defaults.global.legend.display = false;
  window.onload = function () {
      pieChartRender('chart_1_1', surveyData['1_1'], surveyChoice["1_1"]);
      pieChartRender('chart_1_2', surveyData['1_2'], surveyChoice["1_2"]);
      pieChartRender('chart_1_3', surveyData['1_3'], surveyChoice["1_3"]);
      horizontalBarChartRender('chart_2_1', surveyData['2_1']);
      horizontalBarChartRender('chart_2_2_(1)', surveyData['2_2_(1)']);
      horizontalBarChartRender('chart_2_2_(2)', surveyData['2_2_(2)']);
      horizontalBarChartRender('chart_2_2_(3)', surveyData['2_2_(3)']);
      horizontalBarChartRender('chart_2_2_(4)', surveyData['2_2_(4)']);
      horizontalBarChartRender('chart_2_2_(5)', surveyData['2_2_(5)']);
      horizontalBarChartRender('chart_2_2_(6)', surveyData['2_2_(6)']);
      horizontalBarChartRender('chart_2_2_(7)', surveyData['2_2_(7)']);
      verticalBarChartRender('chart_2_3', surveyData['2_3'], surveyChoice["2_3"]);
      listRender('chart_2_4', surveyData['2_4']);

      horizontalBarChartRender('chart_3_1', surveyData['3_1']);
      horizontalBarChartRender('chart_3_2_(1)', surveyData['3_2_(1)']);
      horizontalBarChartRender('chart_3_2_(2)', surveyData['3_2_(2)']);
      horizontalBarChartRender('chart_3_2_(3)', surveyData['3_2_(3)']);
      horizontalBarChartRender('chart_3_2_(4)', surveyData['3_2_(4)']);
      horizontalBarChartRender('chart_3_2_(5)', surveyData['3_2_(5)']);
      verticalBarChartRender('chart_3_3', surveyData['3_3'], surveyChoice['3_3']);
      listRender('chart_3_4', surveyData['3_4']);

      horizontalBarChartRender('chart_4_1', surveyData['4_1']);
      horizontalBarChartRender('chart_4_2_(1)', surveyData['4_2_(1)']);
      horizontalBarChartRender('chart_4_2_(2)', surveyData['4_2_(2)']);
      horizontalBarChartRender('chart_4_2_(3)', surveyData['4_2_(3)']);
      verticalBarChartRender('chart_4_3', surveyData['4_3'], surveyChoice["4_3"]);
      listRender('chart_4_4', surveyData['4_4']);

      horizontalBarChartRender('chart_5_1', surveyData['5_1']);
      horizontalBarChartRender('chart_5_2_(1)', surveyData['5_2_(1)']);
      horizontalBarChartRender('chart_5_2_(2)', surveyData['5_2_(2)']);
      horizontalBarChartRender('chart_5_2_(3)', surveyData['5_2_(3)']);
      horizontalBarChartRender('chart_5_2_(4)', surveyData['5_2_(4)']);
      horizontalBarChartRender('chart_5_2_(5)', surveyData['5_2_(5)']);
      verticalBarChartRender('chart_5_3', surveyData['5_3'], surveyChoice["5_3"]);
      listRender('chart_5_4', surveyData['5_4']);

      horizontalBarChartRender('chart_6_1', surveyData['6_1']);
      horizontalBarChartRender('chart_6_2_(1)', surveyData['6_2_(1)']);
      horizontalBarChartRender('chart_6_2_(2)', surveyData['6_2_(2)']);
      horizontalBarChartRender('chart_6_2_(3)', surveyData['6_2_(3)']);
      horizontalBarChartRender('chart_6_2_(4)', surveyData['6_2_(4)']);
      verticalBarChartRender('chart_6_3', surveyData['6_3'], surveyChoice["6_3"]);
      listRender('chart_6_4', surveyData['6_4']);

      horizontalBarChartRender('chart_7_1', surveyData['7_1']);
      horizontalBarChartRender('chart_7_2_(1)', surveyData['7_2_(1)']);
      horizontalBarChartRender('chart_7_2_(2)', surveyData['7_2_(2)']);
      horizontalBarChartRender('chart_7_2_(3)', surveyData['7_2_(3)']);
      verticalBarChartRender('chart_7_3', surveyData['7_3'], surveyChoice["7_3"]);
      listRender('chart_7_4', surveyData['7_4']);

      horizontalBarChartRender('chart_8_1', surveyData['8_1']);

  };

</script>
@endsection