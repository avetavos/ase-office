<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Survey</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" href="{{asset('css/survey.css')}}">
</head>

<body>
    <div class="container">
        <div class="row" id="content">
            <div class="col l7 s12 m10 mx-auto p-0" style="float:none;">
                <div class="card">
                    <div class="freebirdAccentBackground"></div>
                    <div class="card-content">
                        <h4 class="survey-header">{{$header}}</h4>
                        <h5>เราบันทึกคำตอบของคุณไว้แล้ว</h5>
                        <a href="{{url('/survey_form/'.$survey->id)}}">ส่งคำตอบอีกครั้ง</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="background"></div>
</body>
<script>
    function preventBack(){window.history.forward();}
    setTimeout("preventBack()", 0);
    window.onunload=function(){null};
</script>

</html>