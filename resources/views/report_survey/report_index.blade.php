@extends('layouts.main') 
@section('content')
<div class="row">
    <div class="col-12 px-0">
        <div class="row">
            <div class="col-lg-6 col-12">
                <h3>{{$header}}</h3>
            </div>
            <div class="col-lg-6 text-right">
                {!! Form::open(['id' => 'education_years_form', 'method' => 'get']) !!} {{ Form::label('year', 'ปีการศึกษา', ['class' =>
                'sr-only']) }}
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">ปีการศึกษา</div>
                    </div>
                    {{ Form::select('year', $years, $current_year->id, ['class' => 'form-control', 'onchange' => 'this.form.submit()']) }}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <hr>
    </div>
    <div class="col-lg-6 mx-auto px-0">
        <h4><u>สาขาวิชา</u></h4>
        @if (count($surveys) > 0)
        <ul class="list-group">
            @foreach ($surveys as $item) @if ($item->type != 'graduated')
            <li class="list-group-item"><a href="{{url('/report_survey/graph/'.$item->id)}}">{{$branches[$item->branch_id]}}</a></li>
            @else
            <li class="list-group-item"><a href="{{url('/report_survey/graph/'.$item->id)}}">แบบสำรวจความพึงพอใจผู้ใช้บัณฑิตประจำปีการศึกษา {{$current_year->year}}</a></li>
            @endif @endforeach
        </ul>
        @else
        <div class="text-center py-3">
            <h4>ยังไม่มีแบบประเมิน</h4>
        </div>
        @endif
    </div>
</div>
@endsection