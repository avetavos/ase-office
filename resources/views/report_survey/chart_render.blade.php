<script>
  function pieChartRender(elem, data, label) {
    var dataGraph = [];
    var dataCounts = {};
    data.forEach(function(x) {
      dataCounts[x] = (dataCounts[x] || 0)+1;
    });
    label.forEach(function(y) {
      if (dataCounts[y]) {
        dataGraph.push(dataCounts[y]);
      } else {
        dataGraph.push(0);
      }
    })
    var ctx = document.getElementById(elem).getContext('2d');
    window.myPie = new Chart(ctx, {
        type: 'pie',
        data: {
            datasets: [{
                data: dataGraph,
                backgroundColor: [
                    'rgba(51, 102, 204 ,0.9)',
                    'rgba(220, 57, 18 ,0.9)',
                    'rgba(255, 153, 0 ,0.9)',
                    'rgba(16, 150, 24 ,0.9)',
                    'rgba(153, 0, 153 ,0.9)',
                    'rgba(0, 153, 198 ,0.9)',
                    'rgba(221, 68, 119 ,0.9)',
                    'rgba(102, 170, 0 ,0.9)',
                    'rgba(184, 46, 46 ,0.9)',
                    'rgba(49, 99, 149 ,0.9)',
                ],
                label: 'Dataset'
            }],
            labels: label
        },
        options: {
            responsive: true,
            legend: {
                display: true,
                position: 'right',
            },
            maintainAspectRatio: false,
        }
    });
  }

  function horizontalBarChartRender(elem, data) {
    var dataGraph = [];
    var dataCounts = {};
    var label = ["มากที่สุด", "มาก", "ปานกลาง", "น้อย", "น้อยที่สุด"];
    data.forEach(function(x) {
      dataCounts[x] = (dataCounts[x] || 0)+1;
    });
    label.forEach(function(y) {
      if (dataCounts[y]) {
        dataGraph.push(dataCounts[y]);
      } else {
        dataGraph.push(0);
      }
    })
    var ctx = document.getElementById(elem).getContext('2d');
    window.myHorizontalBar = new Chart(ctx, {
        type: 'horizontalBar',
        "data":{
          "labels":label,
          "datasets":[{
            "label":"",
            "data":dataGraph,
            "fill":false,
            backgroundColor: [
              'rgba(51, 102, 204 ,0.7)',
              'rgba(220, 57, 18 ,0.7)',
              'rgba(255, 153, 0 ,0.7)',
              'rgba(16, 150, 24 ,0.7)',
              'rgba(153, 0, 153 ,0.7)',
              'rgba(0, 153, 198 ,0.7)',
              'rgba(221, 68, 119 ,0.7)',
              'rgba(102, 170, 0 ,0.7)',
              'rgba(184, 46, 46 ,0.7)',
              'rgba(49, 99, 149 ,0.7)',
            ],
            borderColor: [
              'rgb(51, 102, 204)',
              'rgb(220, 57, 18)',
              'rgb(255, 153, 0)',
              'rgb(16, 150, 24)',
              'rgb(153, 0, 153)',
              'rgb(0, 153, 198)',
              'rgb(221, 68, 119)',
              'rgb(102, 170, 0)',
              'rgb(184, 46, 46)',
              'rgb(49, 99, 149)',
            ],
            borderWidth: 1
          }]
        },       
        options: {
          "scales":{
            "xAxes":[{
              "ticks":{
                "beginAtZero":true,
                stepSize: 10
              }
            }]
          },
          elements: {
            rectangle: {
              borderWidth: 2,
            }
          },
          responsive: true,
          legend: {
            position: 'right',
          },
          maintainAspectRatio: false,
        }
    });
  }

  function verticalBarChartRender(elem, data, label) {
    var dataGraph = [];
    var dataCounts = {};
    data.forEach(function(x) {
      dataCounts[x] = (dataCounts[x] || 0)+1;
    });
    label.forEach(function(y) {
      if (dataCounts[y]) {
        dataGraph.push(dataCounts[y]);
      } else {
        dataGraph.push(0);
      }
    })
    var ctx = document.getElementById(elem).getContext('2d');
    window.myBar = new Chart(ctx, {
      "type":"bar",
      "data":{
        "labels":label,
        "datasets":[{
          "label":"",
          "data":dataGraph,
          "fill":false,
          backgroundColor: [
            'rgba(51, 102, 204 ,0.7)',
            'rgba(220, 57, 18 ,0.7)',
            'rgba(255, 153, 0 ,0.7)',
            'rgba(16, 150, 24 ,0.7)',
            'rgba(153, 0, 153 ,0.7)',
            'rgba(0, 153, 198 ,0.7)',
            'rgba(221, 68, 119 ,0.7)',
            'rgba(102, 170, 0 ,0.7)',
            'rgba(184, 46, 46 ,0.7)',
            'rgba(49, 99, 149 ,0.7)',
          ],
          borderColor: [
            'rgb(51, 102, 204)',
            'rgb(220, 57, 18)',
            'rgb(255, 153, 0)',
            'rgb(16, 150, 24)',
            'rgb(153, 0, 153)',
            'rgb(0, 153, 198)',
            'rgb(221, 68, 119)',
            'rgb(102, 170, 0)',
            'rgb(184, 46, 46)',
            'rgb(49, 99, 149)',
          ],
          borderWidth: 1
        }]
      },       
      options: {
        "scales":{
          "yAxes":[{
            "ticks":{
              "beginAtZero":true,
              stepSize: 10
            }
          }],
          xAxes: [{
              display: false
          }]
        },
        elements: {
          rectangle: {
            borderWidth: 2,
          }
        },
        responsive: true,
        legend: {
          position: 'right',
        },
        maintainAspectRatio: false,
      }
    });
  }

  function listRender(elem, data) {
    if (data.length == 0) {
      $('#' + elem).append(`
        <p class="text-center my-5">ไม่มีข้อคิดเห็นหรือข้อเสนอแนะ</p>
      `)
    } else {
      var answerList = "";
      data.forEach(function(item) {
        answerList += `<li>${item}</li>`
      })
      $('#' + elem).append(`
        <ul>
        ${answerList}
        </ul>
      `)
    }
  }


</script>