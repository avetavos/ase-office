@extends('layouts.main')
@endsection

@section('content')
<div class="row">
  <div class="col-12 px-0">
    <h3>{{$header}}</h3>
    <hr>
  </div>
  <a href="{{url('/report_survey/export/'.$survey->id)}}" class="export-excel p-3 shadow">
    <span class="d-block d-md-none"><i class="far fa-file-excel"></i></span>
    <span class="d-none d-md-block">บันทึกเป็น Excel</span>
  </a>
  <div class="col-lg-8 col-md-10 col-12 mx-auto px-0">
    <div class="card shadow">
      <div class="card-header bg-kku">
        <div class="row">
          <div class="col-12 d-flex justify-content-between">
            <h4>การตอบกลับ</h4>
            <h4>{{$count}}/{{$survey->amount}}</h4>
          </div>
        </div>
      </div>
      <div class="card-body">
        {{-- part-1 --}}
        <h5><b><u>ตอนที่ 1 ข้อมูลทั่วไปของผู้ตอบแบบสอบถาม</u></b></h5>
        <h5 class="mt-3">เพศ *</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_1_1"></canvas>
        </div>
        <h5 class="mt-3">ตำแหน่งทางวิชาการ *</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_1_2"></canvas>
        </div>
        <h5 class="mt-3">คุณวุฒิการศึกษา *</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_1_3"></canvas>
        </div>
        <h5 class="mt-3">ประสบการณ์ในการสอน *</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_1_4"></canvas>
        </div>
        <h5 class="mt-3">ประสบการณ์ในการบริหารหลักสูตร *</h5>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_1_5"></canvas>
        </div>

        {{-- part-2 --}}
        <hr>
        <h5><b><u>ความพึงพอใจด้านการบริหารหลักสูตร</u></b></h5>
        <h5>ระดับความพึงพอใจต่อการบริหารหลักสูตร</h5>
        <h6>(1) มีการมอบหมายหน้าที่และความรับผิดชอบของอาจารย์ผู้รับผิดชอบหลักสูตรอย่างชัดเจนและเป็นรายลักษณ์อักษร</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_2_1_(1)"></canvas>
        </div>
        <h6>(2) มีการกำกับติดตามบทบาทหน้าที่ที่ได้รับมอบหมาย</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_2_1_(2)"></canvas>
        </div>
        <h6>(3) มีการวิเคราะห์ผลการดำเนินงานและการปรับปรุงการบริหารงานหลักสูตร</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_2_1_(3)"></canvas>
        </div>
        <h6>(4) มีส่วนร่วมในการประชุมเพื่อวางแผน ติดตาม และทบทวนการดำเนินงานหลักสูตร</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_2_1_(4)"></canvas>
        </div>

        {{-- part-3 --}}
        <hr>
        <h5><b><u>ด้านการพัฒนาอาจารย์ผู้รับผิดชอบหลักสูตร</u></b></h5>
        <h5>ระดับความพึงพอใจด้านการพัฒนาอาจารย์ผู้รับผิดชอบหลักสูตร</h5>
        <h6>(1) มีการวางแผนในการพัฒนาตนเองในด้านวิชาการอย่างเป็นระบบต่อปีการศึกษา</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_3_1_(1)"></canvas>
        </div>
        <h6>(2) มีการประชาสัมพันธ์การอบรมต่าง ๆ
          ที่เป็นประโยชน์ต่อการเรียนการสอนให้อาจารย์ผู้รับผิดชอบหลักสูตรได้รับทราบอยู่เสมอรายวิชาที่สอน</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_3_1_(2)"></canvas>
        </div>
        <h6>(3) อาจารย์ได้รับการสนับสนุนในการเข้าร่วมโครงการพัฒนาตนเองในแต่ละครั้ง</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_3_1_(3)"></canvas>
        </div>
        <h6>(4) มีการติดตามผลการพัฒนาอาจารย์ผู้รับผิดชอบหลักสูตรอย่างเป็นระบบ</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_3_1_(4)"></canvas>
        </div>
        <h6>(5) มีการพัฒนาส่งเสริม กำกับและติดตามการเข้าสู่ตำแหน่งทางวิชาการ</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_3_1_(5)"></canvas>
        </div>

        {{-- part-4 --}}
        <hr>
        <h5><b><u>ด้านการกำกับติดตามคุณภาพของหลักสูตร</u></b></h5>
        <h5>ระดับความพึงพอใจด้านการกำกับติดตามคุณภาพของหลักสูตร</h5>
        <h6>(1) มีการกำกับและติดตามการจัดทำรายละเอียดของรายวิชาและประสบการณ์ภาคสนาม(ถ้ามี) ตามแบบ มคอ.3 และ มคอ.4
          ครบทุกรายวิชาก่อนการเปิดสอนในแต่ละภาคการศึกษา</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_4_1_(1)"></canvas>
        </div>
        <h6>(2) มีการกำกับและติดตามการจัดทำรายงานผลการดำเนินการของรายวิชา และประสบการณ์ภาคสนาม (ถ้ามี) ตามแบบ มคอ.5 และ
          มคอ.6
          ครบทุกรายวิชา หลังสิ้นสุดภาคการศึกษาที่เปิสอนให้</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_4_1_(2)"></canvas>
        </div>
        <h6>(3) มีการทวนสอบผลสัมฤทธิ์ของนักศึกษาตามมาตรฐานผลการเรียนรู้ ที่กำหนดใน มคอ.3 และมคอ.4 (ถ้ามี)
          อย่างน้อยร้อยละ 25
          ของรายวิชาที่เปิดสอนในแต่ละปีการศึกษา
        </h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_4_1_(3)"></canvas>
        </div>

        {{-- part-5 --}}
        <hr>
        <h5><b><u>ด้านการจัดการเรียนการสอน</u></b></h5>
        <h5>ระดับความพึงพอใจด้านการจัดการเรียนการสอน</h5>
        <h6>(1) มีการส่งเสริมให้อาจารย์ใช้วิธีการสอนใหม่ ๆ ที่พัฒนาทักษะการเรียนรู้ของนักศึกษา</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_5_1_(1)"></canvas>
        </div>
        <h6>(2) มีการกำกับติดตามการจัดการเรียนสอนให้เป็นไปตามมาตรฐาน TQF</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_5_1_(2)"></canvas>
        </div>
        <h6>(3) มีการประเมินการสอนของอาจารย์และนำผลการประเมินมาใช้ในพัฒนาทักษะและคุณการสอนของอาจารย์</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_5_1_(3)"></canvas>
        </div>
        <h6>(4)
          มีการส่งเสริมให้มีการบูรณาการด้านการวิจัยและ/หรือการบริการวิชาการ/หรือการทำนุศิลปะวัฒนธรรมในกระบวนการเรียนการสอนและส่งผลต่อการเรียนรู้ของผู้เรียน
        </h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_5_1_(4)"></canvas>
        </div>
        <h6>(5) มีการกำหนดรายวิชาให้ผู้สอนมีความเหมาะสมตรงกับความรู้ความสามารถของอาจารย์ผู้สอน</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_5_1_(5)"></canvas>
        </div>
        <h6>(6) การควบคุมกำกับกระบวนการจัดการเรียนการสอนและการประเมินผลการเรียนของนักศึกษา</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_5_1_(6)"></canvas>
        </div>

        {{-- part-6 --}}
        <hr>
        <h5><b><u>ด้านการพัฒนานักศึกษา</u></b></h5>
        <h5>ระดับความพึงพอใจด้านการพัฒนานักศึกษา</h5>
        <h6>(1) มีการจัดกิจกรรมที่ส่งเสริมทักษะในการพัฒนานักศึกษาที่สอดคล้องกับ TQF</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_6_1_(1)"></canvas>
        </div>
        <h6>(2) มีการส่งเสริมการให้คำปรึกษาทั้งในด้านวิชาการและการใช้ชีวิตของนักศึกษา</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_6_1_(2)"></canvas>
        </div>

        {{-- part-7 --}}
        <hr>
        <h5><b><u>ด้านบรรยากาศและสิ่งสนับสนุนการเรียนรู้</u></b></h5>
        <h5>ระดับความพึงพอใจด้านบรรยากาศและสิ่งสนับสนุนการเรียนรู้</h5>
        <h6>(1) มีการสำรวจและวิเคราะห์ความต้องการด้านสิ่งสนับสนุนการเรียนรู้</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_7_1_(1)"></canvas>
        </div>
        <h6>(2) มีการจัดสภาพแวดล้อมที่เหมาะสมกับการจัดการเรียนการสอ</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_7_1_(2)"></canvas>
        </div>
        <h6>(3) มีการวางแผนและจัดหาทรัพยากรต่างๆอย่างเพียงพอและทันสมัยเพื่อใช้ในการสนับสนุนการเรียนรู้</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_7_1_(3)"></canvas>
        </div>
        <h6>(4) มีการประเมินความพึงพอใจและนำมาทบทวนและปรับปรุงในการจัดการเรียนการสอนครั้งถัดไป</h6>
        <div class="chart-fix-height col-lg-8 mx-auto">
          <canvas id="chart_7_1_(4)"></canvas>
        </div>

        {{-- part-8 --}}
        <hr>
        <h5 class="mt-3">ข้อคิดเห็นหรือข้อเสนอแนะด้านอาจารย์ผู้สอน</h5>
        <div class="col-12" id="chart_8_1">
        </div>
      </div>
    </div>
  </div>
</div>
@include('report_survey.chart_render')
@endsection

@section('custom_script')
<script>
  var surveyData = {!! $surveyData !!};
  var surveyChoice = {
    "1_1": ["ชาย", "หญิง"],
    "1_2": ["ศาสตราจารย์","รองศาสตราจารย์","ผู้ช่วยศาสตราจารย์","อาจารย์"],
    "1_3": ["ปริญญาเอก","ปริญญาโท"],
    "1_4": ["1-4 ปี","5-7 ปี","มากกว่า 7 ปี"],
    "1_5": ["1-3 ปี","4-5 ปี","มากกว่า 5 ปี"]
  }
  Chart.defaults.global.legend.display = false;
  window.onload = function () {
      pieChartRender('chart_1_1', surveyData['1_1'], surveyChoice["1_1"]);
      pieChartRender('chart_1_2', surveyData['1_2'], surveyChoice["1_2"]);
      pieChartRender('chart_1_3', surveyData['1_3'], surveyChoice["1_3"]);
      pieChartRender('chart_1_4', surveyData['1_4'], surveyChoice["1_4"]);
      pieChartRender('chart_1_5', surveyData['1_5'], surveyChoice["1_5"]);

      horizontalBarChartRender('chart_2_1_(1)', surveyData['2_1_(1)']);
      horizontalBarChartRender('chart_2_1_(2)', surveyData['2_1_(2)']);
      horizontalBarChartRender('chart_2_1_(3)', surveyData['2_1_(3)']);
      horizontalBarChartRender('chart_2_1_(4)', surveyData['2_1_(4)']);

      horizontalBarChartRender('chart_3_1_(1)', surveyData['3_1_(1)']);
      horizontalBarChartRender('chart_3_1_(2)', surveyData['3_1_(2)']);
      horizontalBarChartRender('chart_3_1_(3)', surveyData['3_1_(3)']);
      horizontalBarChartRender('chart_3_1_(4)', surveyData['3_1_(4)']);
      horizontalBarChartRender('chart_3_1_(5)', surveyData['3_1_(5)']);

      horizontalBarChartRender('chart_4_1_(1)', surveyData['4_1_(1)']);
      horizontalBarChartRender('chart_4_1_(2)', surveyData['4_1_(2)']);
      horizontalBarChartRender('chart_4_1_(3)', surveyData['4_1_(3)']);

      horizontalBarChartRender('chart_5_1_(1)', surveyData['5_1_(1)']);
      horizontalBarChartRender('chart_5_1_(2)', surveyData['5_1_(2)']);
      horizontalBarChartRender('chart_5_1_(3)', surveyData['5_1_(3)']);
      horizontalBarChartRender('chart_5_1_(4)', surveyData['5_1_(4)']);
      horizontalBarChartRender('chart_5_1_(5)', surveyData['5_1_(5)']);
      horizontalBarChartRender('chart_5_1_(6)', surveyData['5_1_(6)']);

      horizontalBarChartRender('chart_6_1_(1)', surveyData['6_1_(1)']);
      horizontalBarChartRender('chart_6_1_(2)', surveyData['6_1_(2)']);

      horizontalBarChartRender('chart_7_1_(1)', surveyData['7_1_(1)']);
      horizontalBarChartRender('chart_7_1_(2)', surveyData['7_1_(2)']);
      horizontalBarChartRender('chart_7_1_(3)', surveyData['7_1_(3)']);
      horizontalBarChartRender('chart_7_1_(4)', surveyData['7_1_(4)']);

      listRender('chart_8_1', surveyData['8_1']);

  };

</script>
@endsection