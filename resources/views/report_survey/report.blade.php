@extends('layouts.main')
@section('custom_style')
<style>
    #chart1 {
        margin-top: 1rem;
        padding: 1rem;
        position: relative;
        width 50%;
    }
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <h3>{{$header}}</h3>
        <hr>
    </div>
    <div class="col-8 mx-auto">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-12">
                        <h5>การตอบกลับ</h5>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <h5><b><u>ตอนที่ 1 ข้อมูลทั่วไปของผู้ตอบแบบสอบถาม</u></b></h5>
                <h5>เพศ *</h5>
                <div id="canvas-holder">
                    <canvas id="chart1"></canvas>
                </div>
            </div>
            <div class="card-body">
                <h5>ชั้นปี</h5>
                <div id="canvas-holder">
                    <canvas id="chart2"></canvas>
                </div>
            </div>
            <div class="card-body">
                <h5>เกรดเฉลี่ย</h5>
                <div id="canvas-holder">
                    <canvas id="chart3"></canvas>
                </div>
            </div>
            <div class="card-body">
                <h5>1.1 ความพึงพอใจโดยรวมด้านการจัดการหลักสูตรและการเรียนการสอน</h5>
                <div id="container">
                    <canvas id="canvas"></canvas>
                </div>
            </div>
            <div class="card-body">
                <h5>1.2 ความพึงพอใจด้านการจัดการหลักสูตรและการเรียนการสอน</h5>
                <h5>(1) มีรายวิชาในหลักสูตรมีความหลายหมายและทันสมัย</h5>
                <div id="container">
                    <canvas id="canvas2"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_script')
<script>
    var tempData = {!! json_decode($surveyData) !!}
    window.onload = function () {
        var ctx1 = document.getElementById('chart1').getContext('2d');
        var ctx2 = document.getElementById('chart2').getContext('2d');
        var ctx3 = document.getElementById('chart3').getContext('2d');
        window.myPie = new Chart(ctx1, {
        type: 'pie',
        data: {
            datasets: [{
                data: [12, 5],
                backgroundColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                label: 'Dataset'
            }],
            labels: [
                'ชาย ',
                'หญิง',
            ]
        },
        options: {
            responsive: true,
            legend: {
                display: true,
                position: 'right',
            }
        }
    });
        window.myPie = new Chart(ctx2, {
        type: 'pie',
        data: {
            datasets: [{
                data: [12, 5,6,2],
                backgroundColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                label: 'Dataset'
            }],
            labels: [
                'ชั้นปีที่ 1',
                'ชั้นปีที่ 2',
                'ชั้นปีที่ 3',
                'ชั้นปีที่ 4',
                'ชั้นปีที่ 5 ขึ้นไป',
            ]
        },
        options: {
            responsive: true,
            legend: {
                display: true,
                position: 'right',
            }
        }
    });
        window.myPie = new Chart(ctx3, {
        type: 'pie',
        data: {
            datasets: [{
                data: [12, 5,8,2],
                backgroundColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                label: 'Dataset'
            }],
            labels: [
                '1.00 - 2.00 ',
                '2.01 - 2.50',
                '2.51 - 3.00',
                '3.01 - 4.00'
            ]
        },
        options: {
            responsive: true,
            legend: {
                display: true,
                position: 'right',
            }
        }
    });
    };

    var color = Chart.helpers.color;
    var horizontalBarChartData = {
        labels: [
            'มากที่สุด',
            'มาก',
            'ปานกลาง',
            'น้อย',
            'น้อยที่สุด',
        ],
        datasets: [{
            label: 'My First Dataset',
            data: [65, 59, 80, 81, 56],
            fill: false,
            backgroundColor: [
                'rgba(255, 99, 132, 0.5)',
                'rgba(255, 159, 64, 0.5)',
                'rgba(255, 205, 86, 0.5)',
                'rgba(75, 192, 192, 0.5)',
                'rgba(54, 162, 235, 0.5)',
            ],
            borderColor: [
                'rgb(255, 99, 132)',
                'rgb(255, 159, 64)',
                'rgb(255, 205, 86)',
                'rgb(75, 192, 192)',
                'rgb(54, 162, 235)',
            ],
            borderWidth: 1
        }],
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    }
        var ctx = document.getElementById('canvas').getContext('2d');
        Chart.defaults.global.legend.display = false;
        Chart.defaults.global.tooltips.enabled = false;
        window.myHorizontalBar = new Chart(ctx, {
            type: 'horizontalBar',
            data: horizontalBarChartData,
            options: {
                // Elements options apply to all of the options unless overridden in a dataset
                // In this case, we are setting the border of each horizontal bar to be 2px wide
                elements: {
                    rectangle: {
                        borderWidth: 2,
                    }
                },
                responsive: true,
                legend: {
                    position: 'right',
                },
            }
        });

        var color = Chart.helpers.color;
		var barChartData = {
			labels: ['มากที่สุด', 'มาก', 'ปานกลาง', 'น้อย', 'น้อยที่สุด'],
			datasets: [{
				label: 'Dataset 1',
				data: [10,15,20,5,7],
        backgroundColor: [
                'rgba(255, 99, 132, 0.5)',
                'rgba(255, 159, 64, 0.5)',
                'rgba(255, 205, 86, 0.5)',
                'rgba(75, 192, 192, 0.5)',
                'rgba(54, 162, 235, 0.5)',
            ],
            borderColor: [
                'rgb(255, 99, 132)',
                'rgb(255, 159, 64)',
                'rgb(255, 205, 86)',
                'rgb(75, 192, 192)',
                'rgb(54, 162, 235)',
            ],
            borderWidth: 1
			}]
		};

			var ctx = document.getElementById('canvas2').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
					responsive: true,
					legend: {
						position: 'top',
					},
				}
			});

</script>
@endsection