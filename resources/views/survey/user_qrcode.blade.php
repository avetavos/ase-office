@extends('layouts.main') 
@section('custom_style')
<style>
    #qrcode>img {
        margin-left: auto;
        margin-right: auto;
        box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15) !important;
    }
</style>
@endsection
 
@section('content')
<div class="row p-3">
    <div class="col-12">
        <h3>{{$header}}</h3>
        <hr>
    </div>
    <div class="col-12 mb-3">
        <div class="row">
            <div class="col-12 text-center mx-auto p-5" id="qrcode">
            </div>
            <div class="col-12 text-center">
                <p>หรือสามารถถึงได้เข้าผ่านลิงค์ข้างล่าง</p>
            </div>
            <div class="col-12 text-center">
                <a href="{{url('/survey_form/'.$survey->id)}}" target="_blank">{{$header}}</a>
            </div>
        </div>
    </div>
@endsection
 
@section('custom_script')
    <script type="text/javascript">
        new QRCode(document.getElementById('qrcode'), "{{url('/survey_form/'.$survey->id)}}");
    </script>
@endsection






</div>