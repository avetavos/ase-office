@extends('layouts.main')



@section('content')
<div class="row p-3">
  <div class="col-12">
    <h3>แก้ไขข้อมูลแบบประเมิน</h3>
    <hr>
  @include('inc.alert')
  </div>
  <div class="col-lg-6 mx-auto">
    <h4><u>ประเภทแบบประเมิน</u></h4>
    <h5>ระดับปริญญาตรี</h5>
    <ul class="list-group">
      <li class="list-group-item"><a href="{{url('/admin_survey_list/bachelor')}}">แบบประเมินความพึงพอใจของนักศึกษา</a>
      </li>
      <li class="list-group-item"><a
          href="{{url('/admin_survey_list/bachelor_teacher')}}">แบบประเมินอาจารย์ประจำหลักสูตร</a></li>
    </ul>
    <h5>ระดับบัณฑิตศึกษา</h5>
    <ul class="list-group">
      <li class="list-group-item"><a href="{{url('/admin_survey_list/master')}}">แบบประเมินความพึงพอใจของนักศึกษา</a>
      </li>
      <li class="list-group-item"><a
          href="{{url('/admin_survey_list/master_teacher')}}">แบบประเมินอาจารย์ประจำหลักสูตร</a></li>
    </ul>
    <h5>อื่นๆ</h5>
    <ul class="list-group">
      <li class="list-group-item"><a
          href="{{url('/admin_survey_list/teacher')}}">แบบประเมินความพึงพอใจระบบอาจารย์ที่ปรึกษา</a></li>
      <li class="list-group-item"><a href="{{url('/admin_survey_list/graduated')}}">แบบสำรวจความพึงพอใจผู้ใช้บัณฑิต</a>
      </li>
    </ul>
  </div>
</div>
@endsection