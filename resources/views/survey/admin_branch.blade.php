@extends('layouts.main')
@section('content')
<div class="row">
  <div class="col-12 px-0">
    <div class="row">
      <div class="col-lg-6">
        <h3 class="mb-md-0 mb-3">แก้ไขข้อมูลแบบประเมิน</h3>
      </div>
      <div class="col-lg-6 text-right">
        {!! Form::open(['id' => 'education_years_form', 'method' => 'get']) !!} {{ Form::label('year', 'ปีการศึกษา', ['class' =>
        'sr-only']) }}
        <div class="input-group">
          <div class="input-group-prepend">
            <div class="input-group-text">ปีการศึกษา</div>
          </div>
          {{ Form::select('year', $years, $current_year->id, ['class' => 'form-control', 'onchange' => 'this.form.submit()']) }}
        </div>
        {!! Form::close() !!}
      </div>
    </div>
    <hr>
    @include('inc.alert')
  </div>
  <div class="col-lg-6 mx-auto px-0">
    <h4><u>สาขาวิชา</u></h4>
    @if (count($branches) > 0)
    <ul class="list-group">
      @foreach ($survey as $item)
      @if ($item->type == 'graduated')
      <li class="list-group-item"><a href="{{url('/admin_survey/'.$item->id.'/edit')}}">{{$branches[$item->id]}}</a>
      </li>
      @else
      <li class="list-group-item"><a
          href="{{url('/admin_survey/'.$item->id.'/edit')}}">{{$branches[$item->branch_id]}}</a></li>
      @endif
      @endforeach
    </ul>
    @else
    <div class="text-center py-3">
      <h4>ยังไม่มีแบบประเมินของสาขาวิชา</h4>
    </div>
    @endif
  </div>
</div>
@endsection