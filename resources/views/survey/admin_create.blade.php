@extends('layouts.main')
@section('content')
<div class="row p-3">
  <div class="col-12">
    <h3>สร้างแบบประเมินใหม่</h3>
    <hr>
    @include('inc.alert')
  </div>
  <div class="col-lg-10 col-xl-8 mb-3 mx-auto">
    <div class="card p-md-5 p-3">
      {!! Form::open(['action' => 'SurveyController@store']) !!}
      <div class="form-row">
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('type', 'ประเภทแบบประเมิน')}}
          {{ Form::select('type', $types, null,['class' => 'form-control'])}}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('year', 'ปีการศึกษา')}} {{ Form::select('year', $years, null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('start_date', 'วัน/เดือน/ปี ที่เริ่มประเมิน')}}
          {{ Form::input('date', 'start_date', null, ['class' => 'form-control'])}}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('end_date', 'วัน/เดือน/ปี ที่สิ้นสุดการประเมิน')}}
          {{ Form::input('date', 'end_date', null, ['class' => 'form-control'])}}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('amount', 'จำนวนแบบประเมินขั้นต่ำที่ต้องการ')}}
          {{ Form::input('number', 'amount', null, ['class' => 'form-control'])}}
        </div>
        <div class="col-lg-8 mx-auto text-center mt-3">
          {{ Form::input('submit', null, 'สร้าง', ['class' => 'btn btn-primary px-5'])}}
        </div>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection