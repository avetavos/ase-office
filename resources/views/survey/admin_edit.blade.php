@extends('layouts.main')
@section('content')
<div class="row p-3">
  <div class="col-12">
    <h3>{{$header}}</h3>
    <hr>
    @include('inc.alert')
  </div>
  <div class="col-lg-10 col-xl-8 mb-3 mx-auto">
    <div class="card p-md-5 p-3">
      {!! Form::open(['action' => ['SurveyController@update', $survey->id], 'method' => 'put']) !!}
      <div class="form-row">
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('start_date', 'วัน/เดือน/ปี ที่เริ่มประเมิน')}} {{ Form::input('date', 'start_date', $survey->start_date,
          ['class' => 'form-control'])}}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('end_date', 'วัน/เดือน/ปี ที่สิ้นสุดการประเมิน')}} {{ Form::input('date', 'end_date', $survey->end_date, ['class'
          => 'form-control'])}}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('amount', 'จำนวนแบบประเมินขั้นต่ำที่ต้องการ')}} {{ Form::input('number', 'amount', $survey->amount, ['class'
          => 'form-control'])}}
        </div>
        <div class="col-lg-8 mx-auto text-center mt-3">
          {{ Form::input('submit', null, 'บันทึก', ['class' => 'btn btn-primary px-5'])}}
        </div>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
</div>
@endsection