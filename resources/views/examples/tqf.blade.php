@extends('layouts.main')
@section('content')
<div class="row p-3">
  <div class="col-12">
    <h3>ตัวอย่างเอกสารรายละเอียดของรายวิชา</h3>
    <hr />
  </div>
  <div class="col-12">
    <div class="card shadow mb-3">
      <div class="card-body">
        <p>ตัวอย่างรายละเอียดของรายวิชา มคอ.3</p>
        <a href="{{url('/documents/tqf_3_example.pdf')}}" target="_blank" class="btn btn-primary" role="button"
          download>ดาวน์โหลด</a>
      </div>
    </div>
    <div class="card shadow mb-3">
      <div class="card-body">
        <p>ตัวอย่างรายละเอียดของรายวิชา มคอ.4</p>
        <a href="{{url('/documents/tqf_4_example.pdf')}}" target="_blank" class="btn btn-primary" role="button"
          download>ดาวน์โหลด</a>
      </div>
    </div>
    <div class="card shadow mb-3">
      <div class="card-body">
        <p>ตัวอย่างรายละเอียดของรายวิชา มคอ.5</p>
        <a href="{{url('/documents/tqf_5_example.pdf')}}" target="_blank" class="btn btn-primary" role="button"
          download>ดาวน์โหลด</a>
      </div>
    </div>
    <div class="card shadow mb-3">
      <div class="card-body">
        <p>ตัวอย่างรายละเอียดของรายวิชา มคอ.6</p>
        <a href="{{url('/documents/tqf_6_example.pdf')}}" target="_blank" class="btn btn-primary" role="button"
          download>ดาวน์โหลด</a>
      </div>
    </div>
  </div>
</div>
@endsection