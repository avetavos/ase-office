@extends('layouts.main')
@section('content')
<div class="row p-3">
  <div class="col-12">
    <h3>ตัวอย่างเอกสารแบบฟอร์มรายงานผลการดำเนินโครงการ</h3>
    <hr />
  </div>
  <div class="col-12">
    <div class="card shadow">
      <div class="card-body">
        <p>แบบฟอร์มรายงานผลการดำเนินโครงการ</p>
        <a href="{{url('/documents/project_report_example.docx')}}" target="_blank" class="btn btn-primary"
          role="button" download>ดาวน์โหลด</a>
      </div>
    </div>
  </div>
</div>
@endsection