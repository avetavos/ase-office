@extends('layouts.main')
@section('content')
<div class="row p-3">
  <div class="col-12">
    <h3>ตัวอย่างเอกสารประกอบการทวนสอบ</h3>
    <hr />
  </div>
  <div class="col-12 mb-3">
    <div class="card shadow">
      <div class="card-body">
        <p>เอกสารประกอบการทวนสอบ</p>
        <a href="{{url('/documents/exam_document.docx')}}" target="_blank" class="btn btn-primary" role="button"
          download>ดาวน์โหลด</a>
      </div>
    </div>
  </div>
  <div class="col-12">
    <div class="card shadow">
      <div class="card-body">
        <p>ใบปะหน้าส่งเอกสารทวนสอบ</p>
        <a href="{{url('/documents/exam_cover.docx')}}" target="_blank" class="btn btn-primary" role="button"
          download>ดาวน์โหลด</a>
      </div>
    </div>
  </div>
</div>
@endsection