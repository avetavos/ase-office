<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>ASE Office</title>
  <link rel="stylesheet" href="{{asset('css/app.css')}}">
  @yield('custom_style')
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark d-block d-md-none pl-1 fixed-top">
    <button class="navbar-toggler ml-auto border-0" type="button" id="show-sidebar">
      <i class="fas fa-bars text-light"></i>
    </button>
    <a class="navbar-brand ml-1 text-warning" href="{{url('/')}}">
      <span class="text-kku">ASE</span> Office
    </a>
  </nav>
  <div class="page-wrapper chiller-theme">
    <nav id="sidebar" class="sidebar-wrapper">
      <div class="sidebar-content">
        <div class="sidebar-brand">
          <a href="/" class="text-warning"><span class="text-kku">ASE</span> Office</a>
        </div>
        <div class="sidebar-header">
          <div class="user-info">
            <span class="user-name">{{ Auth::user()->name }}</span> @if (Auth::user()->admin === 1)
            <span class="user-role">ผู้ดูแลระบบ</span> @else
            <span class="user-role">ผู้ใช้ทั่วไป</span> @endif
          </div>
        </div>
        <!-- sidebar-search  -->
        <div class="sidebar-menu">
          <ul>
            <li class="header-menu">
              <span>เมนู</span>
            </li>
            @if (Auth::user()->admin === 1)
            @include('inc.admin_menu') @else
            @include('inc.user_menu') @endif
          </ul>
        </div>
        <!-- sidebar-menu  -->
      </div>
      <div class="sidebar-background d-block d-md-none">
      </div>
      <!-- sidebar-content  -->
      <div class="sidebar-footer text-light">
        <a href="{{url('/setting')}}" class="text-dark">
          <i class="fa fa-cog text-warning"></i> ตั้งค่าบัญชี
        </a>
        <a href="#" class="text-dark" data-toggle="modal" data-target="#logoutModal">
          <i class="fa fa-power-off text-danger"></i> ออกจากระบบ
        </a>
      </div>
    </nav>
    <!-- sidebar-wrapper  -->
    <main class="page-content">
      @yield('content')
    </main>
    <!-- page-content" -->
  </div>
  @include('inc.logout')
  <!-- page-wrapper -->
  <script src="{{asset('js/app.js')}}"></script>
  <script src="{{asset('js/qrcode.js')}}"></script>
  @yield('custom_script')
</body>

</html>