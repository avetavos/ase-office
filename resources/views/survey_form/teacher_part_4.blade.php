<div class="hide" id="section-4">
    <h5 class="survey-lg-banner">ด้านการจัดการข้อร้องเรียน</h5>
    <p class="survey-text-detail">โปรดเลือกระดับความพึงพอใจตามความคิดเห็นของท่านมากที่สุด</p>
    <div>
        <h5 class="survey-question">4.1 นักศึกษาเคยมีข้อร้องเรียนต่อการจัดการหลักสูตรหรือคณะหรือไม่ *</h5>
        <p>
            <label>
                <input type="radio" name="4_1" id="" class="with-gap" value="ไม่เคย หากไม่เคยให้สิ้นสุดการทำแบบสำรวจ">
                <span class="black-text survey-choice">ไม่เคย หากไม่เคยให้สิ้นสุดการทำแบบสำรวจ</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_1" id="" class="with-gap" value="เคย หากเคยให้ตอบคำถามในข้อถัดไป">
                <span class="black-text survey-choice">เคย หากเคยให้ตอบคำถามในข้อถัดไป</span>
            </label>
        </p>
    </div>
    <div>
        <h5 class="survey-question">4.2 หากนักศึกษาเคยมีข้อร้องเรียน ข้อร้องเรียนได้รับการแก้ไขหรือไม่</h5>
        <p>
            <label>
                <input type="radio" name="4_2" id="" class="with-gap" value="ได้รับการแก้ไข">
                <span class="black-text survey-choice">ได้รับการแก้ไข</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_2" id="" class="with-gap" value="ไม่ได้รับการแก้ไข">
                <span class="black-text survey-choice">ไม่ได้รับการแก้ไข</span>
            </label>
        </p>
    </div>
    <div>
        <h5 class="survey-question">4.3 ในกรณีที่มีการจัดการข้อร้องเรียน ท่านพึงพอใจอยู่ในระดับใด</h5>
        <p>
            <label>
                <input type="radio" name="4_3" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_3" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_3" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_3" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_3" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h5 class="survey-question">ข้อเสนอแนะต่อระบบอาจารย์ที่ปรึกษา</h5>
        <div class="input-field col s12">
            <textarea id="textarea1" class="materialize-textarea" name="4_4"></textarea>
            <label for="textarea1">คำตอบของคุณ</label>
        </div>
    </div>
    <div class="mt-4">
        <a class="waves-effect waves-blue btn-flat mr-3" id="btn-prev-3">ย้อนกลับ</a>
        <a class="waves-effect waves-blue btn-flat" id="btn-4">ส่ง</a>
    </div>
</div>
