<div class="hide" id="section-5">
    <h5 class="survey-lg-banner">ด้านการพัฒนานักศึกษา</h5>
    <p class="survey-text-detail">โปรดใส่ข้อมูลตามความคิดเห็นของท่านมากที่สุด</p>
    <h5 class="survey-banner">ระดับความพึงพอใจด้านการพัฒนานักศึกษา</h5>
    <div>
        <h5 class="survey-question">4.1 ความพึงพอใจโดยรวมด้านการพัฒนานักศึกษา</h5>
        <p>
            <label>
                <input type="radio" name="5_1" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="5_1" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="5_1" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="5_1" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="5_1" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h5 class="survey-question">4.2 ความพึงพอใจด้านการพัฒนานักศึกษา</h5>
        <h6>(1) มีการจัดกิจกรรม ที่สอดคล้องกับคุณลักษณะเด่นของบัณฑิตมหาวิทยาลัยขอนแก่นคือ บัณฑิตพร้อมทำงาน (Ready to Work) ประกอบด้วย
            1) มีประสบการณ์พร้อมปฏิบัติงานในวิชาชีพ 2) พร้อมต่อการเปลี่ยนแปลง และ 3 ) เรียนรู้ตลอดชีวิต
        </h6>
        <p>
            <label>
                <input type="radio" name="5_2_(1)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="5_2_(1)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="5_2_(1)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="5_2_(1)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="5_2_(1)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(2) มีการจัดกิจกรรมที่สอดคล้องกับทักษะในศตวรรษที่ 21 ได้แก่ 1) ทักษะด้านการเรียนรู้และนวัตกรรม 2) ทักษะสารสนเทศ สื่อและเทคโนโลยี
            3) ทักษะชีวิตและอาชีพ</h6>
        <p>
            <label>
                <input type="radio" name="5_2_(2)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="5_2_(2)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="5_2_(2)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="5_2_(2)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="5_2_(2)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(3) มีการส่งเสริมให้นักศึกษาแก้ปัญหาต่างๆผ่านกิจกรรมพัฒนานักศึกษา</h6>
        <p>
            <label>
                <input type="radio" name="5_2_(3)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="5_2_(3)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="5_2_(3)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="5_2_(3)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="5_2_(3)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(4) มีการส่งเสริมให้นักศึกษาได้เข้าร่วมกิจกรรมเพื่อพัฒนาทักษะทางวิชาการและวิชาชีพของคณะฯหรือมหาวิทยาลัย</h6>
        <p>
            <label>
                <input type="radio" name="5_2_(4)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="5_2_(4)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="5_2_(4)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="5_2_(4)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="5_2_(4)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(5) มีการสนับสนุนทุนการศึกษา เช่น ทุนเรียนดี ทุนกิจกรรมดี ทุนขาดแคลน เป็นต้น</h6>
        <p>
            <label>
                <input type="radio" name="5_2_(5)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="5_2_(5)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="5_2_(5)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="5_2_(5)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="5_2_(5)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h5 class="survey-question">4.3 ท่าน<b class="red-text">ไม่พึงพอใจ</b>ด้านการพัฒนานักศึกษาข้อใด เลือกได้มากกว่า 1 ข้อ</h5>
        <p>
            <label>
                <input type="checkbox" name="5_3[]" id="" class="filled-in" value="มีการจัดกิจกรรมที่สอดคล้องกับคุณลักษณะเด่นของบัณฑิตมหาวิทยาลัยขอนแก่นคือ บัณฑิตพร้อมทำงาน (Ready to Work) ประกอบด้วย 1) มีประสบการณ์พร้อมปฏิบัติงานในวิชาชีพ 2) พร้อมต่อการเปลี่ยนแปลง และ 3) เรียนรู้ตลอดชีวิต">
                <span class="black-text survey-choice">มีการจัดกิจกรรม ที่สอดคล้องกับคุณลักษณะเด่นของบัณฑิตมหาวิทยาลัยขอนแก่นคือ
                    บัณฑิตพร้อมทำงาน (Ready to Work) ประกอบด้วย 1) มีประสบการณ์พร้อมปฏิบัติงานในวิชาชีพ 2)
                    พร้อมต่อการเปลี่ยนแปลง และ 3) เรียนรู้ตลอดชีวิต</span>
            </label>
        </p>
        <p>
            <label>
                <input type="checkbox" name="5_3[]" id="" class="filled-in" value="มีการจัดกิจกรรมที่สอดคล้องกับทักษะในศตวรรษที่ 21 ได้แก่ 1) ทักษะด้านการเรียนรู้และนวัตกรรม 2) ทักษะสารสนเทศ สื่อและเทคโนโลยี 3) ทักษะชีวิตและอาชีพ">
                <span class="black-text survey-choice">มีการจัดกิจกรรมที่สอดคล้องกับทักษะในศตวรรษที่ 21 ได้แก่ 1)
                    ทักษะด้านการเรียนรู้และนวัตกรรม 2) ทักษะสารสนเทศ สื่อและเทคโนโลยี 3) ทักษะชีวิตและอาชีพ</span>
            </label>
        </p>
        <p>
            <label>
                <input type="checkbox" name="5_3[]" id="" class="filled-in" value="มีการส่งเสริมให้นักศึกษาแก้ปัญหาต่างๆผ่านกิจกรรมพัฒนานักศึกษา">
                <span class="black-text survey-choice">มีการส่งเสริมให้นักศึกษาแก้ปัญหาต่างๆผ่านกิจกรรมพัฒนานักศึกษา</span>
            </label>
        </p>
        <p>
            <label>
                <input type="checkbox" name="5_3[]" id="" class="filled-in" value="มีการส่งเสริมให้นักศึกษาได้เข้าร่วมกิจกรรมเพื่อพัฒนาทักษะทางวิชาการและวิชาชีพของคณะฯหรือมหาวิทยาลัย">
                <span
                    class="black-text">มีการส่งเสริมให้นักศึกษาได้เข้าร่วมกิจกรรมเพื่อพัฒนาทักษะทางวิชาการและวิชาชีพของคณะฯหรือมหาวิทยาลัย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="checkbox" name="5_3[]" id="" class="filled-in" value="มีการสนับสนุนทุนการศึกษา เช่น ทุนเรียนดี ทุนกิจกรรมดี ทุนขาดแคลน เป็นต้น">
                <span class="black-text survey-choice">มีการสนับสนุนทุนการศึกษา เช่น ทุนเรียนดี ทุนกิจกรรมดี ทุนขาดแคลน เป็นต้น</span>
            </label>
        </p>
        <p>
            <label>
                <input type="checkbox" name="5_3[]" id="" class="filled-in" value="ไม่มี">
                <span class="black-text survey-choice">ไม่มี</span>
            </label>
        </p>
    </div>
    <div>
        <h5 class="survey-question">ข้อคิดเห็นหรือข้อเสนอแนะด้านการพัฒนานักศึกษา</h5>
        <div class="input-field col s12">
            <textarea id="textarea1" class="materialize-textarea" name="5_4"></textarea>
            <label for="textarea1">คำตอบของคุณ</label>
        </div>
    </div>
    <div class="mt-4">
        <a class="waves-effect waves-blue btn-flat mr-3" id="btn-prev-4">ย้อนกลับ</a>
        <a class="waves-effect waves-blue btn-flat" id="btn-5">ถัดไป</a>
    </div>
</div>