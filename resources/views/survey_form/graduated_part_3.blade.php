<div class="hide" id="section-3">
    <p class="survey-text-detail">โปรดเลือกระดับความพึงพอใจตามความคิดเห็นของท่านมากที่สุด</p>
    <h5 class="survey-banner">2 ด้านความรู้ ความสามารถทางวิชาการ</h5>
    <div>
        <h6>(1) มีองค์ความรู้ในสาขาวิชาที่เรียนและเข้าใจขั้นตอน วิธีการปฏิบัติงาน รวมถึงกฎระเบียบข้อบังคับในสาขาวิชาชีพ</h5>
        <p>
            <label>
                <input type="radio" name="3_1_(1)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(1)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(1)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(1)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(1)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(2) มีความสามารถในการนาความรู้ประยุกต์ใช้ในการปฏิบัติงาน และมีความรอบรู้ในการเพิ่มพูนหรือต่อยอดองค์ความรู้</h5>
        <p>
            <label>
                <input type="radio" name="3_1_(2)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(2)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(2)" id="" class="with-gap">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(2)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(2)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(3) ความสามารถในการถ่ายทอดและเผยแพร่ความรู้</h6>
        <p>
            <label>
                <input type="radio" name="3_1_(3)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(3)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(3)" id="" class="with-gap">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(3)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(3)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div class="mt-4">
      <a class="waves-effect waves-blue btn-flat mr-3" id="btn-prev-2">ย้อนกลับ</a>
      <a class="waves-effect waves-blue btn-flat" id="btn-3">ถัดไป</a>
    </div>
</div>
