<div class="hide" id="section-8">
  <h5 class="survey-lg-banner">ข้อเสนอแนะ</h5>
  <h5 class="survey-banner">ข้อเสนอแนะที่มีต่อหลักสูตร</h5>
  <div class="input-field col s12">
    <textarea id="textarea1" class="materialize-textarea" name="8_1"></textarea>
    <label for="textarea1">คำตอบของคุณ</label>
  </div>
  <div class="mt-4">
    <a class="waves-effect waves-blue btn-flat mr-3" id="btn-prev-7">ย้อนกลับ</a>
    <a class="waves-effect waves-blue btn-flat" id="btn-8">ส่ง</a>
  </div>
</div>