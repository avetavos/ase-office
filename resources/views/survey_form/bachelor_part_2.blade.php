<div class="hide" id="section-2">
    <h5 class="survey-lg-banner">การจัดการหลักสูตรและการเรียนการสอน</h5>
    <p class="survey-text-detail">โปรดใส่ข้อมูลตามความคิดเห็นของท่านมากที่สุด</p>
    <h5 class="survey-banner">ระดับความพึงพอใจการจัดการหลักสูตรและการเรียนการสอน</h5>
    <div>
        <h5 class="survey-question">1.1 ความพึงพอใจโดยรวมด้านการจัดการหลักสูตรและการเรียนการสอน</h5>
        <p>
            <label>
                <input type="radio" name="2_1" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_1" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_1" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_1" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_1" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h5 class="survey-question">1.2 ความพึงพอใจด้านการจัดการหลักสูตรและการเรียนการสอน</h5>
        <h6>(1) มีรายวิชาในหลักสูตรมีความหลายหมายและทันสมัย</h6>
        <p>
            <label>
                <input type="radio" name="2_2_(1)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(1)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(1)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(1)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(1)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(2) มีการจัดการเรียนการสอนสอดคล้องกับวัตถุประสงค์ของหลักสูตร</h6>
        <p>
            <label>
                <input type="radio" name="2_2_(2)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(2)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(2)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(2)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(2)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(3) มีการจัดแผนการเรียนที่เหมาะสม</h6>
        <p>
            <label>
                <input type="radio" name="2_2_(3)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(3)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(3)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(3)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(3)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(4) มีการจัดกิจกรรมเสริมหลักสูตรที่เหมาะสม</h6>
        <p>
            <label>
                <input type="radio" name="2_2_(4)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(4)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(4)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(4)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(4)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(5) มีการกระตุ้นทักษะทางปัญญา การวิเคราะห์ และทักษะการแก้ปัญหา</h6>
        <p>
            <label>
                <input type="radio" name="2_2_(5)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(5)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(5)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(5)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(5)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(6) มีการใช้สื่อการสอนที่เหมาะสม</h6>
        <p>
            <label>
                <input type="radio" name="2_2_(6)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(6)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(6)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(6)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(6)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(7) มีการจัดการเรียนการสอนที่ส่งเสริมทักษะการเรียนรู้ 5 ด้าน ได้แก่ 1) ด้านคุณธรรม จริยธรรม 2) ด้านความรู้ 3) ด้านทักษะทางปัญญา
            4) ด้านทักษะความสัมพันธ์ระหว่างบุคคลและความรับผิดชอบ 5) ด้านทักษะการวิเคราะห์เชิงตัวเลข การสื่อสารและการใช้เทคโนโลยีสารสนเทศ</h6>
        <p>
            <label>
                <input type="radio" name="2_2_(7)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(7)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(7)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(7)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_2_(7)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h5 class="survey-question">1.3 ท่าน<b class="red-text">ไม่พึงพอใจ</b>ในการจัดการหลักสูตรและการเรียนการสอน เลือกได้มากกว่า 1 ข้อ
        </h5>
        <p>
            <label>
                <input type="checkbox" name="2_3[]" id="" class="filled-in" value="มีรายวิชาในหลักสูตรมีความหลายหมายและทันสมัย">
                <span class="black-text survey-choice">มีรายวิชาในหลักสูตรมีความหลายหมายและทันสมัย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="checkbox" name="2_3[]" id="" class="filled-in" value="มีการจัดการเรียนการสอนสอดคล้องกับวัตถุประสงค์ของหลักสูตร">
                <span class="black-text survey-choice">มีการจัดการเรียนการสอนสอดคล้องกับวัตถุประสงค์ของหลักสูตร</span>
            </label>
        </p>
        <p>
            <label>
                <input type="checkbox" name="2_3[]" id="" class="filled-in" value="มีการจัดแผนการเรียนที่เหมาะสม">
                <span class="black-text survey-choice">มีการจัดแผนการเรียนที่เหมาะสม</span>
            </label>
        </p>
        <p>
            <label>
                <input type="checkbox" name="2_3[]" id="" class="filled-in" value="มีการจัดกิจกรรมเสริมหลักสูตรที่เหมาะสม">
                <span class="black-text survey-choice">มีการจัดกิจกรรมเสริมหลักสูตรที่เหมาะสม</span>
            </label>
        </p>
        <p>
            <label>
                <input type="checkbox" name="2_3[]" id="" class="filled-in" value="มีการกระตุ้นทักษะทางปัญญา การวิเคราะห์ และทักษะการแก้ปัญหา">
                <span class="black-text survey-choice">มีการกระตุ้นทักษะทางปัญญา การวิเคราะห์ และทักษะการแก้ปัญหา</span>
            </label>
        </p>
        <p>
            <label>
                <input type="checkbox" name="2_3[]" id="" class="filled-in" value="มีการใช้สื่อการสอนที่เหมาะสม">
                <span class="black-text survey-choice">มีการใช้สื่อการสอนที่เหมาะสม</span>
            </label>
        </p>
        <p>
            <label>
                <input type="checkbox" name="2_3[]" id="" class="filled-in" value="มีการจัดการเรียนการสอนที่ส่งเสริมทักษะการเรียนรู้ 5 ด้าน ได้แก่ 1) ด้านคุณธรรม จริยธรรม 2) ด้านความรู้ 3) ด้านทักษะทางปัญญา 4) ด้านทักษะความสัมพันธ์ระหว่างบุคคลและความรับผิดชอบ 5) ด้านทักษะการวิเคราะห์เชิงตัวเลขการสื่อสารและการใช้เทคโนโลยีสารสนเทศ">
                <span class="black-text survey-choice">มีการจัดการเรียนการสอนที่ส่งเสริมทักษะการเรียนรู้ 5
                    ด้าน ได้แก่ 1) ด้านคุณธรรม จริยธรรม 2) ด้านความรู้ 3) ด้านทักษะทางปัญญา
                    4) ด้านทักษะความสัมพันธ์ระหว่างบุคคลและความรับผิดชอบ 5)
                    ด้านทักษะการวิเคราะห์เชิงตัวเลข
                    การสื่อสารและการใช้เทคโนโลยีสารสนเทศ</span>
            </label>
        </p>
        <p>
            <label>
                <input type="checkbox" name="2_3[]" id="" class="filled-in" value="ไม่มี">
                <span class="black-text survey-choice">ไม่มี</span>
            </label>
        </p>
    </div>
    <div>
        <h5 class="survey-question">ข้อคิดเห็นหรือข้อเสนอแนะด้านการจัดการหลักสูตรและการเรียนการสอน</h5>
        <div class="input-field">
            <textarea id="textarea1" class="materialize-textarea" name="2_4"></textarea>
            <label for="textarea1">คำตอบของคุณ</label>
        </div>
    </div>
    <div class="mt-4">
        <a class="waves-effect waves-blue btn-flat mr-3" id="btn-prev-1">ย้อนกลับ</a>
        <button type="button" class="waves-effect waves-blue btn-flat" id="btn-2">ถัดไป</button>
    </div>
</div>