<div class="hide" id="section-3">
    <h5 class="survey-lg-banner">ด้านอาจารย์ผู้สอน</h5>
    <p class="survey-text-detail">โปรดใส่ข้อมูลตามความคิดเห็นของท่านมากที่สุด</p>
    <h5 class="survey-banner">ระดับความพึงพอใจด้านอาจารย์ผู้สอน</h5>
    <div>
        <h5 class="survey-question">2.1 ความพึงพอใจโดยรวมด้านการอาจารย์ผู้สอน</h5>
        <p>
            <label>
                <input type="radio" name="3_1" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h5 class="survey-question">2.2 ความพึงพอใจในด้านอาจารย์ผู้สอน</h5>
        <h6>(1) มีความรู้ความสามารถและความเชี่ยวชาญในรายวิชาที่สอน</h6>
        <p>
            <label>
                <input type="radio" name="3_2_(1)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_2_(1)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_2_(1)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_2_(1)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_2_(1)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(2) มีคุณธรรม จริยธรรม และเป็นแบบอย่างที่ดี</h6>
        <p>
            <label>
                <input type="radio" name="3_2_(2)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_2_(2)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_2_(2)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_2_(2)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_2_(2)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(3) มีการสนับสนุน ส่งเสริมให้นักศึกษาเรียนรู้และพัฒนาตนเองอย่างสม่ำเสมอ</h6>
        <p>
            <label>
                <input type="radio" name="3_2_(3)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_2_(3)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_2_(3)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_2_(3)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_2_(3)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(4) มีการสอนตรงตามวัตถุประสงค์ โดยใช้วิธีการสอนที่หลากหลาย และเน้นผู้เรียนเป็นสำคัญ</h6>
        <p>
            <label>
                <input type="radio" name="3_2_(4)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_2_(4)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_2_(4)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_2_(4)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_2_(4)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(5) มีการยอมรับฟังความคิดเห็นหรือข้อเสนอแนะของผู้เรียน</h6>
        <p>
            <label>
                <input type="radio" name="3_2_(5)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_2_(5)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_2_(5)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_2_(5)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_2_(5)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h5 class="survey-question">2.3 ท่าน<b class="red-text">ไม่พึงพอใจ</b>ในด้านอาจารย์ผู้สอน เลือกได้มากกว่า 1 ข้อ</h5>
        <p>
            <label>
          <input type="checkbox" name="3_3[]" id="" class="filled-in" value="มีความรู้ความสามารถและความเชี่ยวชาญในรายวิชาที่สอน">
          <span class="black-text survey-choice">มีความรู้ความสามารถและความเชี่ยวชาญในรายวิชาที่สอน</span>
        </label>
        </p>
        <p>
            <label>
          <input type="checkbox" name="3_3[]" id="" class="filled-in" value="มีคุณธรรม จริยธรรม และเป็นแบบอย่างที่ดี">
          <span class="black-text survey-choice">มีคุณธรรม จริยธรรม และเป็นแบบอย่างที่ดี</span>
        </label>
        </p>
        <p>
            <label>
          <input type="checkbox" name="3_3[]" id="" class="filled-in" value="มีการสนับสนุน ส่งเสริมให้นักศึกษาเรียนรู้และพัฒนาตนเองอย่างสม่ำเสมอ">
          <span class="black-text survey-choice">มีการสนับสนุน ส่งเสริมให้นักศึกษาเรียนรู้และพัฒนาตนเองอย่างสม่ำเสมอ</span>
        </label>
        </p>
        <p>
            <label>
          <input type="checkbox" name="3_3[]" id="" class="filled-in" value="มีการสอนตรงตามวัตถุประสงค์ โดยใช้วิธีการสอนที่หลากหลาย และเน้นผู้เรียนเป็นสำคัญ">
          <span class="black-text survey-choice">มีการสอนตรงตามวัตถุประสงค์ โดยใช้วิธีการสอนที่หลากหลาย และเน้นผู้เรียนเป็นสำคัญ</span>
        </label>
        </p>
        <p>
            <label>
          <input type="checkbox" name="3_3[]" id="" class="filled-in" value="มีการยอมรับฟังความคิดเห็นหรือข้อเสนอแนะของผู้เรียน">
          <span class="black-text survey-choice">มีการยอมรับฟังความคิดเห็นหรือข้อเสนอแนะของผู้เรียน</span>
        </label>
        </p>
        <p>
            <label>
          <input type="checkbox" name="3_3[]" id="" class="filled-in" value="ไม่มี">
          <span class="black-text survey-choice">ไม่มี</span>
        </label>
        </p>
    </div>
    <div>
        <h5 class="survey-question">ข้อคิดเห็นหรือข้อเสนอแนะด้านอาจารย์ผู้สอน</h5>
        <div class="input-field col s12">
            <textarea id="textarea1" class="materialize-textarea" name="3_4"></textarea>
            <label for="textarea1">คำตอบของคุณ</label>
        </div>
    </div>
    <div class="mt-4">
        <a class="waves-effect waves-blue btn-flat mr-3" id="btn-prev-2">ย้อนกลับ</a>
        <a class="waves-effect waves-blue btn-flat" id="btn-3">ถัดไป</a>
    </div>
</div>