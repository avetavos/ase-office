<script>
    $('#btn-1').click(function () {
        var check = true;
        $("#section-1 input:radio").each(function () {
            var name = $(this).attr("name");
            if ($("#section-1 input:radio[name=" + name + "]:checked").length == 0) {
                check = false;
            }
        });
        if (check) {
            $('#section-1').addClass('hide');
            $('#section-2').removeClass('hide');
            $(window).scrollTop(0);
        } else {
            alert('กรุณาตอบคำถามให้ครบทุกข้อ');
        }
    });

    $('#btn-2').click(function () {
        var check = true;
        $('#section-2 input:radio').each(function () {
            var name = $(this).attr('name');
            if ($('#section-2 input:radio[name="' + name + '"]:checked').length == 0) {
                check = false;
            }
        });
        $('#section-2 input:checkbox').each(function () {
            var name = $(this).attr('name');
            if (
                $('#section-2 input:checkbox[name="' + name + '"]:checked').length == 0) {
                check = false;
            }
        });
        if (check) {
            $('#section-2').addClass('hide');
            $('#section-3').removeClass('hide');
            $(window).scrollTop(0);
        } else {
            alert('กรุณาตอบคำถามให้ครบทุกข้อ');
        }
    });

    $('#btn-3').click(function () {
        var check = true;
        $('#section-3 input:radio').each(function () {
            var name = $(this).attr('name');
            if ($('#section-3 input:radio[name="' + name + '"]:checked').length == 0) {
                check = false;
            }
        });
        $('#section-3 input:checkbox').each(function () {
            var name = $(this).attr('name');
            if (
                $('#section-3 input:checkbox[name="' + name + '"]:checked').length == 0) {
                check = false;
            }
        });
        if (check) {
            $('#section-3').addClass('hide');
            $('#section-4').removeClass('hide');
            $(window).scrollTop(0);
        } else {
            alert('กรุณาตอบคำถามให้ครบทุกข้อ');
        }
    });

    $('#btn-4').click(function () {
        var check = true;
        $('#section-4 input:radio').each(function () {
            var name = $(this).attr('name');
            if ($('#section-4 input:radio[name="' + name + '"]:checked').length == 0) {
                check = false;
            }
        });
        $('#section-4 input:checkbox').each(function () {
            var name = $(this).attr('name');
            if (
                $('#section-4 input:checkbox[name="' + name + '"]:checked').length == 0) {
                check = false;
            }
        });
        if (check) {
            $('form').submit();
        } else {
            alert('กรุณาตอบคำถามให้ครบทุกข้อ');
        }
    });

    $('#btn-prev-1').click(function () {
        $('#section-1').removeClass('hide');
        $('#section-2').addClass('hide');
        $(window).scrollTop(0);
    });

    $('#btn-prev-2').click(function () {
        $('#section-1').addClass('hide');
        $('#section-2').removeClass('hide');
        $('#section-3').addClass('hide');
        $(window).scrollTop(0);
    });

    $('#btn-prev-3').click(function () {
        $('#section-1').addClass('hide');
        $('#section-2').addClass('hide');
        $('#section-3').removeClass('hide');
        $('#section-4').addClass('hide');
        $(window).scrollTop(0);
    });

</script>