<div class="hide" id="section-6">
    <h5 class="survey-lg-banner">ด้านสิ่งสนับสนุนการเรียนรู้</h5>
    <p class="survey-text-detail">โปรดใส่ข้อมูลตามความคิดเห็นของท่านมากที่สุด</p>
    <h5 class="survey-banner">ระดับความพึงพอใจด้านสิ่งสนับสนุนการเรียนรู้</h5>
    <div>
        <h5 class="survey-question">5.1 ความพึงพอใจโดยรวมด้านสิ่งสนับสนุนการเรียนรู้</h5>
        <p>
            <label>
                <input type="radio" name="6_1" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="6_1" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="6_1" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="6_1" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="6_1" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h5 class="survey-question">5.2 ความพึงพอใจด้านสิ่งสนับสนุนการเรียนรู้</h5>
        <h6>(1) มีสื่อการเรียนการสอน อุปกรณ์การศึกษาและห้องปฏิบัติการที่ทันสมัยพร้อมใช้งานและเพียงพอ</h6>
        <p>
            <label>
                <input type="radio" name="6_2_(1)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="6_2_(1)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="6_2_(1)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="6_2_(1)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="6_2_(1)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(2) มีการให้บริการห้องสมุด เอกสาร หนังสือ ตำรา และแหล่งสารสนเทศอื่นๆ ที่ทันสมัย</h6>
        <p>
            <label>
                <input type="radio" name="6_2_(2)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="6_2_(2)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="6_2_(2)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="6_2_(2)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="6_2_(2)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(3) มีการให้บริการด้านเทคโนโลยีสารสนเทศ (โปรแกรมสนับสนุนการเรียน โปรแกรมประยุกต์(Application software) คอมพิวเตอร์
            ปริ๊นเตอร์)
        </h6>
        <p>
            <label>
                <input type="radio" name="6_2_(3)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="6_2_(3)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="6_2_(3)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="6_2_(3)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="6_2_(3)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(4) มีการให้บริการอินเตอร์เน็ตและ WIFI ที่ครอบคลุมพื้นที่การจัดการเรียนการสอน</h6>
        <p>
            <label>
                <input type="radio" name="6_2_(4)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="6_2_(4)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="6_2_(4)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="6_2_(4)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="6_2_(4)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h5 class="survey-question">5.3 ท่าน<b class="red-text">ไม่พึงพอใจ</b>ด้านสิ่งสนับสนุนการเรียนรู้ข้อใด เลือกได้มากกว่า 1 ข้อ</h5>
        <p>
            <label>
          <input type="checkbox" name="6_3[]" id="" class="filled-in" value="มีสื่อการเรียนการสอน อุปกรณ์การศึกษาและห้องปฏิบัติการที่ทันสมัยพร้อมใช้งานและเพียงพอ">
          <span class="black-text survey-choice">มีสื่อการเรียนการสอน อุปกรณ์การศึกษาและห้องปฏิบัติการที่ทันสมัยพร้อมใช้งานและเพียงพอ</span>
        </label>
        </p>
        <p>
            <label>
          <input type="checkbox" name="6_3[]" id="" class="filled-in" value="มีการให้บริการห้องสมุด เอกสาร หนังสือ ตำรา และแหล่งสารสนเทศอื่นๆ ที่ทันสมัย">
          <span class="black-text survey-choice">มีการให้บริการห้องสมุด เอกสาร หนังสือ ตำรา และแหล่งสารสนเทศอื่นๆ ที่ทันสมัย</span>
        </label>
        </p>
        <p>
            <label>
          <input type="checkbox" name="6_3[]" id="" class="filled-in" value="มีการให้บริการด้านเทคโนโลยีสารสนเทศ (โปรแกรมสนับสนุนการเรียน โปรแกรมประยุกต์(Application software) คอมพิวเตอร์ ปริ๊นเตอร์)">
          <span class="black-text survey-choice">มีการให้บริการด้านเทคโนโลยีสารสนเทศ (โปรแกรมสนับสนุนการเรียน โปรแกรมประยุกต์(Application software) คอมพิวเตอร์ ปริ๊นเตอร์)</span>
        </label>
        </p>
        <p>
            <label>
          <input type="checkbox" name="6_3[]" id="" class="filled-in" value="มีการให้บริการอินเตอร์เน็ตและ WIFI ที่ครอบคลุมพื้นที่การจัดการเรียนการสอน">
          <span class="black-text survey-choice">มีการให้บริการอินเตอร์เน็ตและ WIFI ที่ครอบคลุมพื้นที่การจัดการเรียนการสอน</span>
        </label>
        </p>
    </div>
    <div>
        <h5 class="survey-question">ข้อคิดเห็นหรือข้อเสนอแนะด้านสิ่งสนับสนุนการเรียนรู้</h5>
        <div class="input-field col s12">
            <textarea id="textarea1" class="materialize-textarea" name="6_4"></textarea>
            <label for="textarea1">คำตอบของคุณ</label>
        </div>
    </div>
    <div class="mt-4">
        <a class="waves-effect wave-orange btn-flat mr-3" id="btn-prev-5">ย้อนกลับ</a>
        <a class="waves-effect wave-orange btn-flat" id="btn-6">ถัดไป</a>
    </div>
</div>