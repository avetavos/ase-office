<div class="hide" id="section-4">
    <h5 class="survey-lg-banner">ด้านการวัดและประเมินผล</h5>
    <p class="survey-text-detail">โปรดใส่ข้อมูลตามความคิดเห็นของท่านมากที่สุด</p>
    <h5 class="survey-banner">ระดับความพึงพอใจด้านการวัดและประเมินผล</h5>
    <div>
        <h5 class="survey-question">3.1 ความพึงพอใจโดยรวมด้านการวัดและประเมินผล</h5>
        <p>
            <label>
                <input type="radio" name="4_1" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_1" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_1" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_1" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_1" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h5 class="survey-question">3.2 ความพึงพอใจด้านการวัดและประเมินผล</h5>
        <h6>(1) มีวิธีการวัดและประเมินผลสอดคล้องกับวัตถุประสงค์การเรียนรู้ของเนื้อหารายวิชา</h6>
        <p>
            <label>
                <input type="radio" name="4_2_(1)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_2_(1)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_2_(1)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_2_(1)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_2_(1)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(2) มีการวัดและประเมินที่เป็นธรรม ถูกต้อง โปร่งใส ตรวจสอบได้</h6>
        <p>
            <label>
                <input type="radio" name="4_2_(2)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_2_(2)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_2_(2)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_2_(2)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_2_(2)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(3) มีการประเมินความพึงพอใจในการจัดการเรียนการสอนทุกรายวิชาในแต่ละภาคการศึกษา</h6>
        <p>
            <label>
                <input type="radio" name="4_2_(3)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_2_(3)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_2_(3)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_2_(3)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_2_(3)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h5 class="survey-question">3.3 ท่าน<b class="red-text">ไม่พึงพอใจ</b>ในด้านการวัดและประเมินผลข้อใด เลือกได้มากกว่า 1 ข้อ</h5>
        <p>
            <label>
          <input type="checkbox" name="4_3[]" id="" class="checkbox filled-in" value="มีวิธีการวัดและประเมินผลสอดคล้องกับวัตถุประสงค์การเรียนรู้ของเนื้อหารายวิชา">
          <span class="black-text survey-choice">มีวิธีการวัดและประเมินผลสอดคล้องกับวัตถุประสงค์การเรียนรู้ของเนื้อหารายวิชา</span>
        </label>
        </p>
        <p>
            <label>
          <input type="checkbox" name="4_3[]" id="" class="checkbox filled-in" value="มีการวัดและประเมินที่เป็นธรรม ถูกต้อง โปร่งใส ตรวจสอบได้">
          <span class="black-text survey-choice">มีการวัดและประเมินที่เป็นธรรม ถูกต้อง โปร่งใส ตรวจสอบได้</span>
        </label>
        </p>
        <p>
            <label>
          <input type="checkbox" name="4_3[]" id="" class="checkbox filled-in" value="มีการประเมินความพึงพอใจในการจัดการเรียนการสอนทุกรายวิชาในแต่ละภาคการศึกษา">
          <span class="black-text survey-choice">มีการประเมินความพึงพอใจในการจัดการเรียนการสอนทุกรายวิชาในแต่ละภาคการศึกษา</span>
        </label>
        </p>
        <p>
            <label>
          <input type="checkbox" name="4_3[]" id="" class="checkbox filled-in" value="ไม่มี">
          <span class="black-text survey-choice">ไม่มี</span>
        </label>
        </p>
    </div>
    <div>
        <h5 class="survey-question">ข้อคิดเห็นหรือข้อเสนอแนะด้านการวัดและประเมินผล</h5>
        <div class="input-field col s12">
            <textarea id="textarea1" class="materialize-textarea" name="4_4"></textarea>
            <label for="textarea1">คำตอบของคุณ</label>
        </div>
    </div>
    <div class="mt-4">
        <a class="waves-effect wave-orange btn-flat mr-3" id="btn-prev-3">ย้อนกลับ</a>
        <a class="waves-effect wave-orange btn-flat" id="btn-4">ถัดไป</a>
    </div>
</div>