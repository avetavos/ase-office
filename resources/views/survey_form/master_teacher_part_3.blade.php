<div class="hide" id="section-3">
    <h5 class="survey-lg-banner">ด้านการพัฒนาอาจารย์ผู้รับผิดชอบหลักสูตร</h5>
    <p class="survey-text-detail">โปรดเลือกระดับความพึงพอใจตามความคิดเห็นของท่านมากที่สุด</p>
    <h5 class="survey-banner">ระดับความพึงพอใจด้านการพัฒนาอาจารย์ผู้รับผิดชอบหลักสูตร</h5>
    <div>
        <h6>(1) มีการวางแผนในการพัฒนาตนเองในด้านวิชาการอย่างเป็นระบบต่อปีการศึกษา</h6>
        <p>
            <label>
                <input type="radio" name="3_1_(1)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(1)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(1)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(1)" id="" class="with-gap"  value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(1)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(2) มีการประชาสัมพันธ์การอบรมต่าง ๆ ที่เป็นประโยชน์ต่อการเรียนการสอนให้อาจารย์ผู้รับผิดชอบหลักสูตรได้รับทราบอยู่เสมอรายวิชาที่สอน</h6>
        <p>
            <label>
                <input type="radio" name="3_1_(2)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(2)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(2)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(2)" id="" class="with-gap"  value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(2)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(3) อาจารย์ได้รับการสนับสนุนในการเข้าร่วมโครงการพัฒนาตนเองในแต่ละครั้ง</h6>
        <p>
            <label>
                <input type="radio" name="3_1_(3)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(3)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(3)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(3)" id="" class="with-gap"  value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(3)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(4) มีการติดตามผลการพัฒนาอาจารย์ผู้รับผิดชอบหลักสูตรอย่างเป็นระบบ</h6>
        <p>
            <label>
                <input type="radio" name="3_1_(4)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(4)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(4)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(4)" id="" class="with-gap"  value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(4)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(5) มีการพัฒนาส่งเสริม กำกับและติดตามการเข้าสู่ตำแหน่งทางวิชาการ</h6>
        <p>
            <label>
                <input type="radio" name="3_1_(5)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(5)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(5)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(5)" id="" class="with-gap"  value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(5)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div class="mt-4">
        <a class="waves-effect waves-blue btn-flat mr-3" id="btn-prev-2">ย้อนกลับ</a>
        <a class="waves-effect waves-blue btn-flat" id="btn-3">ถัดไป</a>
    </div>
</div>