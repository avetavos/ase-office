<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Student Survey</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" href="{{asset('css/survey.css')}}">
</head>

<body>
    <div class="container">
        <div class="row" id="content">
            <div class="col l7 s12 m10 mx-auto p-0" style="float:none;">
                <div class="card">
                    <div class="freebirdAccentBackground"></div>
                    <div class="card-content">
                        <h4 class="survey-header">แบบประเมินอาจารย์ประจำหลักสูตร หลักสูตร{{$survey->branch->name}}</h4>
                        <form action="/survey_form/{{$survey->id}}/save" method="post">
                            {{ csrf_field() }}
                            @include('survey_form.bachelor_teacher_part_1')
                            @include('survey_form.bachelor_teacher_part_2')
                            @include('survey_form.bachelor_teacher_part_3')
                            @include('survey_form.bachelor_teacher_part_4')
                            @include('survey_form.bachelor_teacher_part_5')
                            @include('survey_form.bachelor_teacher_part_6')
                            @include('survey_form.bachelor_teacher_part_7')
                            @include('survey_form.bachelor_teacher_part_8')
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div id="background"></div>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@include('survey_form.bachelor_teacher_script')

</html>