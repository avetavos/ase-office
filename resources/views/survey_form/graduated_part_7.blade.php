<div class="hide" id="section-7">
  <p class="survey-text-detail">โปรดเลือกระดับความพึงพอใจตามความคิดเห็นของท่านมากที่สุด</p>
  <h5 class="survey-lg-banner">ตอนที่ 3 ข้อเสนอแนะ</h5>
  <div>
    <h5 class="survey-question">1 ท่านคิดว่าหลักสูตรของมหาวิทยาลัย ควรเพิ่มทักษะหรือความรู้เรื่องใดที่จะเอื้อประโยชน์ต่อการทางานของบัณฑิตมากยิ่งขึ้น (ตอบได้มากกว่า 1 ข้อ)</h5>
    <p>
      <label>
        <input type="checkbox" name="7_1[]" id="" class="filled-in" value="ภาษาอังกฤษ">
        <span class="black-text survey-choice">ภาษาอังกฤษ</span>
      </label>
    </p>
    <p>
      <label>
        <input type="checkbox" name="7_1[]" id="" class="filled-in" value="คอมพิวเตอร์">
        <span class="black-text survey-choice">คอมพิวเตอร์</span>
      </label>
    </p>
    <p>
      <label>
        <input type="checkbox" name="7_1[]" id="" class="filled-in" value="ทักษะการคานวณ">
        <span class="black-text survey-choice">ทักษะการคานวณ</span>
      </label>
    </p>
    <p>
      <label>
        <input type="checkbox" name="7_1[]" id="" class="filled-in" value="การค้นคว้าหาข้อมูล">
        <span class="black-text survey-choice">การค้นคว้าหาข้อมูล</span>
      </label>
    </p>
    <p>
      <label>
        <input type="checkbox" name="7_1[]" id="" class="filled-in" value="การลงมือปฏิบัติจริง">
        <span class="black-text survey-choice">การลงมือปฏิบัติจริง</span>
      </label>
    </p>
    <p>
      <label>
        <input type="checkbox" name="7_1[]" id="" class="filled-in" value="เทคนิคการวิจัย">
        <span class="black-text survey-choice">เทคนิคการวิจัย</span>
      </label>
    </p>
    <p>
      <label>
        <input type="checkbox" name="7_1[]" id="" class="filled-in" value="ทักษะการวิเคราะห์ปัญหา">
        <span class="black-text survey-choice">ทักษะการวิเคราะห์ปัญหา</span>
      </label>
    </p>
    <p>
      <label>
        <input type="checkbox" name="7_1[]" id="" class="filled-in" value="ทักษะความคิดสร้างสรรค์">
        <span class="black-text survey-choice">ทักษะความคิดสร้างสรรค์</span>
      </label>
    </p>
    <p>
      <label>
        <input type="checkbox" name="7_1[]" id="" class="filled-in" value="ทักษะการพูดในที่ชุมชน">
        <span class="black-text survey-choice">ทักษะการพูดในที่ชุมชน</span>
      </label>
    </p>
    <p>
      <label>
        <input type="checkbox" name="7_1[]" id="" class="filled-in" value="อื่นๆ">
        <span class="black-text survey-choice">อื่นๆ</span>
      </label>
    </p>
  </div>
  <div>
    <h5 class="survey-question">ข้อเสนอแนะเกี่ยวกับการพัฒนาหลักสูตร</h5>
    <div class="input-field col s12">
        <textarea id="textarea1" class="materialize-textarea"  name="7_2"></textarea>
        <label for="textarea1">คำตอบของคุณ</label>
      </div>
  </div>
  <div class="mt-4">
      <a class="waves-effect waves-blue btn-flat mr-3" id="btn-prev-6">ย้อนกลับ</a>
      <a class="waves-effect waves-blue btn-flat" id="btn-7">ส่ง</a>
    </div>
</div>