<div class="hide" id="section-4">
    <h5 class="survey-lg-banner">ด้านการกำกับติดตามคุณภาพของหลักสูตร</h5>
    <p class="survey-text-detail">โปรดเลือกระดับความพึงพอใจตามความคิดเห็นของท่านมากที่สุด</p>
    <h5 class="survey-banner">ระดับความพึงพอใจด้านการกำกับติดตามคุณภาพของหลักสูตร</h5>
    <div>
        <h6>(1) มีการกำกับและติดตามการจัดทำรายละเอียดของรายวิชาและประสบการณ์ภาคสนาม(ถ้ามี) ตามแบบ มคอ.3 และ มคอ.4 ครบทุกรายวิชาก่อนการเปิดสอนในแต่ละภาคการศึกษ</h6>
        <p>
            <label>
                <input type="radio" name="4_1_(1)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_1_(1)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_1_(1)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_1_(1)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_1_(1)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(2) มีการกำกับและติดตามการจัดทำรายงานผลการดำเนินการของรายวิชา และประสบการณ์ภาคสนาม (ถ้ามี) ตามแบบ มคอ.5 และ มคอ.6
            ครบทุกรายวิชา หลังสิ้นสุดภาคการศึกษาที่เปิสอนให้</h6>
        <p>
            <label>
                <input type="radio" name="4_1_(2)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_1_(2)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_1_(2)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_1_(2)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_1_(2)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(3) มีการทวนสอบผลสัมฤทธิ์ของนักศึกษาตามมาตรฐานผลการเรียนรู้ ที่กำหนดใน มคอ.3 และมคอ.4 (ถ้ามี) อย่างน้อยร้อยละ 25
            ของรายวิชาที่เปิดสอนในแต่ละปีการศึกษา
        </h6>
        <p>
            <label>
                <input type="radio" name="4_1_(3)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_1_(3)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_1_(3)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_1_(3)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="4_1_(3)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div class="mt-4">
        <a class="waves-effect waves-blue btn-flat mr-3" id="btn-prev-3">ย้อนกลับ</a>
        <a class="waves-effect waves-blue btn-flat" id="btn-4">ถัดไป</a>
    </div>
</div>