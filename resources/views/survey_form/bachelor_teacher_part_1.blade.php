<div id="section-1">
  <p class="survey-text-detail">
    แบบประเมินนี้มีวัตถุประสงค์เพื่อสำรวจความพึงพอใจของอาจารย์ประจำหลักสูตรที่มีต่อคุณภาพหลักสูตรในสังกัดของคณะวิทยาศาสตร์ประยุกต์และวิศวกรรมศาสตร์
    ประจำปีการศึกษา {{$survey->education_year->year}} โดยข้อมูลที่ได้จะนำมาใช้ในการปรับปรุงการดำเนินงานหลักสูตรและคณะฯ
    เพื่อการดำเนินงานที่เป็นเลิศด้านคุณภาพการศึกษา</p>
  <h5 class="survey-lg-banner">ตอนที่ 1 ข้อมูลทั่วไปของผู้ตอบแบบสอบถาม</h5>
  <div>
    <h5 class="survey-question">เพศ *</h5>
    <p>
      <label>
        <input type="radio" name="1_1" id="" class="with-gap" value="ชาย">
        <span class="black-text survey-choice">ชาย</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_1" id="" class="with-gap" value="หญิง">
        <span class="black-text survey-choice">หญิง</span>
      </label>
    </p>
  </div>
  <div>
    <h5 class="survey-question">ตำแหน่งทางวิชาการ *</h5>
    <p>
      <label>
        <input type="radio" name="1_2" id="" class="with-gap" value="ศาสตราจารย์">
        <span class="black-text survey-choice">ศาสตราจารย์</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_2" id="" class="with-gap" value="รองศาสตราจารย์">
        <span class="black-text survey-choice">รองศาสตราจารย์</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_2" id="" class="with-gap" value="ผู้ช่วยศาสตราจารย์">
        <span class="black-text survey-choice">ผู้ช่วยศาสตราจารย์</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_2" id="" class="with-gap" value="อาจารย์">
        <span class="black-text survey-choice">อาจารย์</span>
      </label>
    </p>
  </div>
  <div>
    <h5 class="survey-question">คุณวุฒิการศึกษา *</h5>
    <p>
      <label>
        <input type="radio" name="1_3" id="" class="with-gap" value="ปริญญาเอก">
        <span class="black-text survey-choice">ปริญญาเอก</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_3" id="" class="with-gap" value="ปริญญาโท">
        <span class="black-text survey-choice">ปริญญาโท</span>
      </label>
    </p>
  </div>
  <div>
    <h5 class="survey-question">ประสบการณ์ในการสอน *</h5>
    <p>
      <label>
        <input type="radio" name="1_4" id="" class="with-gap" value="1-4 ปี">
        <span class="black-text survey-choice">1-4 ปี</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_4" id="" class="with-gap" value="5-7 ปี">
        <span class="black-text survey-choice">5-7 ปี</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_4" id="" class="with-gap" value="มากกว่า 7 ปี">
        <span class="black-text survey-choice">มากกว่า 7 ปี</span>
      </label>
    </p>
  </div>
  <div>
    <h5 class="survey-question">ประสบการณ์ในการบริหารหลักสูตร *</h5>
    <p>
      <label>
        <input type="radio" name="1_5" id="" class="with-gap" value="1-3 ปี">
        <span class="black-text survey-choice">1-3 ปี</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_5" id="" class="with-gap" value="4-5 ปี">
        <span class="black-text survey-choice">4-5 ปี</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_5" id="" class="with-gap" value="มากกว่า 5 ปี">
        <span class="black-text survey-choice">มากกว่า 5 ปี</span>
      </label>
    </p>
  </div>
  <div class="mt-4">
    <a class="waves-effect waves-blue btn-flat" id="btn-1">ถัดไป</a>
  </div>
</div>