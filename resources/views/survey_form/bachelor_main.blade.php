<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Student Survey</title>
    <link rel="stylesheet" href="{{asset('css/materialize.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/survey.css')}}">
</head>

<body>
    <div class="container">
        <div class="row" id="content">
            <div class="col l7 s12 m10 mx-auto p-0" style="float:none;">
                <div class="card">
                    <div class="freebirdAccentBackground"></div>
                    <div class="card-content">
                        <h4 class="survey-header">
                            แบบประเมินความพึงพอใจของนักศึกษาที่มีต่อคุณภาพหลักสูตร{{$survey->branch->name}}</h4>
                        <form action="/survey_form/{{$survey->id}}/save" method="post">
                            {{ csrf_field() }}
                            @include('survey_form.bachelor_part_1')
                            @include('survey_form.bachelor_part_2')
                            @include('survey_form.bachelor_part_3')
                            @include('survey_form.bachelor_part_4')
                            @include('survey_form.bachelor_part_5')
                            @include('survey_form.bachelor_part_6')
                            @include('survey_form.bachelor_part_7')
                            @include('survey_form.bachelor_part_8')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="background"></div>
</body>
<script src="{{asset('js/materialize.min.js')}}"></script>
<script src="{{asset('js/jquery.min.js')}}"></script>
@include('survey_form.bachelor_script')

</html>