<div id="section-1">
  <p class="survey-text-detail">
    แบบประเมินนี้มีวัตถุประสงค์เพื่อสำรวจความพึงพอใจของนักศึกษาที่มีต่อคุณภาพหลักสูตรในสังกัดของคณะวิทยาศาสตร์ประยุกต์และวิศวกรรมศาสตร์
    ประจำปีการศึกษา {{$survey->education_year->year}} โดยข้อมูลที่ได้จะนำมาใช้ในการปรับปรุงการดำเนินงานหลักสูตรและคณะฯ
    เพื่อการดำเนินงานที่เป็นเลิศด้านคุณภาพการศึกษา
  </p>
  <h5 class="survey-lg-banner">ตอนที่ 1 ข้อมูลทั่วไปของผู้ตอบแบบสอบถาม</h5>
  <div>
    <h5 class="survey-question">เพศ *</h5>
    <p>
      <label>
        <input class="with-gap" type="radio" name="1_1" value="ชาย">
        <span class="black-text survey-choice">ชาย</span>
      </label>
    </p>
    <p>
      <label>
        <input class="with-gap" type="radio" name="1_1" value="หญิง">
        <span class="black-text survey-choice">หญิง</span>
      </label>
    </p>
  </div>
  <div>
    <h5 class="survey-question">ชั้นปี *</h5>
    <p>
      <label>
        <input type="radio" name="1_2" class="with-gap" value="ชั้นปีที่ 1">
        <span class="black-text survey-choice">ชั้นปีที่ 1</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_2" class="with-gap" value="ชั้นปีที่ 2">
        <span class="black-text survey-choice">ชั้นปีที่ 2</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_2" class="with-gap" value="ชั้นปีที่ 3">
        <span class="black-text survey-choice">ชั้นปีที่ 3</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_2" class="with-gap" value="ชั้นปีที่ 4">
        <span class="black-text survey-choice">ชั้นปีที่ 4</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_2" class="with-gap" value="ชั้นปีที่ 5 ขึ้นไป">
        <span class="black-text survey-choice">ชั้นปีที่ 5 ขึ้นไป</span>
      </label>
    </p>
  </div>
  <div>
    <h5 class="survey-question">เกรดเฉลี่ยสะสม *</h5>
    <p>
      <label>
        <input type="radio" name="1_3" id="" class="with-gap" value="1.00 - 2.00">
        <span class="black-text survey-choice">1.00 - 2.00</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_3" id="" class="with-gap" value="2.01 - 2.50">
        <span class="black-text survey-choice">2.01 - 2.50</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_3" id="" class="with-gap" value="2.51 - 3.00">
        <span class="black-text survey-choice">2.51 - 3.00</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_3" id="" class="with-gap" value="3.01 - 3.50">
        <span class="black-text survey-choice">3.01 - 3.50</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_3" id="" class="with-gap" value="3.51 - 4.00">
        <span class="black-text survey-choice">3.51 - 4.00</span>
      </label>
    </p>
  </div>
  <div class="mt-4">
    <button type="button" class="waves-effect waves-blue btn-flat black-text mx-1 mt-1" id="btn-1">ถัดไป</button>
  </div>
</div>