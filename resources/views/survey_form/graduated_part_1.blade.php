<div id="section-1">
  <h5 class="survey-lg-banner">ตอนที่ 1 ข้อมูลทั่วไปของผู้ตอบแบบสอบถาม</h5>
  <div>
    <h5 class="survey-question">ปัจจุบันท่านดำรงตาแหน่ง *</h5>
    <p>
      <label>
        <input type="radio" name="1_1" id="" class="with-gap" value="ผู้บริหารระดับสูง">
        <span class="black-text survey-choice">ผู้บริหารระดับสูง</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_1" id="" class="with-gap" value="ผู้บริหารระดับกลาง">
        <span class="black-text survey-choice">ผู้บริหารระดับกลาง</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_1" id="" class="with-gap" value="ผู้บริหารระดับต้น">
        <span class="black-text survey-choice">ผู้บริหารระดับต้น</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_1" id="" class="with-gap" value="อื่นๆ">
        <span class="black-text survey-choice">อื่นๆ</span>
      </label>
    </p>
  </div>
  <div>
    <h5 class="survey-question">วุฒิการศึกษาสูงสุดของท่าน</h5>
    <p>
      <label>
        <input type="radio" name="1_2" id="" class="with-gap" value="ต่ากว่าปริญญาตรี">
        <span class="black-text survey-choice">ต่ากว่าปริญญาตรี</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_2" id="" class="with-gap" value="ปริญญาตรี">
        <span class="black-text survey-choice">ปริญญาตรี</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_2" id="" class="with-gap" value="ปริญญาโท">
        <span class="black-text survey-choice">ปริญญาโท</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_2" id="" class="with-gap" value="ปริญญาเอก">
        <span class="black-text survey-choice">ปริญญาเอก</span>
      </label>
    </p>
  </div>
  <div>
    <h5 class="survey-question">ประเภทหน่วยงานของท่าน</h5>
    <p>
      <label>
        <input type="radio" name="1_3" id="" class="with-gap" value="ส่วนรัฐบาล">
        <span class="black-text survey-choice">ส่วนรัฐบาล</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_3" id="" class="with-gap" value="รัฐวิสาหกิจ">
        <span class="black-text survey-choice">รัฐวิสาหกิจ</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_3" id="" class="with-gap" value="บริษัทเอกชน">
        <span class="black-text survey-choice">บริษัทเอกชน</span>
      </label>
    </p>
    <p>
      <label>
        <input type="radio" name="1_3" id="" class="with-gap" value="อื่นๆ">
        <span class="black-text survey-choice">อื่นๆ</span>
      </label>
    </p>
  </div>
  <div class="mt-4">
    <a class="waves-effect waves-blue btn-flat" id="btn-1">ถัดไป</a>
  </div>
</div>