<div class="hide" id="section-7">
    <h5 class="survey-lg-banner">ด้านการจัดการข้อร้องเรียน</h5>
    <p class="survey-text-detail">โปรดใส่ข้อมูลตามความคิดเห็นของท่านมากที่สุด</p>
    <h5 class="survey-banner">ระดับความพึงพอใจด้านการจัดการข้อร้องเรียน</h5>
    <div>
        <h5 class="survey-question">6.1 ความพึงพอใจโดยรวมด้านการจัดการข้อร้องเรียน</h5>
        <p>
            <label>
                <input type="radio" name="7_1" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="7_1" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="7_1" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="7_1" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="7_1" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h5 class="survey-question">6.2 ความพึงพอใจด้านการจัดการข้อร้องเรียน</h5>
        <h6>(1) มีช่องทางการรับฟังข้อร้องเรียนจากนักศึกษาที่หลากหลาย</h6>
        <p>
            <label>
                <input type="radio" name="7_2_(1)" id="" class="with-gap" value="มากทีสุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="7_2_(1)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="7_2_(1)" id="" class="with-gap" value="ปานกลาง"> 
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="7_2_(1)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="7_2_(1)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(2) มีการนำข้อร้องเรียนไปพิจารณาดำเนินการอย่างเหมาะสม</h6>
        <p>
            <label>
                <input type="radio" name="7_2_(2)" id="" class="with-gap" value="มากทีสุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="7_2_(2)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="7_2_(2)" id="" class="with-gap" value="ปานกลาง"> 
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="7_2_(2)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="7_2_(2)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(3) มีวิธีการติดตาม ประเมินผลความพึงพอใจต่อการจัดการข้อร้องเรียน</h6>
        <p>
            <label>
                <input type="radio" name="7_2_(3)" id="" class="with-gap" value="มากทีสุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="7_2_(3)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="7_2_(3)" id="" class="with-gap" value="ปานกลาง"> 
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="7_2_(3)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="7_2_(3)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h5 class="survey-question">6.3 ท่านมีความ<b class="red-text">ไม่พึงพอใจ</b>ด้านการจัดการข้อร้องเรียนในข้อใด เลือกได้มากกว่า 1 ข้อ</h5>
        <p>
            <label>
          <input type="checkbox" name="7_3[]" id="" class="filled-in" value="มีช่องทางการรับฟังข้อร้องเรียนจากนักศึกษาที่หลากหลาย">
          <span class="black-text survey-choice">มีช่องทางการรับฟังข้อร้องเรียนจากนักศึกษาที่หลากหลาย</span>
        </label>
        </p>
        <p>
            <label>
          <input type="checkbox" name="7_3[]" id="" class="filled-in" value="มีการนำข้อร้องเรียนไปพิจารณาดำเนินการอย่างเหมาะสม">
          <span class="black-text survey-choice">มีการนำข้อร้องเรียนไปพิจารณาดำเนินการอย่างเหมาะสม</span>
        </label>
        </p>
        <p>
            <label>
          <input type="checkbox" name="7_3[]" id="" class="filled-in" value="มีวิธีการติดตาม ประเมินผลความพึงพอใจต่อการจัดการข้อร้องเรียน">
          <span class="black-text survey-choice">มีวิธีการติดตาม ประเมินผลความพึงพอใจต่อการจัดการข้อร้องเรียน</span>
        </label>
        </p>
        <p>
            <label>
          <input type="checkbox" name="7_3[]" id="" class="filled-in" value="ไม่มี">
          <span class="black-text survey-choice">ไม่มี</span>
        </label>
        </p>
    </div>
    <div>
        <h5 class="survey-question">ข้อคิดเห็นหรือข้อเสนอแนะด้านการจัดการข้อร้องเรียน</h5>
        <div class="input-field col s12">
            <textarea id="textarea1" class="materialize-textarea" name="7_4"></textarea>
            <label for="textarea1">คำตอบของคุณ</label>
        </div>
    </div>
    <div class="mt-4">
        <a class="waves-effect wave-orange btn-flat mr-3" id="btn-prev-6">ย้อนกลับ</a>
        <a class="waves-effect wave-orange btn-flat" id="btn-7">ถัดไป</a>
    </div>
</div>