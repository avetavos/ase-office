<div class="hide" id="section-2">
    <h5 class="survey-lg-banner">ความพึงพอใจด้านการบริหารหลักสูตร</h5>
    <p class="survey-text-detail">โปรดเลือกระดับความพึงพอใจตามความคิดเห็นของท่านมากที่สุด</p>
    <h5 class="survey-banner">ระดับความพึงพอใจต่อการบริหารหลักสูตร</h5>
    <div>
        <h6>(1) มีการมอบหมายหน้าที่และความรับผิดชอบของอาจารย์ผู้รับผิดชอบหลักสูตรอย่างชัดเจนและเป็นรายลักษณ์อักษร</h6>
        <p>
            <label>
                <input type="radio" name="2_1_(1)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_1_(1)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_1_(1)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_1_(1)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_1_(1)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(2) มีการกำกับติดตามบทบาทหน้าที่ที่ได้รับมอบหมาย</h6>
        <p>
            <label>
                <input type="radio" name="2_1_(2)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_1_(2)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_1_(2)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_1_(2)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_1_(2)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(3) มีการวิเคราะห์ผลการดำเนินงานและการปรับปรุงการบริหารงานหลักสูตร</h6>
        <p>
            <label>
                <input type="radio" name="2_1_(3)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_1_(3)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_1_(3)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_1_(3)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_1_(3)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(4) มีส่วนร่วมในการประชุมเพื่อวางแผน ติดตาม และทบทวนการดำเนินงานหลักสูตร</h6>
        <p>
            <label>
                <input type="radio" name="2_1_(4)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_1_(4)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_1_(4)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_1_(4)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="2_1_(4)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div class="mt-4">
        <a class="waves-effect waves-blue btn-flat mr-3" id="btn-prev-1">ย้อนกลับ</a>
        <a class="waves-effect waves-blue btn-flat" id="btn-2">ถัดไป</a>
    </div>
</div>