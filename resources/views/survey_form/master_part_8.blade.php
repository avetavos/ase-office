<div class="hide" id="section-8">
  <h5 class="survey-lg-banner">ตอนที่ 3 ความผูกพันธ์ต่อองค์กร</h5>
  <div>
    <h5 class="survey-question">หลังจากที่ท่านได้เข้ามาศึกษาต่อในคณะฯท่านมีความผูกพันธ์ต่อหลักสูตรฯ/คณะฯ ในระดับใด</h5>
    <p>
      <label>
          <input type="radio" name="8_1" id="" class="with-gap" value="มากที่สุด">
          <span class="black-text survey-choice">มากที่สุด</span>
        </label>
    </p>
    <p>
      <label>
          <input type="radio" name="8_1" id="" class="with-gap" value="มาก">
          <span class="black-text survey-choice">มาก</span>
        </label>
    </p>
    <p>
      <label>
          <input type="radio" name="8_1" id="" class="with-gap" value="ปานกลาง">
          <span class="black-text survey-choice">ปานกลาง</span>
        </label>
    </p>
    <p>
      <label>
          <input type="radio" name="8_1" id="" class="with-gap" value="น้อย">
          <span class="black-text survey-choice">น้อย</span>
        </label>
    </p>
    <p>
      <label>
          <input type="radio" name="8_1" id="" class="with-gap" value="น้อยที่สุด">
          <span class="black-text survey-choice">น้อยที่สุด</span>
        </label>
    </p>
  </div>
  <div class="mt-4">
    <a class="waves-effect wave-orange btn-flat mr-3" id="btn-prev-7">ย้อนกลับ</a>
    <a class="waves-effect wave-orange btn-flat" id="btn-8">ส่ง</a>
  </div>
</div>