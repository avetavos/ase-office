<div class="hide" id="section-3">
    <h5 class="survey-lg-banner">แฟ้มระเบียนประวัติ</h5>
    <p class="survey-text-detail">โปรดเลือกระดับความพึงพอใจตามความคิดเห็นของท่านมากที่สุด</p>
    <h5 class="survey-question">ระดับความพึงพอใจต่อแฟ้มป์ระเบียนประวัติ</h5>
    <div>
        <h6>(1) ความซับซ้อนของแฟ้มระเบียนประวัติ</h6>
        <p>
            <label>
                <input type="radio" name="3_1_(1)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(1)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(1)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(1)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(1)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h6>(2) ประโยชน์ในการมีแฟ้มระเบียนประวัติ</h6>
        <p>
            <label>
                <input type="radio" name="3_1_(2)" id="" class="with-gap" value="มากที่สุด">
                <span class="black-text survey-choice">มากที่สุด</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(2)" id="" class="with-gap" value="มาก">
                <span class="black-text survey-choice">มาก</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(2)" id="" class="with-gap" value="ปานกลาง">
                <span class="black-text survey-choice">ปานกลาง</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(2)" id="" class="with-gap" value="น้อย">
                <span class="black-text survey-choice">น้อย</span>
            </label>
        </p>
        <p>
            <label>
                <input type="radio" name="3_1_(2)" id="" class="with-gap" value="น้อยที่สุด">
                <span class="black-text survey-choice">น้อยที่สุด</span>
            </label>
        </p>
    </div>
    <div>
        <h5 class="survey-question">ข้อเสนอแนะต่อระบบอาจารย์ที่ปรึกษา</h5>
        <div class="input-field col s12">
            <textarea id="textarea1" class="materialize-textarea" name="3_2"></textarea>
            <label for="textarea1">คำตอบของคุณ</label>
          </div>
      </div>
      <div class="mt-4">
        <a class="waves-effect waves-blue btn-flat mr-3" id="btn-prev-2">ย้อนกลับ</a>
        <a class="waves-effect waves-blue btn-flat" id="btn-3">ถัดไป</a>
      </div>
  </div>
  