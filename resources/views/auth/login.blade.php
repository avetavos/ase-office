<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>ASE Office</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand ml-1 text-warning" href="{{ url('/') }}">
                <span class="text-kku">ASE</span> Office
            </a>
        </div>
    </nav>
    <!-- sidebar-wrapper  -->
    <main class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto mt-5">
                <div class="card shadow">
                    <div class="card-header bg-dark d-none d-md-block">
                        <h1 class="text-center text-warning">
                            <span class="text-kku m-0">ASE</span> Office
                        </h1>
                    </div>
                    <div class="card-body py-5">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="username"
                                    class="col-md-4 col-form-label text-md-right">{{ __('ชื่อผู้ใช้') }}</label>

                                <div class="col-md-6">
                                    <input id="username" type="text"
                                        class="form-control @error('username') is-invalid @enderror" name="username"
                                        value="{{ old('username') }}" required autocomplete="email" autofocus>

                                    @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password"
                                    class="col-md-4 col-form-label text-md-right">{{ __('รหัสผ่าน') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password"
                                        class="form-control @error('password') is-invalid @enderror" name="password"
                                        required autocomplete="current-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 mx-auto text-center">
                                    <button type="submit" class="btn btn-primary px-5">
                                        {{ __('ลงชื่อเข้าใช้') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- page-content" -->
    <!-- page-wrapper -->
    <script src="{{ asset('js/app.js') }}"></script>
    @yield("custom_script")
</body>

</html>