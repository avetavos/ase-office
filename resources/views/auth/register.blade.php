@extends('layouts.main')

@section('content')
<div class="row p-3">
    <div class="col-12">
        <h3>สร้างบัญชีผู้ใช้</h3>
        <hr>
        @include('inc.alert')
    </div>
    <div class="col-lg-10 col-xl-8 mb-3 mx-auto">
        <div class="card p-md-5 p-3">
            {!! Form::open(['action' => 'RegisterController@register']) !!}
            <div class="form-row">
                <div class="form-group col-lg-8 mx-auto">
                    {{ Form::label('name', 'ชื่อ นามสกุล')}}
                    {{ Form::input('text', 'name', null, ['class' => 'form-control'])}}
                </div>
                <div class="form-group col-lg-8 mx-auto">
                    {{ Form::label('username', 'ชื่อผู้ใช้')}}
                    {{ Form::input('text', 'username', null, ['class' => 'form-control'])}}
                </div>
                <div class="form-group col-lg-8 mx-auto">
                    {{ Form::label('role', 'สิทธิ์การเข้าถึง')}}
                    {{ Form::select('role', ['0' => 'ผู้ใช้ทั่วไป', '1' => 'ผู้ดูแลระบบ'], null, ['class' => 'form-control']) }}
                </div>
                <div class="form-group col-lg-8 mx-auto">
                    <p class="text-danger mb-0"><b>* รหัสผ่านเริ่มต้นของผู้ใช้คือ "ase123456"
                            หลังจากเข้าสู่ระบบแล้วผู้ใช้สามารถเปลี่ยนรหัสผ่านได้ที่เมนู "ตั้งค่าบัญชี"</b></p>
                </div>
                <div class="col-lg-8 mx-auto text-center mt-3">
                    {{ Form::input('submit', null, 'สร้างบัญชี', ['class' => 'btn btn-primary px-5'])}}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection