<tr class="even">
  <th colspan="8" class="p-2">ประเด็นยุทธศาสตร์ที่ 11 เสริมสร้างความร่วมมือเพื่อการพัฒนา</th>
</tr>
<tr class="even">
  <th class="text-center p-1">ตัวชี้วัด</th>
  <td class="text-center p-1">{{ count($cooperate['all_process']['indicator']) }}</td>
  <td class="text-center p-1">{{ count($cooperate['processed']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $cooperate['processed']['indicator_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($cooperate['processing']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $cooperate['processing']['indicator_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($cooperate['non_process']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $cooperate['non_process']['indicator_percent'] }}
  </td>
</tr>
<tr class="even">
  <th class="text-center p-1">กลยุทธ์</th>
  <td class="text-center p-1">{{ count($cooperate['all_process']['strategy']) }}</td>
  <td class="text-center p-1">{{ count($cooperate['processed']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $cooperate['processed']['strategy_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($cooperate['processing']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $cooperate['processing']['strategy_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($cooperate['non_process']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $cooperate['non_process']['strategy_percent'] }}
  </td>
</tr>
<tr class="even">
  <th class="text-center p-1">โครงการ</th>
  <td class="text-center p-1">{{ $cooperate['all_process']['project']}}</td>
  <td class="text-center p-1">{{ $cooperate['processed']['project']}}</td>
  <td class="text-center p-1">
    {{ $cooperate['processed']['project_percent'] }}
  </td>
  <td class="text-center p-1">{{ $cooperate['processing']['project']}}</td>
  <td class="text-center p-1">
    {{ $cooperate['processing']['project_percent'] }}
  </td>
  <td class="text-center p-1">{{ $cooperate['non_process']['project']}}</td>
  <td class="text-center p-1">
    {{ $cooperate['non_process']['project_percent'] }}
  </td>
</tr>