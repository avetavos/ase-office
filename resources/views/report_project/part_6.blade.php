<tr class="even">
  <th colspan="8" class="p-2">ประเด็นยุทธศาสตร์ที่ 5 ปรับเปลี่ยนการบริหารจัดการองค์กร</th>
</tr>
<tr class="even">
  <th class="text-center p-1">ตัวชี้วัด</th>
  <td class="text-center p-1">{{ count($organization['all_process']['indicator']) }}</td>
  <td class="text-center p-1">{{ count($organization['processed']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $organization['processed']['indicator_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($organization['processing']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $organization['processing']['indicator_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($organization['non_process']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $organization['non_process']['indicator_percent'] }}
  </td>
</tr>
<tr class="even">
  <th class="text-center p-1">กลยุทธ์</th>
  <td class="text-center p-1">{{ count($organization['all_process']['strategy']) }}</td>
  <td class="text-center p-1">{{ count($organization['processed']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $organization['processed']['strategy_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($organization['processing']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $organization['processing']['strategy_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($organization['non_process']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $organization['non_process']['strategy_percent'] }}
  </td>
</tr>
<tr class="even">
  <th class="text-center p-1">โครงการ</th>
  <td class="text-center p-1">{{ $organization['all_process']['project']}}</td>
  <td class="text-center p-1">{{ $organization['processed']['project']}}</td>
  <td class="text-center p-1">
    {{ $organization['processed']['project_percent'] }}
  </td>
  <td class="text-center p-1">{{ $organization['processing']['project']}}</td>
  <td class="text-center p-1">
    {{ $organization['processing']['project_percent'] }}
  </td>
  <td class="text-center p-1">{{ $organization['non_process']['project']}}</td>
  <td class="text-center p-1">
    {{ $organization['non_process']['project_percent'] }}
  </td>
</tr>