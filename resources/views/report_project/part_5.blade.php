<tr>
  <th colspan="8" class="p-2">ประเด็นยุทธศาสตร์ที่ 4 ปรับเปลี่ยนการบริการวิชาการ</th>
</tr>
<tr>
  <th class="text-center p-1">ตัวชี้วัด</th>
  <td class="text-center p-1">{{ count($academic['all_process']['indicator']) }}</td>
  <td class="text-center p-1">{{ count($academic['processed']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $academic['processed']['indicator_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($academic['processing']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $academic['processing']['indicator_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($academic['non_process']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $academic['non_process']['indicator_percent'] }}
  </td>
</tr>
<tr>
  <th class="text-center p-1">กลยุทธ์</th>
  <td class="text-center p-1">{{ count($academic['all_process']['strategy']) }}</td>
  <td class="text-center p-1">{{ count($academic['processed']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $academic['processed']['strategy_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($academic['processing']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $academic['processing']['strategy_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($academic['non_process']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $academic['non_process']['strategy_percent'] }}
  </td>
</tr>
<tr>
  <th class="text-center p-1">โครงการ</th>
  <td class="text-center p-1">{{ $academic['all_process']['project']}}</td>
  <td class="text-center p-1">{{ $academic['processed']['project']}}</td>
  <td class="text-center p-1">
    {{ $academic['processed']['project_percent'] }}
  </td>
  <td class="text-center p-1">{{ $academic['processing']['project']}}</td>
  <td class="text-center p-1">
    {{ $academic['processing']['project_percent'] }}
  </td>
  <td class="text-center p-1">{{ $academic['non_process']['project']}}</td>
  <td class="text-center p-1">
    {{ $academic['non_process']['project_percent'] }}
  </td>
</tr>