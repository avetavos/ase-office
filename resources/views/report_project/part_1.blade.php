<tr>
  <th colspan="8" class="p-2">รวมทุกประเด็นยุทธศาสตร์</th>
</tr>
<tr>
  <th class="text-center p-1">ตัวชี้วัด</th>
  <td class="text-center p-1">{{ count($all['all_process']['indicator']) }}</td>
  <td class="text-center p-1">{{ count($all['processed']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $all['processed']['indicator_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($all['processing']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $all['processing']['indicator_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($all['non_process']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $all['non_process']['indicator_percent'] }}
  </td>
</tr>
<tr>
  <th class="text-center p-1">กลยุทธ์</th>
  <td class="text-center p-1">{{ count($all['all_process']['strategy']) }}</td>
  <td class="text-center p-1">{{ count($all['processed']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $all['processed']['strategy_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($all['processing']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $all['processing']['strategy_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($all['non_process']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $all['non_process']['strategy_percent'] }}
  </td>
</tr>
<tr>
  <th class="text-center p-1">โครงการ</th>
  <td class="text-center p-1">{{ $all['all_process']['project']}}</td>
  <td class="text-center p-1">{{ $all['processed']['project']}}</td>
  <td class="text-center p-1">
    {{ $all['processed']['project_percent'] }}
  </td>
  <td class="text-center p-1">{{ $all['processing']['project']}}</td>
  <td class="text-center p-1">
    {{ $all['processing']['project_percent'] }}
  </td>
  <td class="text-center p-1">{{ $all['non_process']['project']}}</td>
  <td class="text-center p-1">
    {{ $all['non_process']['project_percent'] }}
  </td>
</tr>