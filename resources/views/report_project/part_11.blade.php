<tr>
  <th colspan="8" class="p-2">ประเด็นยุทธศาสตร์ที่ 10 การบริหารโดยใช้หลักธรรมาภิบาล</th>
</tr>
<tr>
  <th class="text-center p-1">ตัวชี้วัด</th>
  <td class="text-center p-1">{{ count($governance['all_process']['indicator']) }}</td>
  <td class="text-center p-1">{{ count($governance['processed']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $governance['processed']['indicator_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($governance['processing']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $governance['processing']['indicator_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($governance['non_process']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $governance['non_process']['indicator_percent'] }}
  </td>
</tr>
<tr>
  <th class="text-center p-1">กลยุทธ์</th>
  <td class="text-center p-1">{{ count($governance['all_process']['strategy']) }}</td>
  <td class="text-center p-1">{{ count($governance['processed']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $governance['processed']['strategy_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($governance['processing']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $governance['processing']['strategy_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($governance['non_process']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $governance['non_process']['strategy_percent'] }}
  </td>
</tr>
<tr>
  <th class="text-center p-1">โครงการ</th>
  <td class="text-center p-1">{{ $governance['all_process']['project']}}</td>
  <td class="text-center p-1">{{ $governance['processed']['project']}}</td>
  <td class="text-center p-1">
    {{ $governance['processed']['project_percent'] }}
  </td>
  <td class="text-center p-1">{{ $governance['processing']['project']}}</td>
  <td class="text-center p-1">
    {{ $governance['processing']['project_percent'] }}
  </td>
  <td class="text-center p-1">{{ $governance['non_process']['project']}}</td>
  <td class="text-center p-1">
    {{ $governance['non_process']['project_percent'] }}
  </td>
</tr>