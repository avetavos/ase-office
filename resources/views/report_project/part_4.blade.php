<tr class="even">
  <th colspan="8" class="p-2">ประเด็นยุทธศาสตร์ที่ 3 ปรับเปลี่ยนการบริหารทรัพยากรบุคคล</th>
</tr>
<tr class="even">
  <th class="text-center p-1">ตัวชี้วัด</th>
  <td class="text-center p-1">{{ count($humanresource['all_process']['indicator']) }}</td>
  <td class="text-center p-1">{{ count($humanresource['processed']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $humanresource['processed']['indicator_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($humanresource['processing']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $humanresource['processing']['indicator_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($humanresource['non_process']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $humanresource['non_process']['indicator_percent'] }}
  </td>
</tr>
<tr class="even">
  <th class="text-center p-1">กลยุทธ์</th>
  <td class="text-center p-1">{{ count($humanresource['all_process']['strategy']) }}</td>
  <td class="text-center p-1">{{ count($humanresource['processed']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $humanresource['processed']['strategy_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($humanresource['processing']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $humanresource['processing']['strategy_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($humanresource['non_process']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $humanresource['non_process']['strategy_percent'] }}
  </td>
</tr>
<tr class="even">
  <th class="text-center p-1">โครงการ</th>
  <td class="text-center p-1">{{ $humanresource['all_process']['project']}}</td>
  <td class="text-center p-1">{{ $humanresource['processed']['project']}}</td>
  <td class="text-center p-1">
    {{ $humanresource['processed']['project_percent'] }}
  </td>
  <td class="text-center p-1">{{ $humanresource['processing']['project']}}</td>
  <td class="text-center p-1">
    {{ $humanresource['processing']['project_percent'] }}
  </td>
  <td class="text-center p-1">{{ $humanresource['non_process']['project']}}</td>
  <td class="text-center p-1">
    {{ $humanresource['non_process']['project_percent'] }}
  </td>
</tr>