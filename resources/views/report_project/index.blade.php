@extends('layouts.main')

@section('content')
<div class="row p-3">
  <div class="col-12">
    <div class="row">
      <div class="col-lg-6">
        <h3 class="mb-md-0 mb-3">สรุปข้อมูลผลการดำเนินการตามวิสัยทัศน์ ตัวชี้วัด กลยุทธ์ โครงการ</h3>
      </div>
      <div class="col-lg-6 text-right">
        {!! Form::open(['id' => 'project_years_form', 'method' => 'get']) !!} {{ Form::label('year', 'ปีการศึกษา', ['class' =>
                      'sr-only']) }}
        <div class="input-group">
          <div class="input-group-prepend">
            <div class="input-group-text">ปีโครงการ</div>
          </div>
          {{ Form::select('year', $years, $current_year->id, ['class' => 'form-control', 'onchange' => 'this.form.submit()']) }}
        </div>
        {!! Form::close() !!}
      </div>
    </div>
    <hr>
    @include('inc.alert')
  </div>
  <div class="col-lg-10 mx-auto">
    <h4 class="text-center">สรุปข้อมูลผลการดำเนินการตามวิสัยทัศน์ ตัวชี้วัด กลยุทธ์ โครงการ</h4>
    <h4 class="text-center">ตามแผนปฏิบัติราชการ ประจำปีงบประมาณ พ.ศ {{$current_year->education_year->year}}
      คณะวิทยาศาสตร์ประยุกต์และวิศวกรรมศาสตร์
      วิทยาเขตหนองคาย</h4>
    <div class="table-responsive mt-4">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th rowspan="3" class="text-center p-2">ประเด็นยุทธศาสตร์</th>
            <th colspan="7" class="text-center p-1">ผลการดำเนินการ (ระบุจำนวน)</th>
          </tr>
          <tr>
            <th rowspan="2" class="text-center p-1">จำนวน</th>
            <th colspan="2" class="text-center p-1" style="width:27%">ดำเนินการแล้วเป็นไปตามแผนและเป้าหมาย</th>
            <th colspan="2" class="text-center p-1" style="width:27%">เริ่มดำเนินการแล้วแต่ยังไม่บรรลุเป้าหมาย
            </th>
            <th colspan="2" class="text-center p-1" style="width:27%">ยังไม่ได้ดำเนินการ</th>
          </tr>
          <tr>
            <th class="text-center p-1">จำนวน</th>
            <th class="text-center p-1">ร้อยละ</th>
            <th class="text-center p-1">จำนวน</th>
            <th class="text-center p-1">ร้อยละ</th>
            <th class="text-center p-1">จำนวน</th>
            <th class="text-center p-1">ร้อยละ</th>
          </tr>
        </thead>
        <tbody>
          @include('report_project.part_1')
          @include('report_project.part_2')
          @include('report_project.part_3')
          @include('report_project.part_4')
          @include('report_project.part_5')
          @include('report_project.part_6')
          @include('report_project.part_7')
          @include('report_project.part_8')
          @include('report_project.part_9')
          @include('report_project.part_10')
          @include('report_project.part_11')
          @include('report_project.part_12')
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection

@section('custom_style')
<style>
  .even {
    background-color: #F2F2F2;
  }
</style>
@endsection