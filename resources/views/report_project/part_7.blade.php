<tr>
  <th colspan="8" class="p-2">ประเด็นยุทธศาสตร์ที่ 6 สร้างมหาวิทยาลัยขอนแก่นให้เป็นที่น่าทำงาน</th>
</tr>
<tr>
  <th class="text-center p-1">ตัวชี้วัด</th>
  <td class="text-center p-1">{{ count($workplace['all_process']['indicator']) }}</td>
  <td class="text-center p-1">{{ count($workplace['processed']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $workplace['processed']['indicator_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($workplace['processing']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $workplace['processing']['indicator_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($workplace['non_process']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $workplace['non_process']['indicator_percent'] }}
  </td>
</tr>
<tr>
  <th class="text-center p-1">กลยุทธ์</th>
  <td class="text-center p-1">{{ count($workplace['all_process']['strategy']) }}</td>
  <td class="text-center p-1">{{ count($workplace['processed']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $workplace['processed']['strategy_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($workplace['processing']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $workplace['processing']['strategy_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($workplace['non_process']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $workplace['non_process']['strategy_percent'] }}
  </td>
</tr>
<tr>
  <th class="text-center p-1">โครงการ</th>
  <td class="text-center p-1">{{ $workplace['all_process']['project']}}</td>
  <td class="text-center p-1">{{ $workplace['processed']['project']}}</td>
  <td class="text-center p-1">
    {{ $workplace['processed']['project_percent'] }}
  </td>
  <td class="text-center p-1">{{ $workplace['processing']['project']}}</td>
  <td class="text-center p-1">
    {{ $workplace['processing']['project_percent'] }}
  </td>
  <td class="text-center p-1">{{ $workplace['non_process']['project']}}</td>
  <td class="text-center p-1">
    {{ $workplace['non_process']['project_percent'] }}
  </td>
</tr>