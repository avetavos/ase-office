<tr>
  <th colspan="8" class="p-2">ประเด็นยุทธศาสตร์ที่ 8 ปรับเปลี่ยนองค์กรให้ก้าวสู่ยุคดิจิทัล</th>
</tr>
<tr>
  <th class="text-center p-1">ตัวชี้วัด</th>
  <td class="text-center p-1">{{ count($digital['all_process']['indicator']) }}</td>
  <td class="text-center p-1">{{ count($digital['processed']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $digital['processed']['indicator_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($digital['processing']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $digital['processing']['indicator_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($digital['non_process']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $digital['non_process']['indicator_percent'] }}
  </td>
</tr>
<tr>
  <th class="text-center p-1">กลยุทธ์</th>
  <td class="text-center p-1">{{ count($digital['all_process']['strategy']) }}</td>
  <td class="text-center p-1">{{ count($digital['processed']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $digital['processed']['strategy_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($digital['processing']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $digital['processing']['strategy_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($digital['non_process']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $digital['non_process']['strategy_percent'] }}
  </td>
</tr>
<tr>
  <th class="text-center p-1">โครงการ</th>
  <td class="text-center p-1">{{ $digital['all_process']['project']}}</td>
  <td class="text-center p-1">{{ $digital['processed']['project']}}</td>
  <td class="text-center p-1">
    {{ $digital['processed']['project_percent'] }}
  </td>
  <td class="text-center p-1">{{ $digital['processing']['project']}}</td>
  <td class="text-center p-1">
    {{ $digital['processing']['project_percent'] }}
  </td>
  <td class="text-center p-1">{{ $digital['non_process']['project']}}</td>
  <td class="text-center p-1">
    {{ $digital['non_process']['project_percent'] }}
  </td>
</tr>