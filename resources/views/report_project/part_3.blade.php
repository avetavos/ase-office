<tr>
  <th colspan="8" class="p-2">ประเด็นยุทธศาสตร์ที่ 2 ปรับเปลี่ยนการทำงานวิจัย</th>
</tr>
<tr>
  <th class="text-center p-1">ตัวชี้วัด</th>
  <td class="text-center p-1">{{ count($research['all_process']['indicator']) }}</td>
  <td class="text-center p-1">{{ count($research['processed']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $research['processed']['indicator_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($research['processing']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $research['processing']['indicator_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($research['non_process']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $research['non_process']['indicator_percent'] }}
  </td>
</tr>
<tr>
  <th class="text-center p-1">กลยุทธ์</th>
  <td class="text-center p-1">{{ count($research['all_process']['strategy']) }}</td>
  <td class="text-center p-1">{{ count($research['processed']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $research['processed']['strategy_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($research['processing']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $research['processing']['strategy_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($research['non_process']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $research['non_process']['strategy_percent'] }}
  </td>
</tr>
<tr>
  <th class="text-center p-1">โครงการ</th>
  <td class="text-center p-1">{{ $research['all_process']['project']}}</td>
  <td class="text-center p-1">{{ $research['processed']['project']}}</td>
  <td class="text-center p-1">
    {{ $research['processed']['project_percent'] }}
  </td>
  <td class="text-center p-1">{{ $research['processing']['project']}}</td>
  <td class="text-center p-1">
    {{ $research['processing']['project_percent'] }}
  </td>
  <td class="text-center p-1">{{ $research['non_process']['project']}}</td>
  <td class="text-center p-1">
    {{ $research['non_process']['project_percent'] }}
  </td>
</tr>