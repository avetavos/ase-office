<tr class="even">
  <th colspan="8" class="p-2">ประเด็นยุทธศาสตร์ 1 ปรับเปลี่ยนการจัดการศึกษา</th>
</tr>
<tr class="even">
  <th class="text-center p-1">ตัวชี้วัด</th>
  <td class="text-center p-1">{{ count($education['all_process']['indicator']) }}</td>
  <td class="text-center p-1">{{ count($education['processed']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $education['processed']['indicator_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($education['processing']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $education['processing']['indicator_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($education['non_process']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $education['non_process']['indicator_percent'] }}
  </td>
</tr>
<tr class="even">
  <th class="text-center p-1">กลยุทธ์</th>
  <td class="text-center p-1">{{ count($education['all_process']['strategy']) }}</td>
  <td class="text-center p-1">{{ count($education['processed']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $education['processed']['strategy_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($education['processing']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $education['processing']['strategy_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($education['non_process']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $education['non_process']['strategy_percent'] }}
  </td>
</tr>
<tr class="even">
  <th class="text-center p-1">โครงการ</th>
  <td class="text-center p-1">{{ $education['all_process']['project']}}</td>
  <td class="text-center p-1">{{ $education['processed']['project']}}</td>
  <td class="text-center p-1">
    {{ $education['processed']['project_percent'] }}
  </td>
  <td class="text-center p-1">{{ $education['processing']['project']}}</td>
  <td class="text-center p-1">
    {{ $education['processing']['project_percent'] }}
  </td>
  <td class="text-center p-1">{{ $education['non_process']['project']}}</td>
  <td class="text-center p-1">
    {{ $education['non_process']['project_percent'] }}
  </td>
</tr>