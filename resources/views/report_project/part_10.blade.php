<tr class="even">
  <th colspan="8" class="p-2">ประเด็นยุทธศาสตร์ที่ 9 การนำมหาลัยสู่ความเป็นนานาชาติ</th>
</tr>
<tr class="even">
  <th class="text-center p-1">ตัวชี้วัด</th>
  <td class="text-center p-1">{{ count($international['all_process']['indicator']) }}</td>
  <td class="text-center p-1">{{ count($international['processed']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $international['processed']['indicator_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($international['processing']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $international['processing']['indicator_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($international['non_process']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $international['non_process']['indicator_percent'] }}
  </td>
</tr>
<tr class="even">
  <th class="text-center p-1">กลยุทธ์</th>
  <td class="text-center p-1">{{ count($international['all_process']['strategy']) }}</td>
  <td class="text-center p-1">{{ count($international['processed']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $international['processed']['strategy_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($international['processing']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $international['processing']['strategy_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($international['non_process']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $international['non_process']['strategy_percent'] }}
  </td>
</tr>
<tr class="even">
  <th class="text-center p-1">โครงการ</th>
  <td class="text-center p-1">{{ $international['all_process']['project']}}</td>
  <td class="text-center p-1">{{ $international['processed']['project']}}</td>
  <td class="text-center p-1">
    {{ $international['processed']['project_percent'] }}
  </td>
  <td class="text-center p-1">{{ $international['processing']['project']}}</td>
  <td class="text-center p-1">
    {{ $international['processing']['project_percent'] }}
  </td>
  <td class="text-center p-1">{{ $international['non_process']['project']}}</td>
  <td class="text-center p-1">
    {{ $international['non_process']['project_percent'] }}
  </td>
</tr>