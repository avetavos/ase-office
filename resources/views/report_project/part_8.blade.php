<tr class="even">
  <th colspan="8" class="p-2">ประเด็นยุทธศาสตร์ที่ 7 สร้างมหาวิทยาลัยให้เป็นที่น่าอยู่</th>
</tr>
<tr class="even">
  <th class="text-center p-1">ตัวชี้วัด</th>
  <td class="text-center p-1">{{ count($greencampus['all_process']['indicator']) }}</td>
  <td class="text-center p-1">{{ count($greencampus['processed']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $greencampus['processed']['indicator_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($greencampus['processing']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $greencampus['processing']['indicator_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($greencampus['non_process']['indicator']) }}</td>
  <td class="text-center p-1">
    {{ $greencampus['non_process']['indicator_percent'] }}
  </td>
</tr>
<tr class="even">
  <th class="text-center p-1">กลยุทธ์</th>
  <td class="text-center p-1">{{ count($greencampus['all_process']['strategy']) }}</td>
  <td class="text-center p-1">{{ count($greencampus['processed']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $greencampus['processed']['strategy_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($greencampus['processing']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $greencampus['processing']['strategy_percent'] }}
  </td>
  <td class="text-center p-1">{{ count($greencampus['non_process']['strategy']) }}</td>
  <td class="text-center p-1">
    {{ $greencampus['non_process']['strategy_percent'] }}
  </td>
</tr>
<tr class="even">
  <th class="text-center p-1">โครงการ</th>
  <td class="text-center p-1">{{ $greencampus['all_process']['project']}}</td>
  <td class="text-center p-1">{{ $greencampus['processed']['project']}}</td>
  <td class="text-center p-1">
    {{ $greencampus['processed']['project_percent'] }}
  </td>
  <td class="text-center p-1">{{ $greencampus['processing']['project']}}</td>
  <td class="text-center p-1">
    {{ $greencampus['processing']['project_percent'] }}
  </td>
  <td class="text-center p-1">{{ $greencampus['non_process']['project']}}</td>
  <td class="text-center p-1">
    {{ $greencampus['non_process']['project_percent'] }}
  </td>
</tr>