@extends('layouts.main')
@section('content')
<div class="row p-3">
  <div class="col-12">
    <h3>หน้าหลัก</h3>
    <hr />
    @include('inc.alert')
  </div>
  @if ($current_year)
  <div class="col-12 mb-3">
    <div class="card shadow">
      <div class="card-header bg-kku text-light">
        <h5 class="card-title mb-0">
          วิชาในความรับผิดชอบในปีการศึกษา {{$current_year->year}}
        </h5>
      </div>
      <div class="card-body">
        @if (count($subjects) > 0)
        <div class="table-responsive">
          <table class="table table-striped table-bordered">
            <thead>
              <tr>
                <th scope="col" class="text-center">ลำดับ</th>
                <th scope="col" class="text-center">
                  รหัสวิชา
                </th>
                <th scope="col">ชื่อวิชา</th>
                <th scope="col" class="text-center">มคอ. 3</th>
                <th scope="col" class="text-center">มคอ. 5</th>
                <th scope="col" class="text-center">ผลทวนสอบ</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($subjects as $key=>$subject)
              <tr>
                <th scope="row" class="text-center">
                  {{ $key+1}}
                </th>
                <th scope="row" class="text-center">
                  {{ $subject["code"] }}
                </th>
                <td>{{ $subject["name"] }}</td>
                <td class="text-center">
                  @if ($subject['tqf3'])
                  <div class="d-md-inline">
                    <a role="button" class="btn btn-success"
                      href="{{URL::to('/storage/tqf_uploads/'.$subject['tqf3'])}}" target="_blank" download>
                      <i class="fas fa-file-download"></i>
                    </a>
                  </div>
                  @endif {!! Form::open(['action' => ['TqfController@updateTqf3', $subject['id']],
                  'method' => 'put', 'id' => 'form_3_'.$subject['code'],
                  'enctype' => 'multipart/form-data', 'class' => 'd-md-inline']) !!}
                  <label for="tqf3_document_{{$subject['code']}}" class="btn btn-primary mb-0"><i
                      class="fas fa-file-upload"></i></label>{{
                                    Form::input('file', 'tqf3_document', null, ['class' => 'd-none', 'onchange' => 'submitParent(`form_3_'.$subject['code'].'`,`tqf3_document_'.$subject['code'].'`)',
                                    'id' => 'tqf3_document_'.$subject['code']])}}
                  {{ Form::input('hidden', 'path_url', '/', ['class' => 'old-path'])}}
                  {!! Form::close() !!}
                </td>
                <td class="text-center">
                  @if ($subject['tqf5'])
                  <div class="d-md-inline">
                    <a role="button" class="btn btn-success"
                      href="{{URL::to('/storage/tqf_uploads/'.$subject['tqf5'])}}" target="_blank" download>
                      <i class="fas fa-file-download"></i>
                    </a>
                  </div>
                  @endif {!! Form::open(['action' => ['TqfController@updateTqf5', $subject['id']],
                  'method' => 'put', 'id' => 'form_5_'.$subject['code'],
                  'enctype' => 'multipart/form-data', 'class' => 'd-md-inline']) !!}
                  <label for="tqf5_document_{{$subject['code']}}" class="btn btn-primary mb-0"><i
                      class="fas fa-file-upload"></i></label>{{
                                    Form::input('file', 'tqf5_document', null, ['class' => 'd-none', 'onchange' => 'submitParent(`form_5_'.$subject['code'].'`,`tqf5_document_'.$subject['code'].'`)',
                                    'id' => 'tqf5_document_'.$subject['code']])}}
                  {{ Form::input('hidden', 'path_url', '/', ['class' => 'old-path'])}}
                  {!! Form::close() !!}
                </td>
                <td class="text-center">
                  @if ($subject['exam'])
                  <div class="d-md-inline">
                    <a role="button" class="btn btn-success"
                      href="{{URL::to('/storage/tqf_uploads/'.$subject['exam'])}}" target="_blank" download>
                      <i class="fas fa-file-download"></i>
                    </a>
                  </div>
                  @endif {!! Form::open(['action' => ['TqfController@updateExam', $subject['id']],
                  'method' => 'put', 'id' => 'form_exam_'.$subject['code'],
                  'enctype' => 'multipart/form-data', 'class' => 'd-md-inline']) !!}
                  <label for="exam_document_{{$subject['code']}}" class="btn btn-primary mb-0"><i
                      class="fas fa-file-upload"></i></label>{{
                                      Form::input('file', 'exam_document', null, ['class' => 'd-none', 'onchange' => 'submitParent(`form_exam_'.$subject['code'].'`,`exam_document_'.$subject['code'].'`)',
                                      'id' => 'exam_document_'.$subject['code']])}}
                  {{ Form::input('hidden', 'path_url', '/', ['class' => 'old-path'])}}
                  {!! Form::close() !!}
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        @else
        <div class="row">
          <div class="col-12 text-center">
            <h4>ยังไม่มีวิชาที่ต้องรับผิดชอบ</h4>
          </div>
        </div>
        @endif
      </div>
    </div>
  </div>
  <div class="col-12 mb-3">
    <div class="card shadow">
      <div class="card-header bg-kku text-light">
        <h5 class="card-title mb-0">
          โครงการในความรับผิดชอบในปีการศึกษา {{$current_year->year}}
        </h5>
      </div>
      <div class="card-body">
        @if (count($project_users) > 0)
        <div class="table-responsive">
          <table class="table table-striped table-bordered">
            <thead>
              <tr>
                <th scope="col" class="text-center">
                  ลำดับ
                </th>
                <th scope="col" class="text-center">ชื่อโครงการ</th>
                <th scope="col" class="text-center">ตัวชี้วัด</th>
                <th scope="col" class="text-center">กลยุทธ์</th>
                <th scope="col" class="text-center">แผนการดำเนินงาน</th>
                <th scope="col" class="text-center">ผลการดำเนินงาน</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($project_users as $key=>$value)
              <tr>
                <th scope="row" class="text-center">
                  {{$key + 1}}
                </th>
                <td>{{ $value->name }}</td>
                <td>
                  <ul class="mb-0 pl-3">
                    @foreach ($value->indicator as $item)
                    <li>{{$item}}</li>
                    @endforeach
                  </ul>
                </td>
                <td>{{ $value["strategy"] }}</td>
                <td class="text-center">
                  @if ($value->processing_file)
                  <div class="d-md-inline">
                    <a role="button" class="btn btn-success"
                      href="{{URL::to('/storage/processing_file_uploads/'.$value['processing_file'])}}" target="_blank"
                      download>
                      <i class="fas fa-file-download"></i>
                    </a>
                  </div>
                  @endif
                  {!! Form::open(
                  ['action' => ['ProjectController@uploadProcessingFile',$value->id],
                  'method' => 'put', 'id' => 'form_3_'.$value->id,
                  'enctype' => 'multipart/form-data', 'class' => 'd-md-inline'
                  ]) !!}
                  <label for="processing_file{{$value->id}}" class="btn btn-primary mb-0"><i
                      class="fas fa-file-upload"></i></label>
                  {{ Form::input(
                                        'file', 'processing_file', null, 
                                        ['class' => 'd-none', 'onchange' => 'submitParent(`form_3_'.$value->id.'`,`processing_file'.$value->id.'`)',
                                        'id' => 'processing_file'.$value->id])}}
                  {{ Form::input('hidden', 'path_url', '/', ['class' => 'old-path'])}}
                  {!! Form::close() !!}
                </td>

                {{-- processed_file_upload --function --}}
                <td class="text-center">
                  @if ($value->processed_file)
                  <div class="d-md-inline">
                    <a role="button" class="btn btn-success"
                      href="{{URL::to('/storage/processed_file_uploads/'.$value['processed_file'])}}" target="_blank"
                      download>
                      <i class="fas fa-file-download"></i>
                    </a>
                  </div>
                  @endif
                  {!! Form::open(
                  ['action' => ['ProjectController@uploadProcessedFile',$value->id],
                  'method' => 'put', 'id' => 'form_5_'.$value->id,
                  'enctype' => 'multipart/form-data', 'class' => 'd-md-inline'
                  ]) !!}
                  <label for="processed_file{{$value->id}}" class="btn btn-primary mb-0"><i
                      class="fas fa-file-upload"></i></label>
                  {{ Form::input(
                                        'file', 'processed_file', null, 
                                        ['class' => 'd-none', 'onchange' => 'submitParent(`form_5_'.$value->id.'`,`processed_file'.$value->id.'`)',
                                        'id' => 'processed_file'.$value->id])}}
                  {{ Form::input('hidden', 'path_url', '/', ['class' => 'old-path'])}}
                  {!! Form::close() !!}
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        @else
        <div class="row">
          <div class="col-12 text-center">
            <h4>ยังไม่มีวิชาที่ต้องรับผิดชอบ</h4>
          </div>
        </div>
        @endif
      </div>
    </div>
  </div>
  @endif
</div>
@endsection

@section('custom_script')
<script>
  function submitParent(formId, inputId) {
    if ($('#' + inputId)[0].files[0].size > 8000000) {
      alert(`ไฟล์มีขนาดเกิน 8MB`)
      $(`#${formId}`)[0].reset();
    } else {
      $(`#${formId}`).submit();
    }
  }
</script>
@endsection