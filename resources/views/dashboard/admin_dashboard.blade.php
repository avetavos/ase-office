@extends('layouts.main')
@section('content')
<div class="row p-3">
    <div class="col-12">
        <h3>หน้าหลัก</h3>
        <hr />
    </div>
    @if ($current_year)
    <div class="col-12 mb-3">
        <div class="card shadow">
            <div class="card-header bg-kku text-light">
                <h5 class="card-title mb-0">
                    ปีการศึกษา {{$current_year->year}}
                </h5>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6 text-center">
                        <p><b>เทอมที่ 1</b></p>
                        <p>ตั้งแต่วันที่ {{$current_year->start_first_term}} ถึงวันที่ {{$current_year->end_first_term}}
                        </p>
                    </div>
                    <div class="col-md-6 text-center">
                        <p><b>เทอมที่ 2</b></p>
                        <p>ตั้งแต่วันที่ {{$current_year->start_second_term}} ถึงวันที่
                            {{$current_year->end_second_term}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if(count($last_project) > 0)
    <div class="col-md-6 mb-3">
        <div class="card shadow">
            <div class="card-header bg-kku text-light">
                <h5 class="card-title mb-0">
                    โครงการที่มีการเคลื่อนไหวล่าสุด
                </h5>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-sm table-bordered table-striped">
                        <thead>
                            <tr>
                                <th scope="col" class="text-center">ชื่อโครงการ</th>
                                <th scope="col" class="text-center">ผู้รับผิดชอบ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($last_project as $item)
                            <tr>
                                <td class="text-center">{{$item->name}}</td>
                                <td class="text-center">{{$item->authorize->name}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if (count($last_subject) > 0)
    <div class="col-md-6 mb-3">
        <div class="card shadow">
            <div class="card-header bg-kku text-light">
                <h5 class="card-title mb-0">
                    วิชาที่มีการเคลื่อนไหวล่าสุด
                </h5>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-sm table-bordered table-striped">
                        <thead>
                            <tr>
                                <th scope="col" class="text-center">รหัส</th>
                                <th scope="col" class="text-center">ชื่อวิชา</th>
                                <th scope="col" class="text-center">ผู้รับผิดชอบ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($last_subject as $item)
                            <tr>
                                <th scope="row" class="text-center">{{$item->code}}</th>
                                <td class="text-center">{{$item->name}}</td>
                                <td class="text-center">{{$item->authorize->name}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endif
    @isset($survey)
    @if (count($survey) > 0)
    <div class="col-12 mb-3">
        <div class="card shadow">
            <div class="card-header bg-kku text-light">
                <h5 class="card-title mb-0">
                    การตอบแบบประเมิน
                </h5>
            </div>
            <div class="card-body row">
                @foreach ($survey as $item)
                @if($item->type == 'bachelor')
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-8">
                            แบบประเมินความพึงพอใจของนักศึกษา ระดับปริญญาตรี สาขา{{$item->branch->name}}
                        </div>
                        <div class="col-md-4 text-md-right">
                            {{$item->count}}/{{$item->amount}}
                        </div>
                        <div class="col-12 mb-3">
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar"
                                    style="width: {{$item->count/$item->amount*100}}%" aria-valuenow="{{$item->count}}"
                                    aria-valuemin="0" aria-valuemax="{{$item->amount}}"></div>
                            </div>
                        </div>

                        @elseif($item->type == 'bachelor_teacher')
                        <div class="col-md-8">
                            แบบประเมินอาจารย์ประจำหลักสูตร ระดับปริญญาตรี สาขา{{$item->branch->name}}
                        </div>
                        <div class="col-md-4 text-md-right">
                            {{$item->count}}/{{$item->amount}}
                        </div>
                        <div class="col-12 mb-3">
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar"
                                    style="width: {{$item->count/$item->amount*100}}%" aria-valuenow="{{$item->count}}"
                                    aria-valuemin="0" aria-valuemax="{{$item->amount}}"></div>
                            </div>
                        </div>

                        @elseif($item->type == 'master')
                        <div class="col-md-8">
                            แบบประเมินความพึงพอใจของนักศึกษา ระดับบัณฑิตศึกษา สาขา{{$item->branch->name}}
                        </div>
                        <div class="col-md-4 text-md-right">
                            {{$item->count}}/{{$item->amount}}
                        </div>
                        <div class="col-12 mb-3">
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar"
                                    style="width: {{$item->count/$item->amount*100}}%" aria-valuenow="{{$item->count}}"
                                    aria-valuemin="0" aria-valuemax="{{$item->amount}}"></div>
                            </div>
                        </div>

                        @elseif($item->type == 'master_teacher')
                        <div class="col-md-8">
                            แบบประเมินอาจารย์ประจำหลักสูตร ระดับบัณฑิตศึกษา สาขา{{$item->branch->name}}
                        </div>
                        <div class="col-md-4 text-md-right">
                            {{$item->count}}/{{$item->amount}}
                        </div>
                        <div class="col-12 mb-3">
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar"
                                    style="width: {{$item->count/$item->amount*100}}%" aria-valuenow="{{$item->count}}"
                                    aria-valuemin="0" aria-valuemax="{{$item->amount}}"></div>
                            </div>
                        </div>

                        @elseif($item->type == 'graduated')
                        <div class="col-md-8">
                            แบบสำรวจความพึงพอใจผู้ใช้บัณฑิต
                        </div>
                        <div class="col-md-4 text-md-right">
                            {{$item->count}}/{{$item->amount}}
                        </div>
                        <div class="col-12 mb-3">
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar"
                                    style="width: {{$item->count/$item->amount*100}}%" aria-valuenow="{{$item->count}}"
                                    aria-valuemin="0" aria-valuemax="{{$item->amount}}"></div>
                            </div>
                        </div>

                        @elseif($item->type == 'teacher')
                        <div class="col-md-8">
                            แบบประเมินความพึงพอใจระบบอาจารย์ที่ปรึกษา สาขา{{$item->branch->name}}
                        </div>
                        <div class="col-md-4 text-md-right">
                            {{$item->count}}/{{$item->amount}}
                        </div>
                        <div class="col-12 mb-3">
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar"
                                    style="width: {{$item->count/$item->amount*100}}%" aria-valuenow="{{$item->count}}"
                                    aria-valuemin="0" aria-valuemax="{{$item->amount}}"></div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @endforeach
            </div>
        </div>
    </div>
    @endif
    @endisset
</div>
@endsection
</div>


@section('custom_script')
<script type="text/javascript">
    new QRCode(document.getElementById("qrcode"), "http://jindo.dev.naver.com/collie");

</script>
@endsection