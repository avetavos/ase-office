@extends('layouts.main') 
@section('content')
<div class="row">
  <div class="col-12 px-0">
    <h3>{{$header}}</h3>
    <hr>
  </div>
  <div class="col-lg-6 mx-auto px-0">
    <h4><u>สาขาวิชา</u></h4>
    @if (count($branches) > 0)
    <ul class="list-group">
      @foreach ($branches as $branch)
      <li class="list-group-item"><a href="{{url('/subject/'.$degree.'/'.$branch->id)}}">{{$branch->name}}</a></li>
      @endforeach
    </ul>
    @else
    <div class="text-center py-3">
      <h4>ยังไม่มีสาขาวิชา</h4>
    </div>
    @endif
  </div>
</div>
@endsection