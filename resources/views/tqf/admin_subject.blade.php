@extends('layouts.main')

@section('content')
<div class="row p-3">
  <div class="col-lg-6">
    <h3>แผนการสอนของสาขา<b>{{$branch->name}}</b></h3>
  </div>
  <div class="col-lg-6 text-right">
    {!! Form::open(['id' => 'education_years_form', 'method' => 'get']) !!} {{ Form::label('year', 'ปีการศึกษา', ['class' =>
    'sr-only']) }}
    <div class="input-group">
      <div class="input-group-prepend">
        <div class="input-group-text">ปีการศึกษา</div>
      </div>
      {{ Form::select('year', $years, $current_year->id, ['class' => 'form-control', 'onchange' => 'this.form.submit()']) }}
    </div>
    {!! Form::close() !!}
  </div>
  <div class="col-12">
    <hr>
    @include('inc.alert')
  </div>
  <div class="col-lg-8 mx-auto">
    <div class="table-responsive">
      <table class="table table-striped">
        <thead class="bg-kku">
          <tr>
            <th scope="col">ลำดับ</th>
            <th scope="col">รหัสวิชา</th>
            <th scope="col">ชื่อวิชา</th>
            <th scope="col">อาจารย์ผู้สอน</th>
          </tr>
        </thead>
        <tbody>
          @if (count($subjects) > 0)
          @foreach ($subjects as $key=>$value)
          <tr onclick="window.location = '{{url('tqf/'.$value->id.'/edit')}}'" style="cursor: pointer">
            <td>{{$key + 1}}</td>
            <td>{{$value->code}}</td>
            <td>{{$value->name}}</td>
            <td>{{$value->user_id->name}}</td>
          </tr>
          @endforeach
        </tbody>
      </table>@else
      </tbody>
      </table>
    </div>
    <div class="row">
      <div class="col-12 text-center">
        <h4>ยังไม่มีแผนการสอนในตอนนี้</h4>
      </div>
    </div>
    @endif
  </div>
</div>
</div>
@endsection