@extends('layouts.main')

@section('content')
<div class="row p-3">
  <div class="col-12">
    <h3>แก้ไขข้อมูลรายวิชา</h3>
    <hr>
    @include('inc.alert')
  </div>
  <div class="col-lg-10 col-xl-8 mb-3 mx-auto">
    <div class="card p-md-5 p-3">
      {!! Form::open(['action' => ['TqfController@update', $subject->id], 'method' => 'put']) !!}
      <div class="form-row">
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('code', 'รหัสวิชา')}}
          {{ Form::input('text', 'code', $subject->code, ['class' => 'form-control'])}}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('name', 'ชื่อวิชา')}}
          {{ Form::input('text', 'name', $subject->name, ['class' => 'form-control'])}}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('teacher_id', 'อาจารย์ผู้สอน')}}
          {{ Form::select('teacher_id', $teachers, $subject->user_id, ['class' => 'form-control']) }}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('co_teacher', 'อาจารย์ผู้สอนร่วม')}}
          {{ Form::select('co_teacher', $teachers, $subject->co_teacher, ['class' => 'form-control','name' => 'co_teacher[]','multiple' => 'multiple']) }}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('branch_id', 'สาขาของวิชา')}}
          {{ Form::select('branch_id', $branches, $subject->branch_id, ['class' => 'form-control']) }}
        </div>
        <div class="col-lg-8 mx-auto text-center mt-3">
          {{ Form::input('submit', null, 'บันทึก', ['class' => 'btn btn-primary px-5'])}}
        </div>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
  <div class="col-lg-10 col-xl-8 mb-3 mx-auto">
    <div class="card p-3">
      {!! Form::open(['action' => ['TqfController@destroy', $subject->id], 'method' => 'delete']) !!}
      <div class="col-lg-8 mx-auto text-center">
        <h4 class="text-danger">ลบวิชา <b>{{$subject->name}}</b></h4>
        {{ Form::input('hidden', 'branch_id', $subject->branch_id)}}
        {{ Form::input('hidden', 'name', $subject->name)}}
        {{ Form::input('submit', null, 'ลบ', ['class' => 'btn btn-danger px-5'])}}
      </div>
    </div>
    {!! Form::close() !!}
  </div>
</div>
</div>
@endsection
@section('custom_script')
<script>
  $(document).ready(function () {
    $('#co_teacher').select2({ theme: "bootstrap4" });
  })
</script>
@endsection