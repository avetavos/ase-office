@extends('layouts.main')
@section('content')
<div class="row">
  <div class="col-12 px-0">
    <div class="row">
      <div class="col-lg-6">
        <h3 class="mb-md-0 mb-3">{{$branch->name}}</h3>
      </div>
      <div class="col-lg-6 text-right">
        {!! Form::open(['id' => 'education_years_form', 'method' => 'get']) !!} {{ Form::label('year', 'ปีการศึกษา', ['class' =>
        'sr-only']) }}
        <div class="input-group">
          <div class="input-group-prepend">
            <div class="input-group-text">ปีการศึกษา</div>
          </div>
          {{ Form::select('year', $years, $current_year->id, ['class' => 'form-control', 'onchange' => 'this.form.submit()']) }}
        </div>
        {!! Form::close() !!}
      </div>
    </div>
    <hr>
  </div>
  <div class="col-12 mx-auto px-0">
    @include('inc.alert')
    <div class="table-responsive">
      <table class="table table-striped">
        <thead class="bg-kku">
          <tr>
            <th scope="col" class="text-center">
              รหัสวิชา
            </th>
            <th scope="col">ชื่อวิชา</th>
            <th scope="col" class="text-center">มคอ. 3</th>
            <th scope="col" class="text-center">มคอ. 5</th>
            <th scope="col" class="text-center">ผลทวนสอบ</th>
            <th scope="col" class="text-center">อาจารย์ผู้สอน</th>
          </tr>
        </thead>
        <tbody>
          @if (count($subjects) > 0) @foreach ($subjects as $subject)
          <tr>
            <th scope="row" class="text-center">
              {{ $subject["code"] }}
            </th>
            <td>{{ $subject["name"] }}</td>
            <td class="text-center">
              @if ($subject['tqf3'])
              <div class="d-md-inline">
                <a role="button" class="btn btn-success" href="{{URL::to('/storage/tqf_uploads/'.$subject['tqf3'])}}"
                  target="_blank" download>
                  <i class="fas fa-file-download"></i>
                </a>
              </div>
              @endif @if ($subject['user_id'] == Auth::user()->id) {!! Form::open(['action' =>
              ['TqfController@updateTqf3', $subject['id']],
              'method' => 'put', 'id' => 'form_3_'.$subject['code'], 'enctype' => 'multipart/form-data', 'class' =>
              'd-md-inline'])
              !!} {{ Form::input('hidden', 'path_url', $branch->degree.'/'.$branch->id, ['class' => 'old-path'])}}
              <label for="tqf3_document_{{$subject['code']}}" class="btn btn-primary mb-0"><i
                  class="fas fa-file-upload"></i></label>{{
              Form::input('file', 'tqf3_document', null, ['class' => 'd-none', 'onchange' => 'submitParent(`form_3_'.$subject['code'].'`,`tqf3_document'.$subject['code'].'`)',
              'id' => 'tqf3_document_'.$subject['code']])}} {!! Form::close() !!} @endif
            </td>
            <td class="text-center">
              @if ($subject['tqf5'])
              <div class="d-md-inline">
                <a role="button" class="btn btn-success" href="{{URL::to('/storage/tqf_uploads/'.$subject['tqf5'])}}"
                  target="_blank" download>
                  <i class="fas fa-file-download"></i>
                </a>
              </div>
              @endif @if ($subject['user_id'] == Auth::user()->id){!! Form::open(['action' =>
              ['TqfController@updateTqf5', $subject['id']],
              'method' => 'put', 'id' => 'form_5_'.$subject['code'], 'enctype' => 'multipart/form-data', 'class' =>
              'd-md-inline'])
              !!} {{ Form::input('hidden', 'path_url', $branch->degree.'/'.$branch->id, ['class' => 'old-path'])}}
              <label for="tqf5_document_{{$subject['code']}}" class="btn btn-primary mb-0"><i
                  class="fas fa-file-upload"></i></label>{{
              Form::input('file', 'tqf5_document', null, ['class' => 'd-none', 'onchange' => 'submitParent(`form_5_'.$subject['code'].'`,`tqf5_document'.$subject['code'].'`)',
              'id' => 'tqf5_document_'.$subject['code']])}} {!! Form::close() !!} @endif
            </td>
            <td class="text-center">
              @if ($subject['exam'])
              <div class="d-md-inline">
                <a role="button" class="btn btn-success" href="{{URL::to('/storage/tqf_uploads/'.$subject['exam'])}}"
                  target="_blank" download>
                  <i class="fas fa-file-download"></i>
                </a>
              </div>
              @endif @if ($subject['user_id'] == Auth::user()->id){!! Form::open(['action' =>
              ['TqfController@updateExam', $subject['id']],
              'method' => 'put', 'id' => 'form_exam_'.$subject['code'], 'enctype' => 'multipart/form-data', 'class' =>
              'd-md-inline'])
              !!} {{ Form::input('hidden', 'path_url', $branch->degree.'/'.$branch->id, ['class' => 'old-path'])}}
              <label for="exam_document_{{$subject['code']}}" class="btn btn-primary mb-0"><i
                  class="fas fa-file-upload"></i></label>{{
              Form::input('file', 'exam_document', null, ['class' => 'd-none', 'onchange' => 'submitParent(`form_exam_'.$subject['code'].'`,`exam_document'.$subject['code'].'`)',
              'id' => 'exam_document_'.$subject['code']])}} {!! Form::close() !!} @endif
            </td>
            <td class="text-center">
              {{ $subject['teacher']['name']}}
            </td>
          </tr>
          @endforeach @else
          <tr class="text-center">
            <td colspan="6">
              <h4>ยังไม่มีวิชาในความรับผิดชอบ</h4>
            </td>
          </tr>
          @endif
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection

@section('custom_script')
<script>
  function submitParent(formId, inputId) {
    if ($('#' + inputId)[0].files[0].size > 8000000) {
      alert(`ไฟล์มีขนาดเกิน 8MB`)
      $(`#${formId}`)[0].reset();
    } else {
      $(`#${formId}`).submit();
    }
  }
  $('.old-path').val(window.location.pathname);
  $('#education_years_form').arrt('action', window.location.pathname)
</script>
@endsection