@extends('layouts.main')

@section('content')
<div class="row p-3">
  <div class="col-12">
    <h3>สร้างวิชาใหม่</h3>
    <hr>
    @include('inc.alert')
  </div>
  <div class="col-lg-10 col-xl-8 mb-3 mx-auto">
    <div class="card p-md-5 p-3">
      {!! Form::open(['action' => 'TqfController@store']) !!}
      <div class="form-row">
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('code', 'รหัสวิชา')}}
          {{ Form::input('text', 'code', null, ['class' => 'form-control'])}}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('name', 'ชื่อวิชา')}}
          {{ Form::input('text', 'name', null, ['class' => 'form-control'])}}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('teacher_id', 'อาจารย์ผู้สอน')}}
          {{ Form::select('teacher_id', $teachers, null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('co_teacher', 'อาจารย์ผู้สอนร่วม')}}
          {{ Form::select('co_teacher', $teachers, null, ['class' => 'form-control','name' => 'co_teacher[]','multiple' => 'multiple']) }}
        </div>
        <div class="form-group col-lg-8 mx-auto">
          {{ Form::label('branch_id', 'สาขาของวิชา')}}
          {{ Form::select('branch_id', $branches, null, ['class' => 'form-control']) }}
        </div>
        <div class="col-lg-8 mx-auto text-center mt-3">
          {{ Form::input('submit', null, 'สร้าง', ['class' => 'btn btn-primary px-5'])}}
        </div>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection
@section('custom_script')
<script>
  $(document).ready(function () {
    $('#co_teacher').select2({ theme: "bootstrap4" });
  })
</script>
@endsection