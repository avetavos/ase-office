<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Subject extends Model
{
    protected $table = 'subjects';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function branch()
    {
        return $this->belongsTo('App\Branch');
    }

    public function teacher()
    {
        return $this->belongsTo('App\User');
    }

    public function education_year()
    {
        return $this->belongsTo('App\EducationYear');
    }

    public function authorize(){
        return $this->belongsTo('App\User');  //one to many
    }

    public function getAuthorize() {
        return User::find($this->user_id);
    }
}
