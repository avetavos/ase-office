<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table = 'branches';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function subjects()
    {
        return $this->hasMany('App\Subject');
    }

    public function surveys() {
        return $this->hasMany('Survey');
    }

    public function getBranch() {
        return Branch::where('id', $this->branch_id);
    }
}
