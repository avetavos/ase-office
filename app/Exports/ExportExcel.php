<?php

namespace App\Exports;
use App\SurveyData;
use App\Survey;
use App\Question;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExportExcel implements FromCollection, WithHeadings
{
    use Exportable;

    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $survey = Survey::find($this->data);
        $surveyData = SurveyData::select('answer')->where('survey_id', $this->data)->whereBetween('created_at', [$survey->start_date, $survey->end_date])->get();
        foreach($surveyData as $key=>$value) {
            $surveyData[$key] = json_decode($value->answer);
        }
        foreach($surveyData as $key=>$item) {
            foreach($item as $itemKey=>$value) {
                if (is_array($value)) {
                    $surveyData[$key]->$itemKey = implode (", ", $value);
                }
            }
        }
        return collect($surveyData);
    }
    public function headings(): array {
        $survey = Survey::find($this->data);
        if ($survey->type === 'bachelor' || $survey->type === 'master') {
            $questions = new Question();
            return $questions->bachelorAndMasterQuestion();
        } else if ($survey->type === 'bachelor_teacher' || $survey->type === 'master_teacher') {
            $questions = new Question();
            return $questions->bachelorAndMasterTeacherQuestion();
        } else if ($survey->type === 'teacher') {
            $questions = new Question();
            return $questions->teacherQuestions();
        } else if ($survey->type === 'graduated') {
            $questions = new Question();
            return $questions->graduatedQuestion();
        }
    }
}
