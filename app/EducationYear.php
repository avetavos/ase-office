<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EducationYear extends Model
{
    protected $table = 'education_years';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function subjects()
    {
        return $this->hasMany('App\Subject');
    }

    public function surveys()
    {
        return $this->hasMany('App\Surveys');
    }
    public function project_year()
    {
        return $this->hasOne('App\ProjectYear');
    }
}
