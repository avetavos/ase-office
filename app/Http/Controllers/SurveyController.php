<?php

namespace App\Http\Controllers;

use App\Branch;
use Response;
use Redirect;
use App\EducationYear;
use App\Survey;
use App\SurveyData;
use Illuminate\Http\Request;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $years = EducationYear::orderBy('id', 'desc')->get();
        if (sizeof($years) < 1) {
            return redirect('/education_year/create')->with('error', 'กรุณาสร้างปีการศึกษา');
        }
        $branches = Branch::all();
        $bachelor = Branch::where('degree', 'bachelor')->get();
        $master = Branch::where('degree', 'master')->orWhere('degree', 'doctor')->get();
        $data = array(
            'types' => [
                'graduated' => 'แบบสำรวจความพึงพอใจผู้ใช้บัณฑิต',
            ],
        );
        if (sizeof($bachelor) > 0) {
            $data['types']['bachelor'] = 'แบบประเมินความพึงพอใจของนักศึกษา ระดับปริญญาตรี';
            $data['types']['bachelor_teacher'] = 'แบบประเมินอาจารย์ประจำหลักสูตร ระดับปริญญาตรี';
            $data['types']['teacher'] = 'แบบประเมินความพึงพอใจระบบอาจารย์ที่ปรึกษา';
        }
        if (sizeof($master) > 0) {
            $data['types']['master'] = 'แบบประเมินความพึงพอใจของนักศึกษา ระดับบัณฑิตศึกษา';
            $data['types']['master_teacher'] = 'แบบประเมินอาจารย์ประจำหลักสูตร ระดับบัณฑิตศึกษา';
        }
        if (sizeof($branches) < 1) {
            return redirect('/branch/create')->with('error', 'กรุณาสร้างสาขาวิชา');
        }
        foreach ($years as $year) {
            $data['years'][$year->id] = $year->year;
        }
        return view('survey.admin_create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'type' => 'required',
            'year' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'amount' => 'required',
        ]);
        $check = Survey::where('type', $request->type)->where('education_year_id', $request->year)->get();
        if (sizeof($check) == 0) {
            if ($request->type == 'bachelor' || $request->type == 'bachelor_teacher' || $request->type == 'teacher') {
                $branches = Branch::all()->where('degree', '=', 'bachelor');
                foreach ($branches as $branch) {
                    $newSurvey = new Survey;
                    $newSurvey->type = $request->type;
                    $newSurvey->start_date = $request->start_date;
                    $newSurvey->end_date = $request->end_date;
                    $newSurvey->amount = $request->amount;
                    $newSurvey->education_year_id = $request->year;
                    $newSurvey->branch_id = $branch->id;
                    $newSurvey->save();
                }
            } else if ($request->type == 'master' || $request->type == 'master_teacher') {
                $branches = Branch::all()->where('degree', '!=', 'bachelor');
                foreach ($branches as $branch) {
                    $newSurvey = new Survey;
                    $newSurvey->type = $request->type;
                    $newSurvey->start_date = $request->start_date;
                    $newSurvey->end_date = $request->end_date;
                    $newSurvey->amount = $request->amount;
                    $newSurvey->education_year_id = $request->year;
                    $newSurvey->branch_id = $branch->id;
                    $newSurvey->save();
                }
            } else {
                $newSurvey = new Survey;
                $newSurvey->type = $request->type;
                $newSurvey->start_date = $request->start_date;
                $newSurvey->end_date = $request->end_date;
                $newSurvey->amount = $request->amount;
                $newSurvey->education_year_id = $request->year;
                $newSurvey->save();
            }
            return redirect('/admin_survey/create')->with('success', 'สร้างแบบประเมินเรียบร้อยแล้ว');
        } else {
            return redirect('/admin_survey/create')->with('error', 'มีแบบประเมินนี้ในระบบอยู่แล้ว');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($survey_id)
    {
        $survey = Survey::find($survey_id);
        $survey->education_year = EducationYear::find($survey->education_year_id);
        $survey->branch;
        if ($survey->type == 'bachelor') {
            return view('survey_form.bachelor_main')->with('survey', $survey);
        } else
        if ($survey->type == 'master') {
            return view('survey_form.master_main')->with('survey', $survey);
        } else
        if ($survey->type == 'bachelor_teacher') {
            return view('survey_form.bachelor_teacher_main')->with('survey', $survey);
        } else
        if ($survey->type == 'master_teacher') {
            return view('survey_form.master_teacher_main')->with('survey', $survey);
        } else
        if ($survey->type == 'teacher') {
            return view('survey_form.teacher_main')->with('survey', $survey);
        } else
        if ($survey->type == 'graduated') {
            return view('survey_form.graduated_main')->with('survey', $survey);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $survey = Survey::find($id);
        $data = array(
            'survey' => $survey,
        );
        if ($survey->type == 'bachelor' || $survey->type == 'master') {
            $data['header'] = 'แบบประเมินความพึงพอใจของนักศึกษาที่มีต่อคุณภาพหลักสูตร' . $survey->branch->name;
        }
        if ($survey->type == 'master_teacher' || $survey->type == 'bachelor_teacher') {
            $data['header'] = 'แบบประเมินอาจารย์ประจำหลักสูตรหลักสูตร' . $survey->branch->name;
        }
        if ($survey->type == 'teacher') {
            $data['header'] = 'แบบประเมินความพึงพอใจระบบอาจารย์ที่ปรึกษาในหลักสูตร' . $survey->branch->name;
        }
        if ($survey->type == 'graduated') {
            $data['header'] = 'แบบสำรวจความพึงพอใจผู้ใช้บัณฑิต';
        }
        return view('survey.admin_edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'start_date' => 'required',
            'end_date' => 'required',
            'amount' => 'required',
        ]);
        $survey = Survey::find($id);
        $survey->start_date = $request->start_date;
        $survey->end_date = $request->end_date;
        $survey->amount = $request->amount;
        $survey->save();
        return redirect('/admin_survey_list/' . $survey->type)->with('success', 'แก้ไขแบบประเมินเรียบร้อยแล้ว');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function typeIndex(Request $request)
    {
        return view('survey.admin_type');
    }

    public function branchIndex(Request $request, $type)
    {
        $years = EducationYear::orderBy('id', 'desc')->get();
        if (sizeof($years) <= 0) {
            return redirect('/education_year/create')->with('error', 'กรุณาสร้างปีการศึกษา');
        }
        if ($request->year) {
            $current_year = EducationYear::find($request->year);
        } else {
            $current_year = EducationYear::orderBy('id', 'desc')->first();
        }
        $survey = Survey::where('type', $type)->where('education_year_id', $current_year->id)->get();
        if ($type == 'graduated') {
            $data = array(
                'branches' => [],
                'current_year' => $current_year,
                'survey' => $survey,
            );
            foreach ($years as $year) {
                $data['years'][$year->id] = $year->year;
            }
            foreach ($survey as $item) {
                $data['branches'][$item->id] = 'แบบสำรวจความพึงพอใจผู้ใช้บัณฑิตประจำปีการศึกษา '.$current_year->year;
            }
        } else {
            $data = array(
                'branches' => [],
                'current_year' => $current_year,
                'survey' => $survey,
            );
            foreach ($years as $year) {
                $data['years'][$year->id] = $year->year;
            }
            foreach ($survey as $item) {
                $branch = $item->getBranch();
                $data['branches'][$branch->id] = $branch->name;
            }
        }
        return view('survey.admin_branch')->with($data);
    }

    ///User Survey Function
    public function UserTypeIndex(Request $request)
    {
        return view('survey.user_type');
    }

    public function levelIndex(Request $request, $level)
    {
        $years = EducationYear::orderBy('id', 'desc')->get();
        if ($request->year) {
            $current_year = EducationYear::find($request->year);
        } else {
            $current_year = EducationYear::orderBy('id', 'desc')->first();
        }
        $survey = Survey::where('type', $level)->where('education_year_id', $current_year->id)->get();
        if ($level == 'graduated') {
            $data = array(
                'level' => $level,
                'current_year' => $current_year,
                'survey' => $survey,
            );
            foreach ($years as $year) {
                $data['years'][$year->id] = $year->year;
            }
        } else {
            $data = array(
                'branches' => array(),
                'level' => $level,
                'current_year' => $current_year,
                'survey' => $survey,
            );
            foreach ($years as $year) {
                $data['years'][$year->id] = $year->year;
            }
            foreach ($survey as $item) {
                $branch = $item->getBranch();
                $data['branches'][$branch->id] = $branch->name;
            }
        }
        return view('survey.user_index')->with($data);
    }
    public function qrcodeBranch(Request $request, $level, $branch_id)
    {
        $survey = Survey::find($branch_id);
        $years = EducationYear::all();
        $current_year = EducationYear::orderBy('id', 'desc')->first();
        $data = array(
            'survey' => $survey,
            'current_year' => $current_year,
        );
        foreach ($years as $year) {
            $data['years'][$year->id] = $year->year;
        }
        if ($level == 'bachelor') {
            $data['header'] = 'แบบประเมินความพึงพอใจของนักศึกษาที่มีต่อคุณภาพหลักสูตร' . $survey->branch->name;
        } else if ($level == 'master') {
            $data['header'] = 'แบบประเมินความพึงพอใจของนักศึกษาที่มีต่อคุณภาพหลักสูตร' . $survey->branch->name;
        } else if ($level == 'bachelor_teacher') {
            $data['header'] = 'แบบประเมินอาจารย์ประจำหลักสูตร หลักสูตร' . $survey->branch->name;
        } else if ($level == 'master_teacher') {
            $data['header'] = 'แบบประเมินอาจารย์ประจำหลักสูตร หลักสูตร' . $survey->branch->name;
        } else if ($level == 'teacher') {
            $data['header'] = 'แบบประเมินความพึงพอใจระบบอาจารย์ที่ปรึกษาในหลักสูตร' . $survey->branch->name;
        } else if ($level == 'graduated') {
            $data['header'] = 'แบบสำรวจความพึงพอใจผู้ใช้บัณทิต';
        }
        return view('survey.user_qrcode')->with($data);
    }

    //Get Answer Function save
    public function save(Request $request, $id)
    {
        unset($request['_token']);
        $data = json_encode($request->all());
        $surveyData = new SurveyData;
        $surveyData->answer = $data;
        $surveyData->survey_id = $id;
        $surveyData->save();
        return Redirect::to('/survey_form/'.$id.'/saved');
    }

    public function saveSuccess($id) {
        $survey = Survey::find($id);
        $survey->branch();
        $data = array(
            'survey' => $survey,
        );
        if ($survey->type == 'bachelor') {
            $data['header'] = 'แบบประเมินความพึงพอใจของนักศึกษาที่มีต่อคุณภาพหลักสูตร' . $survey->branch->name;
        } else if ($survey->type == 'master') {
            $data['header'] = 'แบบประเมินความพึงพอใจของนักศึกษาที่มีต่อคุณภาพหลักสูตร' . $survey->branch->name;
        } else if ($survey->type == 'bachelor_teacher') {
            $data['header'] = 'แบบประเมินอาจารย์ประจำหลักสูตร หลักสูตร' . $survey->branch->name;
        } else if ($survey->type == 'master_teacher') {
            $data['header'] = 'แบบประเมินอาจารย์ประจำหลักสูตร หลักสูตร' . $survey->branch->name;
        } else if ($survey->type == 'teacher') {
            $data['header'] = 'แบบประเมินความพึงพอใจระบบอาจารย์ที่ปรึกษาในหลักสูตร' . $survey->branch->name;
        } else if ($survey->type == 'graduated') {
            $data['header'] = 'แบบสำรวจความพึงพอใจผู้ใช้บัณทิต';
        }
        return view('report_survey.end_section')->with($data);
    }
}
