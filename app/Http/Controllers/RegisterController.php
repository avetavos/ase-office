<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function show() {
        return view('auth.register');
    }
    
    public function register(Request $request) {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'min:6','max:20', 'unique:users'],
        ]);
        User::create([
            'name' => $request->name,
            'username' => $request->username,
            'password' => Hash::make('ase123456'),
            'admin' => $request->role
        ]);
        return redirect('/register')->with('success', 'สร้างบัญชีผู้ใช้เรียบร้อยแล้ว');
    }
}
