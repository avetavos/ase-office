<?php

namespace App\Http\Controllers;

use Response;
use Auth;
use App\Branch;
use App\User;
use App\EducationYear;
use App\Subject;
use Illuminate\Http\Request;
use Storage;

class TqfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bachelor = Branch::all()->where('degree', '=', 'bachelor');
        $master = Branch::all()->where('degree', '=', 'master');
        $doctor = Branch::all()->where('degree', '=', 'doctor');
        $data = array(
            'bachelor' => $bachelor,
            'master' => $master,
            'doctor' => $doctor,
        );
        return view('tqf.admin_index', $data);
    }
    public function indexOfBranch(Request $request, $branch) {
        $years = EducationYear::orderBy('id', 'desc')->get();
        if (sizeof($years) <= 0) {
            return redirect('education_year/create')->with('error', 'กรุณาสร้างปีการศึกษา');
        }
        $branch = Branch::find($branch);
        if ($request->year) {
            $current_year = EducationYear::find($request->year);
        } else {
            $current_year = EducationYear::orderBy('id', 'desc')->first();
        }
        $subjects = Subject::where('branch_id',$branch->id)->where('education_year_id',$current_year->id)->get();
        foreach($subjects as $subject) {
            $subject['user_id'] = User::find($subject->user_id);
        }
        $data = array(
            'current_year' => $current_year,
            'subjects' => $subjects,
            'branch' => $branch
        );
        foreach ($years as $year) {
            $data['years'][$year->id] = $year->year;
        }
        return view('tqf.admin_subject')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            $teachers = User::all()->where('admin', '!=', '1')->sortBy('name');
            $branches = Branch::all();
            $year = EducationYear::all();
            if(sizeof($year) <= 0) {
                return redirect('/education_year/create')->with('error', 'กรุณาสร้างปีการศึกษา');
            }
            foreach ($teachers as $teacher) {
                $data['teachers'][$teacher->id] = $teacher->name;
            }
            foreach ($branches as $branch) {
                $data['branches'][$branch->id] = $branch->name;
            }
            if (!isset($data['branches'])) {
                return redirect('/branch/create')->with('error', 'กรุณาสร้างสาขาวิชาเนื่องจากยังไม่มีหลักสูตรในระบบ');
            }
            if (!isset($data['teachers'])) {
                return redirect('/register')->with('error', 'กรุณาสร้างบัญชีผู้ใช้ของอาจารย์และบุคลากรก่อนอย่างน้อย 1 บัญชี');
            }
            if (sizeof($year) <= 0) {
                return redirect('/education_year/create')->with('error', 'กรุณาสร้างปีการศึกษา');
            }
            return view('tqf.admin_create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $this->validate($request, [
                'code' => 'required',
                'name' => 'required',
                'branch_id' => 'required',
                'teacher_id' => 'required',
            ]);
            $current_year = EducationYear::orderBy('id', 'desc')->first();
            $subject = new Subject;
            $subject->code = $request->code;
            $subject->name = $request->name;
            $subject->branch_id = $request->branch_id;
            $subject->user_id = $request->teacher_id;
            if(isset($request->co_teacher) && sizeof($request->co_teacher) > 0) {
                $subject->co_teacher = implode(',',$request->co_teacher);
            } else {
                $subject->co_teacher = "";
            }
            $subject->education_year_id = $current_year->id;
            $subject->save();
            return redirect('/subjects/'.$request->branch_id)->with('success', 'สร้างวิชา '.$request->name.' เรียบร้อยแล้ว');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
            $subject = Subject::find($id);
            $teachers = User::all()->where('username', '!=', 'admin');
            $branches = Branch::all();
            $years = EducationYear::all();
            $subject->co_teacher = explode(',',$subject->co_teacher);
            $data = array(
                'subject' => $subject,
            );
            foreach ($teachers as $teacher) {
                $data['teachers'][$teacher->id] = $teacher->name;
            }
            foreach ($branches as $branch) {
                $data['branches'][$branch->id] = $branch->name;
            }
            foreach ($years as $year) {
                $data['years'][$year->id] = $year->year;
            }
            return view('tqf.admin_edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'code' => 'required',
            'name' => 'required',
            'teacher_id' => 'required',
            'branch_id' => 'required',
        ]);
        $subject = Subject::find($id);
        $subject->code = $request->code;
        $subject->name = $request->name;
        $subject->branch_id = $request->branch_id;
        $subject->user_id = $request->teacher_id;
        if(isset($request->co_teacher) && sizeof($request->co_teacher) > 0) {
            $subject->co_teacher = implode(',',$request->co_teacher);
        } else {
            $subject->co_teacher = "";
        }
        $subject->save();
        return redirect('/subjects/'.$subject->branch_id)->with('success', 'บันทึกแก้ไขวิชา '.$request->name.' เรียบร้อยแล้ว');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Subject::destroy($id);
        return redirect('/subjects/'.$request->branch_id)->with('success', 'ลบวิชา '.$request->name.' เรียบร้อยแล้ว');
    }


    //Upload tqf User

    public function updateTqf3(Request $request, $id)
    {
        $subject = Subject::find($id);
        if ($subject->tqf3 != "") {
            $filePath = 'public/tqf_uploads/' . $subject->tqf3;
            Storage::delete($filePath);
        }
        $filenameWithExt = $request->file('tqf3_document')->getClientOriginalName();
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        $extension = $request->file('tqf3_document')->getClientOriginalExtension();
        $filenameToStore = $filename . '_' . time() . '.' . $extension;
        $path = $request->file('tqf3_document')->storeAs('public/tqf_uploads', $filenameToStore);
        $subject->tqf3 = $filenameToStore;
        $subject->save();
        return redirect($request->path_url);
    }

    public function updateTqf5(Request $request, $id)
    {
        $subject = Subject::find($id);
        if ($subject->tqf5 != "") {
            $filePath = 'public/tqf_uploads/' . $subject->tqf5;
            Storage::delete($filePath);
        }
        $filenameWithExt = $request->file('tqf5_document')->getClientOriginalName();
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        $extension = $request->file('tqf5_document')->getClientOriginalExtension();
        $filenameToStore = $filename . '_' . time() . '.' . $extension;
        $path = $request->file('tqf5_document')->storeAs('public/tqf_uploads', $filenameToStore);
        $subject->tqf5 = $filenameToStore;
        $subject->save();
        return redirect($request->path_url);
    }
    public function updateExam(Request $request, $id)
    {
        $subject = Subject::find($id);
        if ($subject->exam != "") {
            $filePath = 'public/tqf_uploads/' . $subject->exam;
            Storage::delete($filePath);
        }
        $filenameWithExt = $request->file('exam_document')->getClientOriginalName();
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        $extension = $request->file('exam_document')->getClientOriginalExtension();
        $filenameToStore = $filename . '_' . time() . '.' . $extension;
        $path = $request->file('exam_document')->storeAs('public/tqf_uploads', $filenameToStore);
        $subject->exam = $filenameToStore;
        $subject->save();
        return redirect($request->path_url);
    }

    //User TQF
    public function degreeIndex(Request $request, $degree)
    {
        $branches = Branch::all()->where('degree', '=', $degree);
        $data = array(
            'branches' => $branches,
            'degree' => $degree,
        );
        if ($degree == 'bachelor') {
            $data['header'] = 'ระดับปริญญาตรี';
        } else {
            $data['header'] = 'ระดับบัณฑิตศึกษา';
        }
        return view('tqf.user_index')->with($data);
    }

    public function subjectIndex(Request $request, $degree, $branch)
    {
        $years = EducationYear::all();
        if ($request->year) {
            $current_year = EducationYear::find($request->year);
        } else {
            $current_year = EducationYear::orderBy('id', 'desc')->first();
        }
        $subjects = Subject::all()->where('branch_id', '=', $branch)->where('education_year_id', '=', $current_year->id)->sortBy('code');
        $branch_detail = Branch::find($branch);
        $data = array(
            'subjects' => $subjects,
            'branch' => $branch_detail,
            'current_year' => $current_year,
        );
        foreach ($years as $year) {
            $data['years'][$year->id] = $year->year;
        }
        foreach ($data['subjects'] as $key => $subject) {
            $teacher = User::find($subject->user_id);
            $data['subjects'][$key]['teacher'] = $teacher;
        }
        return view('tqf.user_subject')->with($data);
    }

}
