<?php

namespace App\Http\Controllers;

use Response;
use Illuminate\Http\Request;
use App\EducationYear;
use App\ProjectYear;
use App\Project;
use App\User;
use Storage;
use Auth;

class ProjectController extends Controller
{
    public function index(Request $request) {
        $projectYear = ProjectYear::all();
        $years = ProjectYear::orderBy('id', 'desc')->get();
        if (sizeof($years) == 0) {
            return redirect('/project_year/create')->with('error', 'กรุณาสร้างปีการโครงการ');
        }
        if ($request->year) {
            $now = ProjectYear::where('education_year_id', $request->year)->first();
            if ($now) {
                $education = Project::where('type', 1)->where('project_year_id', $now->id)->get();
                $research = Project::where('type', 2)->where('project_year_id', $now->id)->get();
                $humanresource = Project::where('type', 3)->where('project_year_id', $now->id)->get();
                $academic = Project::where('type', 4)->where('project_year_id', $now->id)->get();
                $organization = Project::where('type', 5)->where('project_year_id', $now->id)->get();
                $workplace = Project::where('type', 6)->where('project_year_id', $now->id)->get();
                $greencampus = Project::where('type', 7)->where('project_year_id', $now->id)->get();
                $digital = Project::where('type', 8)->where('project_year_id', $now->id)->get();
                $international = Project::where('type', 9)->where('project_year_id', $now->id)->get();
                $governance = Project::where('type', 10)->where('project_year_id', $now->id)->get();
                $cooperate = Project::where('type', 11)->where('project_year_id', $now->id)->get();
            } else {
                $education = [];
                $research = [];
                $humanresource = [];
                $academic = [];
                $organization = [];
                $workplace = [];
                $greencampus = [];
                $digital = [];
                $international = [];
                $governance = [];
                $cooperate = [];
            }

        } else {
            $education = Project::where('type', 1)->where('project_year_id', $years[0]->id)->get();
            $research = Project::where('type', 2)->where('project_year_id', $years[0]->id)->get();
            $humanresource = Project::where('type', 3)->where('project_year_id', $years[0]->id)->get();
            $academic = Project::where('type', 4)->where('project_year_id', $years[0]->id)->get();
            $organization = Project::where('type', 5)->where('project_year_id', $years[0]->id)->get();
            $workplace = Project::where('type', 6)->where('project_year_id', $years[0]->id)->get();
            $greencampus = Project::where('type', 7)->where('project_year_id', $years[0]->id)->get();
            $digital = Project::where('type', 8)->where('project_year_id', $years[0]->id)->get();
            $international = Project::where('type', 9)->where('project_year_id', $years[0]->id)->get();
            $governance = Project::where('type', 10)->where('project_year_id', $years[0]->id)->get();
            $cooperate = Project::where('type', 11)->where('project_year_id', $years[0]->id)->get();
        }
        $data = array(
            'education' => $this->explodeAndGetAuthorize($education),
            'research' => $this->explodeAndGetAuthorize($research),
            'humanresource' => $this->explodeAndGetAuthorize($humanresource),
            'academic' => $this->explodeAndGetAuthorize($academic),
            'organization' => $this->explodeAndGetAuthorize($organization),
            'workplace' => $this->explodeAndGetAuthorize($workplace),
            'greencampus' => $this->explodeAndGetAuthorize($greencampus),
            'digital' => $this->explodeAndGetAuthorize($digital),
            'international' => $this->explodeAndGetAuthorize($international),
            'governance' => $this->explodeAndGetAuthorize($governance),
            'cooperate' => $this->explodeAndGetAuthorize($cooperate),
            'years' => array(),
        );
        if ($request->year) {
            $data['current_year'] = $request->year;
        } else {
            $data['current_year'] = null;
        }
        foreach ($years as $item) {
            $year = $item->getYear();
            $data['years'][$year->id] = $year->year;
        }
        return view('project.admin_index', $data);
    }

    public function show($id) {

    }

    public function create() {
        $years = EducationYear::orderBy('id', 'desc')->get();
        $current_year = EducationYear::orderBy('id', 'desc')->first();
        $project_year = ProjectYear::where('education_year_id', $current_year->id)->first();
        if (!$project_year) {
            return redirect('/project_year/create')->with('error', 'กรุณาสร้างปีการโครงการ');
        }
        $users = User::where('admin', 0)->orderBy('name')->get();
        $project_year->education_indicator = explode(',',$project_year->education_indicator);
        $project_year->education_strategy = explode(',',$project_year->education_strategy);
        $project_year->research_indicator = explode(',',$project_year->research_indicator);
        $project_year->research_strategy = explode(',',$project_year->research_strategy);
        $project_year->humanresource_indicator = explode(',',$project_year->humanresource_indicator);
        $project_year->humanresource_strategy = explode(',',$project_year->humanresource_strategy);
        $project_year->academic_indicator = explode(',',$project_year->academic_indicator);
        $project_year->academic_strategy = explode(',',$project_year->academic_strategy);
        $project_year->organization_indicator = explode(',',$project_year->organization_indicator);
        $project_year->organization_strategy = explode(',',$project_year->organization_strategy);
        $project_year->workplace_indicator = explode(',',$project_year->workplace_indicator);
        $project_year->workplace_strategy = explode(',',$project_year->workplace_strategy);
        $project_year->greencampus_indicator = explode(',',$project_year->greencampus_indicator);
        $project_year->greencampus_strategy = explode(',',$project_year->greencampus_strategy);
        $project_year->digital_indicator = explode(',',$project_year->digital_indicator);
        $project_year->digital_strategy = explode(',',$project_year->digital_strategy);
        $project_year->international_indicator = explode(',',$project_year->international_indicator);
        $project_year->international_strategy = explode(',',$project_year->international_strategy);
        $project_year->governance_indicator = explode(',',$project_year->governance_indicator);
        $project_year->governance_strategy = explode(',',$project_year->governance_strategy);
        $project_year->cooperate_indicator = explode(',',$project_year->cooperate_indicator);
        $project_year->cooperate_strategy = explode(',',$project_year->cooperate_strategy);
        $data = array(
            'current_year' => $current_year,
            'project_year' => $project_year
        );
        foreach ($years as $year) {
            $data['years'][$year->id] = $year->year;
        }
        foreach ($users as $user) {
            $data['users'][$user->id] = $user->name;
        }
        if (!isset($data['users'])) {
            return redirect('/register')->with('error', 'กรุณาสร้างบัญชีผู้ใช้ของอาจารย์และบุคลากรก่อนอย่างน้อย 1 บัญชี');
        }
        return view('project.admin_create', $data);
    }

    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'type' => 'required',
            'indicator' => 'required',
            'strategy' => 'required',
            'user_id' => 'required',
        ]);
        $project_year = ProjectYear::orderBy('id', 'desc')->first();
        $project = new Project;
        $project->name = $request->name;
        $project->type = $request->type;
        $project->indicator = implode(',',$request->indicator);
        $project->strategy = $request->strategy;
        $project->user_id = $request->user_id;
        $project->project_year_id = $project_year->id;
        $project->save();
        return redirect('/project')->with('success', 'สร้างโครงการ '.$request->name.' เรียบร้อยแล้ว');
    }

    public function edit($id) {
        $years = EducationYear::orderBy('id', 'desc')->get();
        $current_year = EducationYear::orderBy('id', 'desc')->first();
        $users = User::all()->where('admin', '!=', 'admin')->sortBy('name');
        $project = Project::find($id);
        $project->indicator = explode(',',$project->indicator);
        $project_year = ProjectYear::where('education_year_id', $current_year->id)->first();
        $users = User::where('admin', 0)->orderBy('name')->get();
        $project_year->education_indicator = $this->createKeyByValue($project_year->education_indicator);
        $project_year->education_strategy = explode(',',$project_year->education_strategy);
        $project_year->research_indicator = $this->createKeyByValue($project_year->research_indicator);
        $project_year->research_strategy = explode(',',$project_year->research_strategy);
        $project_year->humanresource_indicator = $this->createKeyByValue($project_year->humanresource_indicator);
        $project_year->humanresource_strategy = explode(',',$project_year->humanresource_strategy);
        $project_year->academic_indicator = $this->createKeyByValue($project_year->academic_indicator);
        $project_year->academic_strategy = explode(',',$project_year->academic_strategy);
        $project_year->organization_indicator = $this->createKeyByValue($project_year->organization_indicator);
        $project_year->organization_strategy = explode(',',$project_year->organization_strategy);
        $project_year->workplace_indicator = $this->createKeyByValue($project_year->workplace_indicator);
        $project_year->workplace_strategy = explode(',',$project_year->workplace_strategy);
        $project_year->greencampus_indicator = $this->createKeyByValue($project_year->greencampus_indicator);
        $project_year->greencampus_strategy = explode(',',$project_year->greencampus_strategy);
        $project_year->digital_indicator = $this->createKeyByValue($project_year->digital_indicator);
        $project_year->digital_strategy = explode(',',$project_year->digital_strategy);
        $project_year->international_indicator = $this->createKeyByValue($project_year->international_indicator);
        $project_year->international_strategy = explode(',',$project_year->international_strategy);
        $project_year->governance_indicator = $this->createKeyByValue($project_year->governance_indicator);
        $project_year->governance_strategy = explode(',',$project_year->governance_strategy);
        $project_year->cooperate_indicator = $this->createKeyByValue($project_year->cooperate_indicator);
        $project_year->cooperate_strategy = explode(',',$project_year->cooperate_strategy);
        $data = array(
            'current_year' => $current_year,
            'project' => $project,
            'project_year' => $project_year
        );
        foreach ($years as $year) {
            $data['years'][$year->id] = $year->year;
        }
        foreach ($users as $user) {
            $data['users'][$user->id] = $user->name;
        }
        if (!isset($data['users'])) {
            return redirect('/register')->with('error', 'กรุณาสร้างบัญชีผู้ใช้ของอาจารย์และบุคลากรก่อนอย่างน้อย 1 บัญชี');
        }
        return view('project.admin_edit')->with($data);
    }

    public function update(Request $request, $id) {
        $project = Project::find($id);
        $project->name = $request->name;
        $project->type = $request->type;
        $project->strategy = $request->strategy;
        $project->indicator = implode(',',$request->indicator);
        $project->user_id = $request->user_id;
        $project->save();
        return redirect('/project')->with('success', 'บันทึกการแก้ไขโครงการ '.$request->name.' เรียบร้อยแล้ว');
    }

    public function destroy(Request $request, $id) {
        Project::destroy($id);
        return redirect('/project')->with('success', 'ลบโครงการ '.$request->name.' เรียบร้อยแล้ว');
    }

    public function createYear() {
        $current_year = EducationYear::orderBy('id','desc')->first();
        $years = EducationYear::all();
        if (sizeof($years) <= 0) {
            return redirect('/education_year/create')->with('error', 'กรุณาสร้างปีการศึกษา');
        }
        $check_project_year = ProjectYear::where('education_year_id', $current_year->id)->first();
        if ($check_project_year) {
            return redirect('/project_year/edit')->with('error', 'มีปีโครงการปัจจุบันในอยู่แล้ว');
        }
        $current_year = EducationYear::orderBy('id', 'desc')->first();
        $data = array(
            'current_year' => $current_year,
        );
        foreach ($years as $year) {
            $data['years'][$year->id] = $year->year;
        }
        return view('project.admin_createYear', $data);
    }

    public function storeYear(Request $request) {
        $current_year = EducationYear::orderBy('id','desc')->first();
        $project_year = new ProjectYear;
        $project_year->education_indicator = implode(',',$request->education_indicator);
        $project_year->education_strategy = implode(',',$request->education_strategy);
        $project_year->research_indicator = implode(',',$request->research_indicator);
        $project_year->research_strategy = implode(',',$request->research_strategy);
        $project_year->humanresource_indicator = implode(',',$request->humanresource_indicator);
        $project_year->humanresource_strategy = implode(',',$request->humanresource_strategy);
        $project_year->academic_indicator = implode(',',$request->academic_indicator);
        $project_year->academic_strategy = implode(',',$request->academic_strategy);
        $project_year->organization_indicator = implode(',',$request->organization_indicator);
        $project_year->organization_strategy = implode(',',$request->organization_strategy);
        $project_year->workplace_indicator = implode(',',$request->workplace_indicator);
        $project_year->workplace_strategy = implode(',',$request->workplace_strategy);
        $project_year->greencampus_indicator = implode(',',$request->greencampus_indicator);
        $project_year->greencampus_strategy = implode(',',$request->greencampus_strategy);
        $project_year->digital_indicator = implode(',',$request->digital_indicator);
        $project_year->digital_strategy = implode(',',$request->digital_strategy);
        $project_year->international_indicator = implode(',',$request->international_indicator);
        $project_year->international_strategy = implode(',',$request->international_strategy);
        $project_year->governance_indicator = implode(',',$request->governance_indicator);
        $project_year->governance_strategy = implode(',',$request->governance_strategy);
        $project_year->cooperate_indicator = implode(',',$request->cooperate_indicator);
        $project_year->cooperate_strategy = implode(',',$request->cooperate_strategy);
        $project_year->education_year_id = $current_year->id;
        $project_year->save();
        return redirect('/project_year/edit')->with('success', 'สร้างปีโครงการเรียบร้อยแล้ว');
    }

    public function editYear() {
        $project_year = ProjectYear::orderBy('id', 'desc')->first();
        if (empty($project_year)) {
            return redirect('/project_year/create')->with('error', 'กรุณาสร้างปีโครงการ');
        }
        $current_year = EducationYear::orderBy('id', 'desc')->first();
        $project_year->education_indicator = explode(',',$project_year->education_indicator);
        $project_year->education_strategy = explode(',',$project_year->education_strategy);
        $project_year->research_indicator = explode(',',$project_year->research_indicator);
        $project_year->research_strategy = explode(',',$project_year->research_strategy);
        $project_year->humanresource_indicator = explode(',',$project_year->humanresource_indicator);
        $project_year->humanresource_strategy = explode(',',$project_year->humanresource_strategy);
        $project_year->academic_indicator = explode(',',$project_year->academic_indicator);
        $project_year->academic_strategy = explode(',',$project_year->academic_strategy);
        $project_year->organization_indicator = explode(',',$project_year->organization_indicator);
        $project_year->organization_strategy = explode(',',$project_year->organization_strategy);
        $project_year->workplace_indicator = explode(',',$project_year->workplace_indicator);
        $project_year->workplace_strategy = explode(',',$project_year->workplace_strategy);
        $project_year->greencampus_indicator = explode(',',$project_year->greencampus_indicator);
        $project_year->greencampus_strategy = explode(',',$project_year->greencampus_strategy);
        $project_year->digital_indicator = explode(',',$project_year->digital_indicator);
        $project_year->digital_strategy = explode(',',$project_year->digital_strategy);
        $project_year->international_indicator = explode(',',$project_year->international_indicator);
        $project_year->international_strategy = explode(',',$project_year->international_strategy);
        $project_year->governance_indicator = explode(',',$project_year->governance_indicator);
        $project_year->governance_strategy = explode(',',$project_year->governance_strategy);
        $project_year->cooperate_indicator = explode(',',$project_year->cooperate_indicator);
        $project_year->cooperate_strategy = explode(',',$project_year->cooperate_strategy);
        $data = array(
            'current_year' => $current_year,
            'project_year' => $project_year,
        );
        return view('project.admin_editYear', $data);
    }

    public function updateYear(Request $request, $id) {
        $projectYear = ProjectYear::find($id);
        $projectYear->education_indicator = implode(',',$request->education_indicator);
        $projectYear->education_strategy = implode(',',$request->education_strategy);
        $projectYear->research_indicator = implode(',',$request->research_indicator);
        $projectYear->research_strategy = implode(',',$request->research_strategy);
        $projectYear->humanresource_indicator = implode(',',$request->humanresource_indicator);
        $projectYear->humanresource_strategy = implode(',',$request->humanresource_strategy);
        $projectYear->academic_indicator = implode(',',$request->academic_indicator);
        $projectYear->academic_strategy = implode(',',$request->academic_strategy);
        $projectYear->organization_indicator = implode(',',$request->organization_indicator);
        $projectYear->organization_strategy = implode(',',$request->organization_strategy);
        $projectYear->workplace_indicator = implode(',',$request->workplace_indicator);
        $projectYear->workplace_strategy = implode(',',$request->workplace_strategy);
        $projectYear->greencampus_indicator = implode(',',$request->greencampus_indicator);
        $projectYear->greencampus_strategy = implode(',',$request->greencampus_strategy);
        $projectYear->digital_indicator = implode(',',$request->digital_indicator);
        $projectYear->digital_strategy = implode(',',$request->digital_strategy);
        $projectYear->international_indicator = implode(',',$request->international_indicator);
        $projectYear->international_strategy = implode(',',$request->international_strategy);
        $projectYear->governance_indicator = implode(',',$request->governance_indicator);
        $projectYear->governance_strategy = implode(',',$request->governance_strategy);
        $projectYear->cooperate_indicator = implode(',',$request->cooperate_indicator);
        $projectYear->cooperate_strategy = implode(',',$request->cooperate_strategy);
        $projectYear->save();
        return redirect('/project_year/edit')->with('success', 'บันทึกการแก้ไขปีโครงการเรียบร้อยแล้ว');
    }

    // Upload File Project
    public function uploadProcessingFile(Request $request, $id) {
        $project = Project::find($id);
        if($project->processing_file != "") {
            $filePath = 'public/processing_file_uploads'.$project->processing_file;
            Storage::delete($filePath);
        }
        $fileWithExtens = $request->file('processing_file')->getClientOriginalName();
        $fileName = pathinfo($fileWithExtens, PATHINFO_FILENAME);
        $ext = $request->file('processing_file')->getClientOriginalExtension();
        $fileToStore = $fileName.'_'.time().'.'.$ext; 
        $path = $request->file('processing_file')->storeAs('public/processing_file_uploads', $fileToStore);
        $project->processing_file = $fileToStore;
        $project->save();
        return redirect($request->path_url);
    }

    public function uploadProcessedFile(Request $request, $id) {
        $project = Project::find($id);
        if($project->processing_file != "") {
            $filePath = 'public/processed_file_uploads'.$project->processed_file;
            Storage::delete($filePath);
        }
        $fileWithExtens = $request->file('processed_file')->getClientOriginalName();
        $fileName = pathinfo($fileWithExtens, PATHINFO_FILENAME);
        $ext = $request->file('processed_file')->getClientOriginalExtension();
        $fileToStore = $fileName.'_'.time().'.'.$ext; 
        $path = $request->file('processed_file')->storeAs('public/processed_file_uploads', $fileToStore);
        $project->processed_file = $fileToStore;
        $project->save();
        return redirect($request->path_url);
    }

    public function userProject(Request $request) {
        $years = ProjectYear::orderBy('id', 'desc')->get();
        if ($request->year) {
            $current_year = EducationYear::find($request->year);
        } else {
            $current_year = EducationYear::orderBy('id','desc')->first();
        }
        if (empty($current_year)) {
            return redirect('/')->with('error', 'ยังไม่มีปีโครงการในระบบ');
        }
        $data = array(
            'education' => $this->explodeAndGetAuthorize(Project::where('type', 1)->where('project_year_id', $current_year->id)->get()),
            'research' => $this->explodeAndGetAuthorize(Project::where('type', 2)->where('project_year_id', $current_year->id)->get()),
            'humanresource' => $this->explodeAndGetAuthorize(Project::where('type', 3)->where('project_year_id', $current_year->id)->get()),
            'academic' => $this->explodeAndGetAuthorize(Project::where('type', 4)->where('project_year_id', $current_year->id)->get()),
            'organization' => $this->explodeAndGetAuthorize(Project::where('type', 5)->where('project_year_id', $current_year->id)->get()),
            'workplace' => $this->explodeAndGetAuthorize(Project::where('type', 6)->where('project_year_id', $current_year->id)->get()),
            'greencampus' => $this->explodeAndGetAuthorize(Project::where('type', 7)->where('project_year_id', $current_year->id)->get()),
            'digital' => $this->explodeAndGetAuthorize(Project::where('type', 8)->where('project_year_id', $current_year->id)->get()),
            'international' => $this->explodeAndGetAuthorize(Project::where('type', 9)->where('project_year_id', $current_year->id)->get()),
            'governance' => $this->explodeAndGetAuthorize(Project::where('type', 10)->where('project_year_id', $current_year->id)->get()),
            'cooperate' => $this->explodeAndGetAuthorize(Project::where('type', 11)->where('project_year_id', $current_year->id)->get()),
            'current_year' => $current_year,
            'years' => array()
        );
        foreach ($years as $item) {
            $year = $item->getYear();
            $data['years'][$year->id] = $year->year;
        }
        return view('project.user_project')->with($data);
    }

    private function explodeAndGetAuthorize($projects) {
        foreach($projects as $key => $item) {
            $item->indicator = explode(',', $item->indicator);
            $auth = $item->getAuthorize();
            $projects[$key]['authorize'] = $auth->name;
        }
        return $projects;
    }

    private function createKeyByValue($stringArray) {
        $array = explode(',',$stringArray);
        foreach($array as $key => $value) {
            unset($array[$key]);
            $array[$value] = $value;
        }
        return $array;
    }
}
