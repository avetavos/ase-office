<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ExampleFileController extends Controller
{
    public function tqf(Request $request) {
        return view('examples.tqf');
    }

    public function project(Request $request) {
        return view('examples.project');
    }
    
    public function exam(Request $request) {
        return view('examples.exam');
    }
}
