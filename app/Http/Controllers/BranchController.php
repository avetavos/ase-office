<?php

namespace App\Http\Controllers;

use App\Branch;
use Auth;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
            $bachelor = Branch::all()->where('degree', '=', 'bachelor');
            $master = Branch::all()->where('degree', '=', 'master');
            $doctor = Branch::all()->where('degree', '=', 'doctor');
            $data = array(
                'bachelor' => $bachelor,
                'master' => $master,
                'doctor' => $doctor,
            );
            return view('branch.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(Auth::user()->admin == 1){
            return view('branch.create');
        }else {
            return redirect('/');
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'degree' => 'required',
        ]);
        $branch = new Branch;
        $branch->name = $request->name;
        $branch->degree = $request->degree;
        $branch->save();
        return redirect('/branch/create')->with('success', 'สร้างสาขา '.$request->name.' เรียบร้อย');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $branch = Branch::find($id);
        $data = array(
            'branch' => $branch,
        );
        return view('branch.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'degree' => 'required',
        ]);
        $branch = Branch::find($id);
        $branch->name = $request->name;
        $branch->degree = $request->degree;
        $branch->save();
        return redirect('/branch')->with('success', 'บันทึกการแก้ไขข้อมูลสาขา '.$request->name.' เรียบร้อยแล้ว');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Branch::destroy($id);
        return redirect('/branch')->with('success', 'ลบสาขา '.$request->branch_name.' เรียบร้อย');
    }
}