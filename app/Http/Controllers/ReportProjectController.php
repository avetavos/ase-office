<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\ProjectYear;
use App\EducationYear;
use Response;

class ReportProjectController extends Controller
{
    public function index(Request $request) {
        $year_arr = ProjectYear::orderBy('id', 'desc')->get();
        $education_year_checking = EducationYear::all();
        if(sizeof($education_year_checking) <= 0) {
            return redirect('/education_year/create')->with('error', 'กรุณาสร้างปีการศึกษา');
        }
        $project_year_checking = ProjectYear::orderBy('id', 'desc')->get();
        if (sizeof($project_year_checking) <= 0) {
            return redirect('/project_year/create')->with('error', 'กรุณาสร้างปีการโครงการ');
        }
        foreach($year_arr as $item) {
            $item['education_year'] = EducationYear::find($item->education_year_id);
        }
        if ($request->year) {
            $current_year = ProjectYear::find($request->year);
        } {
            $current_year = ProjectYear::orderBy('id', 'desc')->first();
        }
        $current_year['education_year'] = EducationYear::find($current_year->education_year_id);
        $current_year->education_indicator = explode(',',$current_year->education_indicator);
        $current_year->education_strategy = explode(',',$current_year->education_strategy);
        $current_year->research_indicator = explode(',',$current_year->research_indicator);
        $current_year->research_strategy = explode(',',$current_year->research_strategy);
        $current_year->humanresource_indicator = explode(',',$current_year->humanresource_indicator);
        $current_year->humanresource_strategy = explode(',',$current_year->humanresource_strategy);
        $current_year->academic_indicator = explode(',',$current_year->academic_indicator);
        $current_year->academic_strategy = explode(',',$current_year->academic_strategy);
        $current_year->organization_indicator = explode(',',$current_year->organization_indicator);
        $current_year->organization_strategy = explode(',',$current_year->organization_strategy);
        $current_year->workplace_indicator = explode(',',$current_year->workplace_indicator);
        $current_year->workplace_strategy = explode(',',$current_year->workplace_strategy);
        $current_year->greencampus_indicator = explode(',',$current_year->greencampus_indicator);
        $current_year->greencampus_strategy = explode(',',$current_year->greencampus_strategy);
        $current_year->digital_indicator = explode(',',$current_year->digital_indicator);
        $current_year->digital_strategy = explode(',',$current_year->digital_strategy);
        $current_year->international_indicator = explode(',',$current_year->international_indicator);
        $current_year->international_strategy = explode(',',$current_year->international_strategy);
        $current_year->governance_indicator = explode(',',$current_year->governance_indicator);
        $current_year->governance_strategy = explode(',',$current_year->governance_strategy);
        $current_year->cooperate_indicator = explode(',',$current_year->cooperate_indicator);
        $current_year->cooperate_strategy = explode(',',$current_year->cooperate_strategy);

        /*================================ All ================================*/
        $all_all = [];
        $all_project = Project::where('project_year_id', $current_year->id)->get();
        $education_all_full = Project::where('type', 1)->where('project_year_id', $current_year->id)->get();
        $research_all_full = Project::where('type', 2)->where('project_year_id', $current_year->id)->get();
        $humanresource_all_full = Project::where('type', 3)->where('project_year_id', $current_year->id)->get();
        $academic_all_full = Project::where('type', 4)->where('project_year_id', $current_year->id)->get();
        $organization_all_full = Project::where('type', 5)->where('project_year_id', $current_year->id)->get();
        $workplace_all_full = Project::where('type', 6)->where('project_year_id', $current_year->id)->get();
        $greencampus_all_full = Project::where('type', 7)->where('project_year_id', $current_year->id)->get();
        $digital_all_full = Project::where('type', 8)->where('project_year_id', $current_year->id)->get();
        $international_all_full = Project::where('type', 9)->where('project_year_id', $current_year->id)->get();
        $governance_all_full = Project::where('type', 10)->where('project_year_id', $current_year->id)->get();
        $cooperate_all_full = Project::where('type', 11)->where('project_year_id', $current_year->id)->get();
        foreach($all_project as $item) {
            $item->indicator = explode(',', $item->indicator);
        }
        $education_all = $this->countArray($education_all_full);
        $research_all = $this->countArray($research_all_full);
        $humanresource_all = $this->countArray($humanresource_all_full);
        $academic_all = $this->countArray($academic_all_full);
        $organization_all = $this->countArray($organization_all_full);
        $workplace_all = $this->countArray($workplace_all_full);
        $greencampus_all = $this->countArray($greencampus_all_full);
        $digital_all = $this->countArray($digital_all_full);
        $international_all = $this->countArray($international_all_full);
        $governance_all = $this->countArray($governance_all_full);
        $cooperate_all = $this->countArray($cooperate_all_full);
        $all_all['indicator'] = $this->mergeArray(
            $education_all['indicator'], 
            $research_all['indicator'],
            $humanresource_all['indicator'], 
            $academic_all['indicator'], 
            $organization_all['indicator'],
            $workplace_all['indicator'],
            $greencampus_all['indicator'],
            $digital_all['indicator'],
            $international_all['indicator'],
            $governance_all['indicator'],
            $cooperate_all['indicator'],
        );
        $all_all['strategy'] = $this->mergeArray(
            $education_all['strategy'], 
            $research_all['strategy'], 
            $humanresource_all['strategy'], 
            $academic_all['strategy'], 
            $organization_all['strategy'],
            $workplace_all['strategy'],
            $greencampus_all['strategy'],
            $digital_all['strategy'],
            $international_all['strategy'],
            $governance_all['strategy'],
            $cooperate_all['strategy'],
        );

        /*================================ Processing ================================*/
        $processing_all = [];
        $processing_project_all = Project::where('project_year_id', $current_year->id)->where('processing_file','!=','')->where('processed_file','')->get();
        $education_processing_all = Project::where('type', 1)->where('project_year_id', $current_year->id)->where('processing_file','!=','')->where('processed_file','')->get();
        $research_processing_all = Project::where('type', 2)->where('project_year_id', $current_year->id)->where('processing_file','!=','')->where('processed_file','')->get();
        $humanresource_processing_all = Project::where('type', 3)->where('project_year_id', $current_year->id)->where('processing_file','!=','')->where('processed_file','')->get();
        $academic_processing_all = Project::where('type', 4)->where('project_year_id', $current_year->id)->where('processing_file','!=','')->where('processed_file','')->get();
        $organization_processing_all = Project::where('type', 5)->where('project_year_id', $current_year->id)->where('processing_file','!=','')->where('processed_file','')->get();
        $workplace_processing_all = Project::where('type', 6)->where('project_year_id', $current_year->id)->where('processing_file','!=','')->where('processed_file','')->get();
        $greencampus_processing_all = Project::where('type', 7)->where('project_year_id', $current_year->id)->where('processing_file','!=','')->where('processed_file','')->get();
        $digital_processing_all = Project::where('type', 8)->where('project_year_id', $current_year->id)->where('processing_file','!=','')->where('processed_file','')->get();
        $international_processing_all = Project::where('type', 9)->where('project_year_id', $current_year->id)->where('processing_file','!=','')->where('processed_file','')->get();
        $governance_processing_all = Project::where('type', 10)->where('project_year_id', $current_year->id)->where('processing_file','!=','')->where('processed_file','')->get();
        $cooperate_processing_all = Project::where('type', 11)->where('project_year_id', $current_year->id)->where('processing_file','!=','')->where('processed_file','')->get();
        foreach($processing_project_all as $item) {
            $item->indicator = explode(',', $item->indicator);
        }
        $education_processing = $this->countArray($education_processing_all);
        $research_processing = $this->countArray($research_processing_all);
        $humanresource_processing = $this->countArray($humanresource_processing_all);
        $academic_processing = $this->countArray($academic_processing_all);
        $organization_processing = $this->countArray($organization_processing_all);
        $workplace_processing = $this->countArray($workplace_processing_all);
        $greencampus_processing = $this->countArray($greencampus_processing_all);
        $digital_processing = $this->countArray($digital_processing_all);
        $international_processing = $this->countArray($international_processing_all);
        $governance_processing = $this->countArray($governance_processing_all);
        $cooperate_processing = $this->countArray($cooperate_processing_all);
        $processing_all['indicator'] = $this->mergeArray(
            $education_processing['indicator'],
            $research_processing['indicator'], 
            $humanresource_processing['indicator'], 
            $academic_processing['indicator'], 
            $organization_processing['indicator'],
            $workplace_processing['indicator'],
            $greencampus_processing['indicator'],
            $digital_processing['indicator'],
            $international_processing['indicator'],
            $governance_processing['indicator'],
            $cooperate_processing['indicator'],
        );
        $processing_all['strategy'] = $this->mergeArray(
            $education_processing['strategy'], 
            $research_processing['strategy'], 
            $humanresource_processing['strategy'], 
            $academic_processing['strategy'], 
            $organization_processing['strategy'],
            $workplace_processing['strategy'],
            $greencampus_processing['strategy'],
            $digital_processing['strategy'],
            $international_processing['strategy'],
            $governance_processing['strategy'],
            $cooperate_processing['strategy'],
        );

        /*================================ Processed ================================*/
        $processed_all = [];
        $processed_project_all = Project::where('project_year_id', $current_year->id)->where('processing_file','!=','')->where('processed_file','!=', '')->get();
        $education_processed_all = Project::where('type', 1)->where('project_year_id', $current_year->id)->where('processed_file','!=','')->where('processed_file','!=', '')->get();
        $research_processed_all = Project::where('type', 2)->where('project_year_id', $current_year->id)->where('processed_file','!=','')->where('processed_file','!=', '')->get();
        $humanresource_processed_all = Project::where('type', 3)->where('project_year_id', $current_year->id)->where('processed_file','!=','')->where('processed_file','!=', '')->get();
        $academic_processed_all = Project::where('type', 4)->where('project_year_id', $current_year->id)->where('processed_file','!=','')->where('processed_file','!=', '')->get();
        $organization_processed_all = Project::where('type', 5)->where('project_year_id', $current_year->id)->where('processed_file','!=','')->where('processed_file','!=', '')->get();
        $workplace_processed_all = Project::where('type', 6)->where('project_year_id', $current_year->id)->where('processed_file','!=','')->where('processed_file','!=', '')->get();
        $greencampus_processed_all = Project::where('type', 7)->where('project_year_id', $current_year->id)->where('processed_file','!=','')->where('processed_file','!=', '')->get();
        $digital_processed_all = Project::where('type', 8)->where('project_year_id', $current_year->id)->where('processed_file','!=','')->where('processed_file','!=', '')->get();
        $international_processed_all = Project::where('type', 9)->where('project_year_id', $current_year->id)->where('processed_file','!=','')->where('processed_file','!=', '')->get();
        $governance_processed_all = Project::where('type', 10)->where('project_year_id', $current_year->id)->where('processed_file','!=','')->where('processed_file','!=', '')->get();
        $cooperate_processed_all = Project::where('type', 11)->where('project_year_id', $current_year->id)->where('processed_file','!=','')->where('processed_file','!=', '')->get();
        foreach($processed_project_all as $item) {
            $item->indicator = explode(',', $item->indicator);
        }
        $education_processed = $this->countArray($education_processed_all);
        $research_processed = $this->countArray($research_processed_all);
        $humanresource_processed = $this->countArray($humanresource_processed_all);
        $academic_processed = $this->countArray($academic_processed_all);
        $organization_processed = $this->countArray($organization_processed_all);
        $workplace_processed = $this->countArray($workplace_processed_all);
        $greencampus_processed = $this->countArray($greencampus_processed_all);
        $digital_processed = $this->countArray($digital_processed_all);
        $international_processed = $this->countArray($international_processed_all);
        $governance_processed = $this->countArray($governance_processed_all);
        $cooperate_processed = $this->countArray($cooperate_processed_all);
        $processed_all['indicator'] = $this->mergeArray(
            $education_processed['indicator'], 
            $research_processed['indicator'], 
            $humanresource_processed['indicator'], 
            $academic_processed['indicator'], 
            $organization_processed['indicator'],
            $workplace_processed['indicator'],
            $greencampus_processed['indicator'],
            $digital_processed['indicator'],
            $international_processed['indicator'],
            $governance_processed['indicator'],
            $cooperate_processed['indicator'],
        );
        $processed_all['strategy'] = $this->mergeArray(
            $education_processed['strategy'], 
            $research_processed['strategy'], 
            $humanresource_processed['strategy'], 
            $academic_processed['strategy'], 
            $organization_processed['strategy'],
            $workplace_processed['strategy'],
            $greencampus_processed['strategy'],
            $digital_processed['strategy'],
            $international_processed['strategy'],
            $governance_processed['strategy'],
            $cooperate_processed['strategy'],
        );

        /*================================ Non process ================================*/
        $non_all = [];
        $non_project_all = Project::where('project_year_id', $current_year->id)->where('processed_file','')->where('processing_file','')->get();
        $education_non_all = Project::where('type', 1)->where('project_year_id', $current_year->id)->where('processed_file','')->where('processing_file', '')->get();
        $research_non_all = Project::where('type', 2)->where('project_year_id', $current_year->id)->where('processed_file','')->where('processing_file', '')->get();
        $humanresource_non_all = Project::where('type', 3)->where('project_year_id', $current_year->id)->where('processed_file','')->where('processing_file', '')->get();
        $academic_non_all = Project::where('type', 4)->where('project_year_id', $current_year->id)->where('processed_file','')->where('processing_file', '')->get();
        $organization_non_all = Project::where('type', 5)->where('project_year_id', $current_year->id)->where('processed_file','')->where('processing_file', '')->get();
        $workplace_non_all = Project::where('type', 6)->where('project_year_id', $current_year->id)->where('processed_file','')->where('processing_file', '')->get();
        $greencampus_non_all = Project::where('type', 7)->where('project_year_id', $current_year->id)->where('processed_file','')->where('processing_file', '')->get();
        $digital_non_all = Project::where('type', 8)->where('project_year_id', $current_year->id)->where('processed_file','')->where('processing_file', '')->get();
        $international_non_all = Project::where('type', 9)->where('project_year_id', $current_year->id)->where('processed_file','')->where('processing_file', '')->get();
        $governance_non_all = Project::where('type', 10)->where('project_year_id', $current_year->id)->where('processed_file','')->where('processing_file', '')->get();
        $cooperate_non_all = Project::where('type', 11)->where('project_year_id', $current_year->id)->where('processed_file','')->where('processing_file', '')->get();
        foreach($non_project_all as $item) {
            $item->indicator = explode(',', $item->indicator);
        }
        $education_non = $this->countArray($education_non_all);
        $research_non = $this->countArray($research_non_all);
        $humanresource_non = $this->countArray($humanresource_non_all);
        $academic_non = $this->countArray($academic_non_all);
        $organization_non = $this->countArray($organization_non_all);
        $workplace_non = $this->countArray($workplace_non_all);
        $greencampus_non = $this->countArray($greencampus_non_all);
        $digital_non = $this->countArray($digital_non_all);
        $international_non = $this->countArray($international_non_all);
        $governance_non = $this->countArray($governance_non_all);
        $cooperate_non = $this->countArray($cooperate_non_all);
        $non_all['indicator'] = $this->mergeArray(
            $education_non['indicator'], 
            $research_non['indicator'], 
            $humanresource_non['indicator'], 
            $academic_non['indicator'], 
            $organization_non['indicator'],
            $workplace_non['indicator'],
            $greencampus_non['indicator'],
            $digital_non['indicator'],
            $international_non['indicator'],
            $governance_non['indicator'],
            $cooperate_non['indicator'],
        );
        $non_all['strategy'] = $this->mergeArray(
            $education_non['strategy'], 
            $research_non['strategy'], 
            $humanresource_non['strategy'], 
            $academic_non['strategy'],
            $organization_non['strategy'],
            $workplace_non['strategy'],
            $greencampus_non['strategy'],
            $digital_non['strategy'],
            $international_non['strategy'],
            $governance_non['strategy'],
            $cooperate_non['strategy'],
        );

        $data = [
            'current_year' => $current_year,
            'all' => [
                'all_process' => [
                    'indicator' => $all_all['indicator'],
                    'strategy' => $all_all['strategy'],
                    'project' => sizeof($all_project),
                ],
                'processed' => [
                    'indicator' => $processed_all['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($processed_all['indicator']), sizeof($all_all['indicator'])),
                    'strategy' => $processed_all['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($processed_all['strategy']), sizeof($all_all['strategy'])),
                    'project' => sizeof($processed_project_all),
                    'project_percent' => $this->calPercent(sizeof($processed_project_all), sizeof($all_project))
                ],
                'processing' => [
                    'indicator' => $processing_all['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($processing_all['indicator']), sizeof($all_all['indicator'])),
                    'strategy' => $processing_all['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($processing_all['strategy']), sizeof($all_all['strategy'])),
                    'project' => sizeof($processing_project_all),
                    'project_percent' => $this->calPercent(sizeof($processing_project_all), sizeof($all_project))
                ],
                'non_process' => [
                    'indicator' => $non_all['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($non_all['indicator']), sizeof($all_all['indicator'])),
                    'strategy' => $non_all['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($non_all['strategy']), sizeof($all_all['strategy'])),
                    'project' => sizeof($non_project_all),
                    'project_percent' => $this->calPercent(sizeof($non_project_all), sizeof($all_project))
                ]
            ],
            'education' => [
                'all_process' => [
                    'indicator' => $education_all['indicator'],
                    'strategy' => $education_all['strategy'],
                    'project' => sizeof($education_all_full),
                ],
                'processed' => [
                    'indicator' => $education_processed['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($education_processed['indicator']), sizeof($education_all['indicator'])),
                    'strategy' => $education_processed['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($education_processed['strategy']), sizeof($education_all['strategy'])),
                    'project' => sizeof($education_processed_all),
                    'project_percent' => $this->calPercent(sizeof($education_processed_all), sizeof($education_all_full))
                ],
                'processing' => [
                    'indicator' => $education_processing['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($education_processing['indicator']), sizeof($education_all['indicator'])),
                    'strategy' => $education_processing['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($education_processing['strategy']), sizeof($education_all['strategy'])),
                    'project' => sizeof($education_processing_all),
                    'project_percent' => $this->calPercent(sizeof($education_processing_all), sizeof($education_all_full))
                ],
                'non_process' => [
                    'indicator' => $education_non['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($education_non['indicator']), sizeof($education_all['indicator'])),
                    'strategy' => $education_non['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($education_non['strategy']), sizeof($education_all['strategy'])),
                    'project' => sizeof($education_non_all),
                    'project_percent' => $this->calPercent(sizeof($education_non_all), sizeof($education_all_full))
                ]
            ],
            'research' => [
                'all_process' => [
                    'indicator' => $research_all['indicator'],
                    'strategy' => $research_all['strategy'],
                    'project' => sizeof($research_all_full),
                ],
                'processed' => [
                    'indicator' => $research_processed['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($research_processed['indicator']), sizeof($research_all['indicator'])),
                    'strategy' => $research_processed['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($research_processed['strategy']), sizeof($research_all['strategy'])),
                    'project' => sizeof($research_processed_all),
                    'project_percent' => $this->calPercent(sizeof($research_processed_all), sizeof($research_all_full))
                ],
                'processing' => [
                    'indicator' => $research_processing['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($research_processing['indicator']), sizeof($research_all['indicator'])),
                    'strategy' => $research_processing['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($research_processing['strategy']), sizeof($research_all['strategy'])),
                    'project' => sizeof($research_processing_all),
                    'project_percent' => $this->calPercent(sizeof($research_processing_all), sizeof($research_all_full))
                ],
                'non_process' => [
                    'indicator' => $research_non['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($research_non['indicator']), sizeof($research_all['indicator'])),
                    'strategy' => $research_non['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($research_non['strategy']), sizeof($research_all['strategy'])),
                    'project' => sizeof($research_non_all),
                    'project_percent' => $this->calPercent(sizeof($research_non_all), sizeof($research_all_full))
                ]
            ],
            'humanresource' => [
                'all_process' => [
                    'indicator' => $humanresource_all['indicator'],
                    'strategy' => $humanresource_all['strategy'],
                    'project' => sizeof($humanresource_all_full),
                ],
                'processed' => [
                    'indicator' => $humanresource_processed['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($humanresource_processed['indicator']), sizeof($humanresource_all['indicator'])),
                    'strategy' => $humanresource_processed['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($humanresource_processed['strategy']), sizeof($humanresource_all['strategy'])),
                    'project' => sizeof($humanresource_processed_all),
                    'project_percent' => $this->calPercent(sizeof($humanresource_processed_all), sizeof($humanresource_all_full))
                ],
                'processing' => [
                    'indicator' => $humanresource_processing['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($humanresource_processing['indicator']), sizeof($humanresource_all['indicator'])),
                    'strategy' => $humanresource_processing['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($humanresource_processing['strategy']), sizeof($humanresource_all['strategy'])),
                    'project' => sizeof($humanresource_processing_all),
                    'project_percent' => $this->calPercent(sizeof($humanresource_processing_all), sizeof($humanresource_all_full))
                ],
                'non_process' => [
                    'indicator' => $humanresource_non['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($humanresource_non['indicator']), sizeof($humanresource_all['indicator'])),
                    'strategy' => $humanresource_non['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($humanresource_non['strategy']), sizeof($humanresource_all['strategy'])),
                    'project' => sizeof($humanresource_non_all),
                    'project_percent' => $this->calPercent(sizeof($humanresource_non_all), sizeof($humanresource_all_full))
                ]
            ],
            'academic' => [
                'all_process' => [
                    'indicator' => $academic_all['indicator'],
                    'strategy' => $academic_all['strategy'],
                    'project' => sizeof($academic_all_full),
                ],
                'processed' => [
                    'indicator' => $academic_processed['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($academic_processed['indicator']), sizeof($academic_all['indicator'])),
                    'strategy' => $academic_processed['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($academic_processed['strategy']), sizeof($academic_all['strategy'])),
                    'project' => sizeof($academic_processed_all),
                    'project_percent' => $this->calPercent(sizeof($academic_processed_all), sizeof($academic_all_full))
                ],
                'processing' => [
                    'indicator' => $academic_processing['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($academic_processing['indicator']), sizeof($academic_all['indicator'])),
                    'strategy' => $academic_processing['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($academic_processing['strategy']), sizeof($academic_all['strategy'])),
                    'project' => sizeof($academic_processing_all),
                    'project_percent' => $this->calPercent(sizeof($academic_processing_all), sizeof($academic_all_full))
                ],
                'non_process' => [
                    'indicator' => $academic_non['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($academic_non['indicator']), sizeof($academic_all['indicator'])),
                    'strategy' => $academic_non['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($academic_non['strategy']), sizeof($academic_all['strategy'])),
                    'project' => sizeof($academic_non_all),
                    'project_percent' => $this->calPercent(sizeof($academic_non_all), sizeof($academic_all_full))
                ]
            ],
            'organization' => [
                'all_process' => [
                    'indicator' => $organization_all['indicator'],
                    'strategy' => $organization_all['strategy'],
                    'project' => sizeof($organization_all_full),
                ],
                'processed' => [
                    'indicator' => $organization_processed['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($organization_processed['indicator']), sizeof($organization_all['indicator'])),
                    'strategy' => $organization_processed['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($organization_processed['strategy']), sizeof($organization_all['strategy'])),
                    'project' => sizeof($organization_processed_all),
                    'project_percent' => $this->calPercent(sizeof($organization_processed_all), sizeof($organization_all_full))
                ],
                'processing' => [
                    'indicator' => $organization_processing['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($organization_processing['indicator']), sizeof($organization_all['indicator'])),
                    'strategy' => $organization_processing['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($organization_processing['strategy']), sizeof($organization_all['strategy'])),
                    'project' => sizeof($organization_processing_all),
                    'project_percent' => $this->calPercent(sizeof($organization_processing_all), sizeof($organization_all_full))
                ],
                'non_process' => [
                    'indicator' => $organization_non['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($organization_non['indicator']), sizeof($organization_all['indicator'])),
                    'strategy' => $organization_non['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($organization_non['strategy']), sizeof($organization_all['strategy'])),
                    'project' => sizeof($organization_non_all),
                    'project_percent' => $this->calPercent(sizeof($organization_non_all), sizeof($organization_all_full))
                ]
            ],
            'workplace' => [
                'all_process' => [
                    'indicator' => $workplace_all['indicator'],
                    'strategy' => $workplace_all['strategy'],
                    'project' => sizeof($workplace_all_full),
                ],
                'processed' => [
                    'indicator' => $workplace_processed['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($workplace_processed['indicator']), sizeof($workplace_all['indicator'])),
                    'strategy' => $workplace_processed['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($workplace_processed['strategy']), sizeof($workplace_all['strategy'])),
                    'project' => sizeof($workplace_processed_all),
                    'project_percent' => $this->calPercent(sizeof($workplace_processed_all), sizeof($workplace_all_full))
                ],
                'processing' => [
                    'indicator' => $workplace_processing['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($workplace_processing['indicator']), sizeof($workplace_all['indicator'])),
                    'strategy' => $workplace_processing['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($workplace_processing['strategy']), sizeof($workplace_all['strategy'])),
                    'project' => sizeof($workplace_processing_all),
                    'project_percent' => $this->calPercent(sizeof($workplace_processing_all), sizeof($workplace_all_full))
                ],
                'non_process' => [
                    'indicator' => $workplace_non['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($workplace_non['indicator']), sizeof($workplace_all['indicator'])),
                    'strategy' => $workplace_non['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($workplace_non['strategy']), sizeof($workplace_all['strategy'])),
                    'project' => sizeof($workplace_non_all),
                    'project_percent' => $this->calPercent(sizeof($workplace_non_all), sizeof($workplace_all_full))
                ]
            ],
            'greencampus' => [
                'all_process' => [
                    'indicator' => $greencampus_all['indicator'],
                    'strategy' => $greencampus_all['strategy'],
                    'project' => sizeof($greencampus_all_full),
                ],
                'processed' => [
                    'indicator' => $greencampus_processed['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($greencampus_processed['indicator']), sizeof($greencampus_all['indicator'])),
                    'strategy' => $greencampus_processed['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($greencampus_processed['strategy']), sizeof($greencampus_all['strategy'])),
                    'project' => sizeof($greencampus_processed_all),
                    'project_percent' => $this->calPercent(sizeof($greencampus_processed_all), sizeof($greencampus_all_full))
                ],
                'processing' => [
                    'indicator' => $greencampus_processing['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($greencampus_processing['indicator']), sizeof($greencampus_all['indicator'])),
                    'strategy' => $greencampus_processing['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($greencampus_processing['strategy']), sizeof($greencampus_all['strategy'])),
                    'project' => sizeof($greencampus_processing_all),
                    'project_percent' => $this->calPercent(sizeof($greencampus_processing_all), sizeof($greencampus_all_full))
                ],
                'non_process' => [
                    'indicator' => $greencampus_non['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($greencampus_non['indicator']), sizeof($greencampus_all['indicator'])),
                    'strategy' => $greencampus_non['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($greencampus_non['strategy']), sizeof($greencampus_all['strategy'])),
                    'project' => sizeof($greencampus_non_all),
                    'project_percent' => $this->calPercent(sizeof($greencampus_non_all), sizeof($greencampus_all_full))
                ]
            ],
            'digital' => [
                'all_process' => [
                    'indicator' => $digital_all['indicator'],
                    'strategy' => $digital_all['strategy'],
                    'project' => sizeof($digital_all_full),
                ],
                'processed' => [
                    'indicator' => $digital_processed['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($digital_processed['indicator']), sizeof($digital_all['indicator'])),
                    'strategy' => $digital_processed['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($digital_processed['strategy']), sizeof($digital_all['strategy'])),
                    'project' => sizeof($digital_processed_all),
                    'project_percent' => $this->calPercent(sizeof($digital_processed_all), sizeof($digital_all_full))
                ],
                'processing' => [
                    'indicator' => $digital_processing['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($digital_processing['indicator']), sizeof($digital_all['indicator'])),
                    'strategy' => $digital_processing['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($digital_processing['strategy']), sizeof($digital_all['strategy'])),
                    'project' => sizeof($digital_processing_all),
                    'project_percent' => $this->calPercent(sizeof($digital_processing_all), sizeof($digital_all_full))
                ],
                'non_process' => [
                    'indicator' => $digital_non['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($digital_non['indicator']), sizeof($digital_all['indicator'])),
                    'strategy' => $digital_non['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($digital_non['strategy']), sizeof($digital_all['strategy'])),
                    'project' => sizeof($digital_non_all),
                    'project_percent' => $this->calPercent(sizeof($digital_non_all), sizeof($digital_all_full))
                ]
            ],
            'international' => [
                'all_process' => [
                    'indicator' => $international_all['indicator'],
                    'strategy' => $international_all['strategy'],
                    'project' => sizeof($international_all_full),
                ],
                'processed' => [
                    'indicator' => $international_processed['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($international_processed['indicator']), sizeof($international_all['indicator'])),
                    'strategy' => $international_processed['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($international_processed['strategy']), sizeof($international_all['strategy'])),
                    'project' => sizeof($international_processed_all),
                    'project_percent' => $this->calPercent(sizeof($international_processed_all), sizeof($international_all_full))
                ],
                'processing' => [
                    'indicator' => $international_processing['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($international_processing['indicator']), sizeof($international_all['indicator'])),
                    'strategy' => $international_processing['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($international_processing['strategy']), sizeof($international_all['strategy'])),
                    'project' => sizeof($international_processing_all),
                    'project_percent' => $this->calPercent(sizeof($international_processing_all), sizeof($international_all_full))
                ],
                'non_process' => [
                    'indicator' => $international_non['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($international_non['indicator']), sizeof($international_all['indicator'])),
                    'strategy' => $international_non['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($international_non['strategy']), sizeof($international_all['strategy'])),
                    'project' => sizeof($international_non_all),
                    'project_percent' => $this->calPercent(sizeof($international_non_all), sizeof($international_all_full))
                ]
            ],
            'governance' => [
                'all_process' => [
                    'indicator' => $governance_all['indicator'],
                    'strategy' => $governance_all['strategy'],
                    'project' => sizeof($governance_all_full),
                ],
                'processed' => [
                    'indicator' => $governance_processed['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($governance_processed['indicator']), sizeof($governance_all['indicator'])),
                    'strategy' => $governance_processed['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($governance_processed['strategy']), sizeof($governance_all['strategy'])),
                    'project' => sizeof($governance_processed_all),
                    'project_percent' => $this->calPercent(sizeof($governance_processed_all), sizeof($governance_all_full))
                ],
                'processing' => [
                    'indicator' => $governance_processing['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($governance_processing['indicator']), sizeof($governance_all['indicator'])),
                    'strategy' => $governance_processing['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($governance_processing['strategy']), sizeof($governance_all['strategy'])),
                    'project' => sizeof($governance_processing_all),
                    'project_percent' => $this->calPercent(sizeof($governance_processing_all), sizeof($governance_all_full))
                ],
                'non_process' => [
                    'indicator' => $governance_non['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($governance_non['indicator']), sizeof($governance_all['indicator'])),
                    'strategy' => $governance_non['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($governance_non['strategy']), sizeof($governance_all['strategy'])),
                    'project' => sizeof($governance_non_all),
                    'project_percent' => $this->calPercent(sizeof($governance_non_all), sizeof($governance_all_full))
                ]
            ],
            'cooperate' => [
                'all_process' => [
                    'indicator' => $cooperate_all['indicator'],
                    'strategy' => $cooperate_all['strategy'],
                    'project' => sizeof($cooperate_all_full),
                ],
                'processed' => [
                    'indicator' => $cooperate_processed['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($cooperate_processed['indicator']), sizeof($cooperate_all['indicator'])),
                    'strategy' => $cooperate_processed['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($cooperate_processed['strategy']), sizeof($cooperate_all['strategy'])),
                    'project' => sizeof($cooperate_processed_all),
                    'project_percent' => $this->calPercent(sizeof($cooperate_processed_all), sizeof($cooperate_all_full))
                ],
                'processing' => [
                    'indicator' => $cooperate_processing['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($cooperate_processing['indicator']), sizeof($cooperate_all['indicator'])),
                    'strategy' => $cooperate_processing['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($cooperate_processing['strategy']), sizeof($cooperate_all['strategy'])),
                    'project' => sizeof($cooperate_processing_all),
                    'project_percent' => $this->calPercent(sizeof($cooperate_processing_all), sizeof($cooperate_all_full))
                ],
                'non_process' => [
                    'indicator' => $cooperate_non['indicator'],
                    'indicator_percent' => $this->calPercent(sizeof($cooperate_non['indicator']), sizeof($cooperate_all['indicator'])),
                    'strategy' => $cooperate_non['strategy'],
                    'strategy_percent' => $this->calPercent(sizeof($cooperate_non['strategy']), sizeof($cooperate_all['strategy'])),
                    'project' => sizeof($cooperate_non_all),
                    'project_percent' => $this->calPercent(sizeof($cooperate_non_all), sizeof($cooperate_all_full))
                ]
            ],
        ];
        foreach($year_arr as $item) {
            $data['years'][$item->id] = $item->education_year->year;
        }
        return view('report_project.index', $data);
    }

    function countArray($arr):array {
        $tempArr = [
            'indicator' => [],
            'strategy' => []
        ];
        foreach($arr as $item) {
            $item['indicator'] = explode(',', $item['indicator']);
            foreach($item['indicator'] as $value) {
                if (!in_array($value,$tempArr['indicator'])) {
                    array_push($tempArr['indicator'], $value);
                }
            }
            if (!in_array($item['strategy'],$tempArr['strategy'])) {
                array_push($tempArr['strategy'],$item['strategy']);
            }
        }
        return $tempArr;
    }

    function mergeArray(...$arr):array {
        $tempArr = [];
        foreach($arr as $item) {
            $tempArr = array_merge($tempArr, $item);
        }
        return $tempArr;
    }

    function calPercent($dividend, $denominator): float {
        $result;
        if ($dividend != 0) {
            $result = number_format((float)$dividend/$denominator*100, 2, '.', '');
        } else {
            $result = 0.00;
        }
        return $result;
    }
}
