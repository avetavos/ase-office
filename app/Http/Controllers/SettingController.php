<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use Auth;

class SettingController extends Controller
{
    public function show() {
        return view('setting.setting');
    }

    public function update(Request $request) {
        if(isset($request->password)) {
            $this->validate($request, [
                'name' => ['required', 'string', 'max:255'],
                'password' => ['string', 'min:8', 'confirmed'],
            ]);
        } else {
            
            $this->validate($request, [
                'name' => ['required', 'string', 'max:255'],
            ]);
        }
        $setting = User::find(Auth::user()->id);
        if(isset($request->password)) {
            $setting->name = $request->name;
            $setting->password = Hash::make($request->password);
            $setting->save();
        } else {
            $setting->name = $request->name;
            $setting->save();
        }
        return redirect('/setting')->with('success', 'บันทึกการแก้ไขบัญชีผู้ใช้เรียบร้อย');
    }
}
