<?php

namespace App\Http\Controllers;

use App\EducationYear;
use Auth;
use Illuminate\Http\Request;

class EducationYearController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $years = EducationYear::all();
        $data = [];
        for ($i = 0; $i < 5; $i++) {
            $data['years'][(date("Y") + $i) + 543] = (date("Y") + $i) + 543;
        }
        foreach ($years as $year) {
            if (isset($data['years'][$year->year])) {
                unset($data['years'][$year->year]);
            }
        }
        return view('education_year.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'year' => 'required',
            'start_first_term' => 'required',
            'end_first_term' => 'required',
            'start_second_term' => 'required',
            'end_second_term' => 'required',
        ]);
        $eduYear = new EducationYear;
        $eduYear->year = $request->year;
        $eduYear->start_first_term = $request->start_first_term;
        $eduYear->end_first_term = $request->end_first_term;
        $eduYear->start_second_term = $request->start_second_term;
        $eduYear->end_second_term = $request->end_second_term;
        $eduYear->save();
        return redirect('education_year/last/edit')->with('success', 'สร้างปีการศึกษาใหม่เรียบร้อย');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $years = EducationYear::all();
        if(sizeof($years) <= 0){
            return redirect('/education_year/create')->with('error', 'กรุณาสร้างปีการศีกษา');
        }
        $current_year = EducationYear::orderBy('id', 'desc')->first();
        $data = array(
            'current_year' => $current_year,
        );
        for ($i = 0; $i < 5; $i++) {
            if (sizeof($years) < 0) {
                foreach ($years as $year) {
                    if ((date("Y") + $i) + 543 != $year['year']) {
                        $data['years'][(date("Y") + $i) + 543] = (date("Y") + $i) + 543;
                    }
                }
            } else {
                $data['years'][(date("Y") + $i) + 543] = (date("Y") + $i) + 543;
            }
        }
        return view('education_year.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'start_first_term' => 'required',
            'end_first_term' => 'required',
            'start_second_term' => 'required',
            'end_second_term' => 'required',
        ]);
        $eduYear = EducationYear::find($id);
        $eduYear->start_first_term = $request->start_first_term;
        $eduYear->end_first_term = $request->end_first_term;
        $eduYear->start_second_term = $request->start_second_term;
        $eduYear->end_second_term = $request->end_second_term;
        $eduYear->save();
        return redirect('education_year/last/edit')->with('success', 'แก้ไขปีการศึกษาเรียบร้อยแล้ว');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}