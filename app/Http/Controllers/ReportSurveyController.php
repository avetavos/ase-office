<?php

namespace App\Http\Controllers;

use App\Branch;
use Response;
use App\EducationYear;
use App\Exports\ExportExcel;
use App\Survey;
use App\SurveyData;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ReportSurveyController extends Controller
{
    public function reportType(Request $request, $level)
    {
        $years = EducationYear::all();

        if ($request->year) {
            $current_year = EducationYear::find($request->year);
        } else {
            $current_year = EducationYear::orderBy('id', 'desc')->first();
        }
        $surveys = Survey::where('type', $level)->where('education_year_id', $current_year->id)->get();
        $data = array(
            'surveys' => $surveys,
            'current_year' => $current_year,
        );
        foreach ($years as $year) {
            $data['years'][$year->id] = $year->year;
        }
        if ($level == 'graduated') {
            $data['header'] = 'แบบสำรวจความพึงพอใจผู้ใช้บัณฑิต';
        } else {
            foreach ($surveys as $item) {
                $branch = $item->getBranch();
                $data['branches'][$branch->id] = $branch->name;
            }
            if ($level == 'bachelor') {
                $data['header'] = 'ระดับปริญญาตรี';
            } else if ($level == 'master') {
                $data['header'] = 'ระดับบัณฑิตศึกษา';
            } else if ($level == 'teacher') {
                $data['header'] = 'อาจารย์ที่ปรึกษาในหลักสูตร';
            } else if ($level == 'bachelor_teacher') {
                $data['header'] = 'แบบประเมินอาจารย์ประจำหลักสูตร';
            } else if ($level == 'master_teacher') {
                $data['header'] = 'แบบประเมินอาจารย์ประจำหลักสูตร';
            }
        }
        return view('report_survey.report_index')->with($data);
    }

    public function reportBranch($survey_id)
    {
        $survey = Survey::find($survey_id);
        $survey->branch();
        $surveyData = SurveyData::where('survey_id', $survey->id)->whereBetween('created_at', [$survey->start_date, $survey->end_date])->get();
        $surveyAnswer = array();
        $surveyLabel = array();
        $keys = array();
        foreach ($surveyData as $key => $item) {
            $temp = json_decode($item->answer);
            if ($key == 0) {
                foreach ($temp as $key => $val) {
                    array_push($keys, $key);
                    $surveyAnswer[$key] = array();
                }
            }
        }
        foreach ($surveyData as $key => $item) {
            $temp = json_decode($item->answer);
            foreach ($temp as $key => $val) {
                if (is_array($val)) {
                    foreach ($val as $item) {
                        array_push($surveyAnswer[$key], $item);
                    }
                } else if (is_null($val)) {
                    continue;
                } else {
                    array_push($surveyAnswer[$key], $val);
                }
            }
        }
        $data = array(
            'survey' => $survey,
            'surveyData' => json_encode($surveyAnswer, true),
            'count' => sizeof($surveyData),
        );
        if ($survey->type == 'bachelor') {
            $data['header'] = 'แบบประเมินความพึงพอใจของนักศึกษาที่มีต่อคุณภาพหลักสูตร' . $survey->branch->name;
        } else if ($survey->type == 'master') {
            $data['header'] = 'แบบประเมินความพึงพอใจของนักศึกษาที่มีต่อคุณภาพหลักสูตร' . $survey->branch->name;
        } else if ($survey->type == 'bachelor_teacher') {
            $data['header'] = 'แบบประเมินอาจารย์ประจำหลักสูตร หลักสูตร' . $survey->branch->name;
        } else if ($survey->type == 'master_teacher') {
            $data['header'] = 'แบบประเมินอาจารย์ประจำหลักสูตร หลักสูตร' . $survey->branch->name;
        } else if ($survey->type == 'teacher') {
            $data['header'] = 'แบบประเมินความพึงพอใจระบบอาจารย์ที่ปรึกษาในหลักสูตร' . $survey->branch->name;
        } else if ($survey->type == 'graduated') {
            $data['header'] = 'แบบสำรวจความพึงพอใจผู้ใช้บัณทิต';
        }
        if ($survey->type == 'bachelor') {
            return view('report_survey.bachelor_report')->with($data);
        } else if ($survey->type == 'master') {
            return view('report_survey.master_report')->with($data);
        } else if ($survey->type == 'bachelor_teacher') {
            return view('report_survey.bachelor_teacher_report')->with($data);
        } else if ($survey->type == 'master_teacher') {
            return view('report_survey.master_teacher_report')->with($data);
        } else if ($survey->type == 'teacher') {
            return view('report_survey.teacher_report')->with($data);
        } else if ($survey->type == 'graduated') {
            return view('report_survey.graduated_report')->with($data);
        }

    }

    public function typeIndex(Request $request)
    {
        return view('report_survey.type_index');
    }

    public function export($survey_id)
    {
        return Excel::download(new ExportExcel($survey_id), 'report_' . $survey_id . '_' . date('d-m-Y_hia') . '.xlsx');
    }
}
