<?php

namespace App\Http\Controllers;

use Response;
use Auth;
use App\User;
use App\EducationYear;
use App\Project;
use App\SurveyData;
use App\Survey;
use App\Subject;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
  public function index() {
    if (Auth::user()->admin == 0) {
      $current_year = EducationYear::orderBy('id', 'desc')->first();
      $user_data = User::find(Auth::user()->id);
      $user_data->subjects();
      $user_data->projects();
      $project_users = Project::where('user_id','=',Auth::user()->id)->get();
      foreach($project_users as $item) {
        $item->indicator = explode(',', $item->indicator);
      }
      $data = array(
          'current_year' => $current_year,
          'project_users' => $project_users
      );
      if ($current_year) {
          $data['subjects'] = $user_data->subjects->where('education_year_id', '=', $current_year->id)->sortBy('code');
      }
      return view('dashboard.user_dashboard')->with($data);
    } else {
      $current_year = EducationYear::orderBy('id', 'desc')->first();
      $last_project = Project::orderBy('updated_at', 'desc')->limit(5)->get();
      $last_subject = Subject::orderBy('updated_at', 'desc')->limit(5)->get();
      $data = array(
        'current_year' => $current_year,
        'last_project' => $last_project,
        'last_subject' => $last_subject,
      );
        foreach($last_project as $key=>$item){
          $data['last_project'][$key]['authorize'] = $item->getAuthorize();
        }
        foreach($last_subject as $key=>$item){
          $data['last_subject'][$key]['authorize'] = $item->getAuthorize();
        }
      if(!empty($current_year)){
        $surv = Survey::where('education_year_id',$current_year->id)->get();
        if(!empty($surv)){
          $data['survey'] = $surv;

          foreach($surv as $key=>$val) {
            $surveyData = SurveyData::where('survey_id',$val->id)->whereBetween('created_at',[$val->start_date,$val->end_date])->count();
            $data['survey'][$key]['count'] = $surveyData;
            $data['survey'][$key]['branch'] = $val->branch;
          }
        }
      }
      return view('dashboard.admin_dashboard', $data);
    }
  }
}
