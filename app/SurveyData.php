<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyData extends Model
{
    protected $table = 'survey_data';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function branch()
    {
        return $this->belongsTo('App\Branch');
    }

    public function education_year()
    {
        return $this->belongsTo('App\EducationYear');
    }

    public function survey() {
        return $this->belongsTo('App\Survey');
    }
}
