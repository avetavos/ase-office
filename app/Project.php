<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'projects';
    public $primaryKey = 'id';
    public $timestamp = true;

    public function project_year(){
        return $this->belongsTo('App\ProjectYear');  //one to many
    }

    public function authorize(){
        return $this->belongsTo('App\User');  //one to many
    }

    public function getAuthorize() {
        return User::where('id', $this->user_id)->first();
    }

    public function getProjectYears() {
        return EducationYear::where('id', $this->project_year_id)->first();
    }
}
