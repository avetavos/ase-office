<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $table = 'surveys';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function branch()
    {
        return $this->belongsTo('App\Branch');
    }

    public function education_year()
    {
        return $this->belongsTo('App\EducationYear');
    }

    public function survey_data()
    {
        return $this->hasMany('App\SurveyData');
    }

    public function getBranch() {
        return Branch::where('id', $this->branch_id)->first();
    }
}
