<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectYear extends Model
{
    protected $table = 'project_years';
    public $primaryKey = 'id';
    public $timestamp = true;

    public function education_year(){
        return $this->belongsTo('App\EducationYear');
    }
    public function projects(){
        return $this->hasMany('App\Project'); // one to many
    }
    public function getYear() {
        return EducationYear::where('id', $this->education_year_id)->first();
    }
}
